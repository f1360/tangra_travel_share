# **Tangra Travel Carpool**
 
## Project Description
>#### Tangra travel is a software application that allows registered users to apply for trips or create their own.
 
### Dashboard
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639089336/Tangra_Travel_Readme/Dashboard_ql76nr.jpg)
### Login
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639089382/Tangra_Travel_Readme/Login_qitvbt.jpg)
### Register User
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639089430/Tangra_Travel_Readme/Register_iv2fub.jpg)
### Register Car
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639090217/Tangra_Travel_Readme/RegisterCar_zcbin5.jpg)
### Create Trip
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639089523/Tangra_Travel_Readme/CreateTrip_dwkj8v.jpg)
### Create Feedback
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639090115/Tangra_Travel_Readme/CreateFeedback_uwpbnn.jpg)
### Available Trips table
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639090302/Tangra_Travel_Readme/AvailableTripsTable_ye146m.jpg)
### Trip Filter
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639090364/Tangra_Travel_Readme/TripFilter_kpgdz5.jpg)
### Feedback Card
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639090496/Tangra_Travel_Readme/FeedbackCard_zfhvuo.jpg)
### Customer Table (Admin)
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639090622/Tangra_Travel_Readme/AdminCustomerTable_maeter.jpg)
### Car Table (Admin)
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639090693/Tangra_Travel_Readme/AdminCarTable_jgdjlh.jpg)
 
---
 
## Features - MVC
 
> ### View - 1  - (visible to everyone)
```sh
> The home page without sing in gives us the options to login, register, see Top10 drivers/passengers. 
> Upon registration you will be asked to confirm your credentials by accepting the confirmation email.
```
> ### View - 2 - (visible only to logged users)
FOR Passenger
```sh
> User can view and edit his profile.
> User can register a car and the role will be changed to driver automatically.
> User can apply to travel.
> User can see all trips in the system.
> User can give feedback/s to already finished travel/s only to driver on selected travel.
> User can see already given feedback to other users in the system.
> User can see own given and received feedback/s.
```
FOR Driver
```sh
> User can view and edit his profile.
> User can edit own car.
> User can delete own car and the role will be changed to passenger automatically.
> User can create travel.
> User can see all travels.
> User can accept or reject applications to join a trip
> User can apply to other travel as a passenger.
> User can give feedback/s to already finished travel/s only to passengers in selected travel.
> User can see already given feedback to other users in the system.
> User can see own given and received feedback/s.
```
> ### View - 3 - (visible only to admins)
FOR ADMINS
```sh
> Can block/unblock users in the system. Then this user cannot create and apply in trip/s.
> See personal information on a selected customer.
> Can see all cars in the system
```
> ### All tables support pagination.
## Features - API
 
> API DOCUMENT USING SWAGGER!
 
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639091697/Tangra_Travel_Readme/Swagger_ehgojk.jpg)
 
> ### Cars
```sh
> GET: "/api/Cars/user/get-car" => driver get information about his car.
> GET: "/api/Cars/check-car/{carId}" => admin check selected car in the system.
> GET: "/api/Cars" => get all car in the system.
> DELETE: "/api/Cars/" => driver delete own car and role will switched to passenger.
> POST: "/api/Cars/users/register-car" => passenger register a car and role will be switched to driver.
> PUT: "/api/Cars/user/update-car" => driver updates registration number of the car.
```
> ### Customers
```sh
> GET: "/api/Customers" => admin can see all customers in the system.
> POST: "/api/Customers" => user can create profile in the system.
> PUT: "/api/Customers" => user can update his profile.
> DELETE: "/api/Customers" => user can delete his profile.
> GET: "/api/Customers/personal-information" => user take own information.
> GET: "/api/Customers/{customerId}" => admin take personal information for selected user.
> GET: "/api/Customers/search-by" => admin can search by email, username or phone number.
> DELETE: "/api/Customers/user/{customerId}" => admin can delete user profile.
> PUT: "/api/Customers/block-user/{customerId}" => admin can block user.
> PUT: "/api/Customers/unblock-user/{customerId}" => admin can unblock user.
> POST: "/api/Customers/travelApplications/{travelId}" => customer applies to take part in a travel.
> GET: "/api/Customers/travelApplications/{travelId}" => driver list all applicants for selected travel.
> PUT: "/api/Customers/travelApplications/{travelPassengerId}" => driver approves/rejects an applicant.
```
> ### Feedback
```sh
> GET: "/api/Feedbacks" => take all feedbacks.
> POST: "/api/Feedbacks/driver-leave-feedback" => driver leave feedback to passenger.
> POST: "/api/Feedbacks/passenger-leave-feedback" => passenger leave feedback to driver.
```
> ### Travels
```sh
> GET: "/api/Travels" => admin retrieves information for all travels in the system.
> POST: "/api/Travels" => driver create a new travel.
> GET: "/api/Travels/{travelId}" => admin retrieves information about a selected travel.
> PUT: "/api/Travels/{travelId}" => driver updates a travel.
> DELETE: "/api/Travels/{travelId}" => driver deletes a travel.
> GET: "/api/Travels/filter/driver/{driverId}" => admin filters travels by their driver.
> GET: "/api/Travels/filter/startAddress/{startAddressId}" => admin filters travels by their start address.
> GET: "/api/Travels/filter/endAddress/{endAddressId}" => admin filters travels by their end address.
> GET: "/api/Travels/filter/departureTime/{departureTime}" => admin filters travels by their departure time.
> GET: "/api/Travels/filter/status/{travelStatus}" => admin filters travels by their status.
> GET: "/api/Travels/filter/sortBy/" => admin sorts travels by a selected criterion.
```
---
## Installation
> If you wish to clone the app follow the steps below
```sh
> Download the app from the repository
> Add the connection string to your database
> Run the application. The database will be automatically created
```
### Database Diagram
---
![N|Solid](https://res.cloudinary.com/batcave2021/image/upload/v1639093795/Tangra_Travel_Readme/DAtaBaseDiagram_xknhkj.jpg)
 
## Technologies used
As a good average Sabaton enjoyers we choosed following technologies in the development: 
 - ASP.NET Core
 - Microsoft Entity Framework Core
 - MSSQL
 - Moq
 - Postman
 - Swagger
 - Cloudinary
 - View Components
 - HTML
 - CSS
 - SMTP Server
 
## Contacts
 
You can contact us:
 
| Contacts | Email | LinkedIn |
| ------ | ------ | ------ |
| Nikolay Velikov | nikolay_velikov@yahoo.com | https://www.linkedin.com/in/nikolay-velikov |
| Viet Chan | vietchan8820@gmail.com | https://www.linkedin.com/in/viet-chan-63198bb9/ |
