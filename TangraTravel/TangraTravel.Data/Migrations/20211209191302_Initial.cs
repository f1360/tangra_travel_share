﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TangraTravel.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Avatars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FileName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PictureUri = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PublicId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Alternative = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avatars", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Brands",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Access = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Models",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BrandModel = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    BrandId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Models", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Models_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cities_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StreetName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CityId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Admins",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JobLevel = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    AvatarId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Admins_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Admins_Avatars_AvatarId",
                        column: x => x.AvatarId,
                        principalTable: "Avatars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Admins_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    ConfirmationString = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    IsVerified = table.Column<bool>(type: "bit", nullable: false),
                    IsBlocked = table.Column<bool>(type: "bit", nullable: false),
                    Rating = table.Column<double>(type: "float", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    AvatarId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_Avatars_AvatarId",
                        column: x => x.AvatarId,
                        principalTable: "Avatars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModelId = table.Column<int>(type: "int", nullable: false),
                    RegistrationNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Seats = table.Column<int>(type: "int", nullable: false),
                    CustomerId = table.Column<int>(type: "int", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cars_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cars_Models_ModelId",
                        column: x => x.ModelId,
                        principalTable: "Models",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Travels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DriverId = table.Column<int>(type: "int", nullable: false),
                    StartAddressId = table.Column<int>(type: "int", nullable: false),
                    EndAddressId = table.Column<int>(type: "int", nullable: false),
                    DepartureTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FreeSeats = table.Column<int>(type: "int", nullable: false),
                    TravelStatus = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Travels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Travels_Addresses_EndAddressId",
                        column: x => x.EndAddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Travels_Addresses_StartAddressId",
                        column: x => x.StartAddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Travels_Customers_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Feedbacks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rating = table.Column<int>(type: "int", nullable: false),
                    TravelId = table.Column<int>(type: "int", nullable: false),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    TargetId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feedbacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Feedbacks_Customers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Feedbacks_Customers_TargetId",
                        column: x => x.TargetId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Feedbacks_Travels_TravelId",
                        column: x => x.TravelId,
                        principalTable: "Travels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TravelComments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Comment = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    TravelId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TravelComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TravelComments_Travels_TravelId",
                        column: x => x.TravelId,
                        principalTable: "Travels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TravelPassengers",
                columns: table => new
                {
                    PassengerId = table.Column<int>(type: "int", nullable: false),
                    TravelId = table.Column<int>(type: "int", nullable: false),
                    ApplyStatus = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TravelPassengers", x => new { x.PassengerId, x.TravelId });
                    table.ForeignKey(
                        name: "FK_TravelPassengers_Customers_PassengerId",
                        column: x => x.PassengerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TravelPassengers_Travels_TravelId",
                        column: x => x.TravelId,
                        principalTable: "Travels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FeedbackComments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Comment = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    FeedbackId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedbackComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedbackComments_Feedbacks_FeedbackId",
                        column: x => x.FeedbackId,
                        principalTable: "Feedbacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Avatars",
                columns: new[] { "Id", "Alternative", "FileName", "PictureUri", "PublicId" },
                values: new object[,]
                {
                    { 1, "Batman", "batman@tangratravel.com", "https://res.cloudinary.com/batcave2021/image/upload/v1638347229/Tangra_Travel/Batman_kx6m3o.jpg", "no-deleting" },
                    { 16, "Steve Rogers", "captamerica@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/captainamerica_q5a9sf.jpg", "no-deleting" },
                    { 15, "Pepper Pots", "pepperpots@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/ironman_e8ajf1.jpg", "no-deleting" },
                    { 14, "Natasha Romanoff", "natRomanoff@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/blackwidow_fcl6ol.jpg", "no-deleting" },
                    { 13, "Dio Brando", "diobrando2022@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/diobrando_inqihb.jpg", "no-deleting" },
                    { 11, "John Wick", "jWick@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/johnwick_w3gejb.jpg", "no-deleting" },
                    { 10, "Antony Davis", "antonyDavis2022@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/antonydavis_c0wv2b.jpg", "no-deleting" },
                    { 9, "Chris Bosh", "chrisBosh2022@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/chrisbosh_b40odx.jpg", "no-deleting" },
                    { 12, "Jonhatan Joestar", "jojojojo@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/johanatajoestar_wimjib.jpg", "no-deleting" },
                    { 7, "Tim Dunkan", "timiDunkan@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/timdunkan_zlrqu2.jpg", "no-deleting" },
                    { 6, "Chris Paul", "cp32022@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/chrispaul_rfuaui.jpg", "no-deleting" },
                    { 5, "Lebron James", "lbj2022@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/lebronjames_yut0bc.jpg", "no-deleting" },
                    { 4, "Wonder Woman", "wonder_woman@tangratravel.com", "https://res.cloudinary.com/batcave2021/image/upload/v1638347230/Tangra_Travel/Wonder_Woman_btqdvo.jpg", "no-deleting" },
                    { 3, "Superman", "superman@tangratravel.com", "https://res.cloudinary.com/batcave2021/image/upload/v1638348087/Tangra_Travel/Superman_z8pkde.jpg", "no-deleting" },
                    { 2, "Vasil Levski", "lion@tangratravel.com", "https://res.cloudinary.com/batcave2021/image/upload/v1638347229/Tangra_Travel/Vasil_Levski_wztuea.jpg", "no-deleting" },
                    { 8, "Tony Parker", "tonyP2022@dir.bg", "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/tonyparker_kytsbt.png", "no-deleting" }
                });

            migrationBuilder.InsertData(
                table: "Brands",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 10, false, "VW" },
                    { 15, false, "Skoda" },
                    { 14, false, "Citroen" },
                    { 13, false, "Peugeot" },
                    { 12, false, "Renault" },
                    { 11, false, "Opel" },
                    { 9, false, "Cadillac" },
                    { 5, false, "Mazda" },
                    { 7, false, "Ford" },
                    { 6, false, "Honda" },
                    { 4, false, "Toyota" },
                    { 3, false, "Audi" },
                    { 2, false, "Mercedes-Benz" },
                    { 1, false, "BMW" },
                    { 8, false, "GMC" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 7, false, "England" },
                    { 6, false, "Italy" },
                    { 5, false, "Spain" },
                    { 2, false, "Germany" },
                    { 3, false, "France" },
                    { 1, false, "Bulgaria" },
                    { 4, false, "Portugal" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Access", "IsDeleted" },
                values: new object[,]
                {
                    { 2, "Driver", false },
                    { 1, "Admin", false },
                    { 3, "Passenger", false }
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "Id", "CountryId", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 28, 7, false, "Liverpool" },
                    { 1, 1, false, "Sofia" },
                    { 2, 1, false, "Plovdiv" },
                    { 3, 1, false, "Burgas" },
                    { 4, 1, false, "Varna" },
                    { 5, 2, false, "Berlin" },
                    { 6, 2, false, "Munich" },
                    { 7, 2, false, "Hamburg" },
                    { 8, 2, false, "Stuttgart" },
                    { 9, 3, false, "Paris" },
                    { 10, 3, false, "Marseille" },
                    { 11, 3, false, "Lyon" },
                    { 13, 4, false, "Lisbon" },
                    { 12, 3, false, "Bordo" },
                    { 15, 4, false, "Faro" },
                    { 26, 7, false, "Coventry" },
                    { 25, 7, false, "London" },
                    { 24, 6, false, "Genoa" },
                    { 23, 6, false, "Palermo" },
                    { 14, 4, false, "Porto" },
                    { 21, 6, false, "Rome" },
                    { 22, 6, false, "Milan" },
                    { 19, 5, false, "Sevillia" },
                    { 18, 5, false, "Barcelona" },
                    { 17, 5, false, "Madrid" },
                    { 16, 4, false, "Lagos" },
                    { 20, 5, false, "Valencia" },
                    { 27, 7, false, "Manchester" }
                });

            migrationBuilder.InsertData(
                table: "Models",
                columns: new[] { "Id", "BrandId", "BrandModel", "IsDeleted" },
                values: new object[,]
                {
                    { 33, 15, "Superb", false },
                    { 35, 15, "Kodiac", false },
                    { 34, 15, "Rapid", false },
                    { 32, 14, "C-Cross", false },
                    { 30, 13, "3008", false },
                    { 2, 1, "5-er series", false },
                    { 3, 1, "X3 series", false },
                    { 4, 1, "X5 series", false },
                    { 5, 2, "C-class", false },
                    { 6, 2, "E-class", false },
                    { 7, 2, "GLE", false },
                    { 8, 2, "GL", false },
                    { 9, 3, "A4", false },
                    { 10, 3, "A6", false }
                });

            migrationBuilder.InsertData(
                table: "Models",
                columns: new[] { "Id", "BrandId", "BrandModel", "IsDeleted" },
                values: new object[,]
                {
                    { 11, 3, "Q3", false },
                    { 12, 3, "Q7", false },
                    { 13, 4, "Avensis", false },
                    { 14, 5, "6", false },
                    { 31, 13, "5008", false },
                    { 15, 5, "CX-30", false },
                    { 17, 6, "Jazz", false },
                    { 18, 7, "B-Max", false },
                    { 19, 8, "Terrain", false },
                    { 20, 8, "Acadia", false },
                    { 21, 9, "Escalade", false },
                    { 22, 10, "Passat", false },
                    { 23, 10, "Tiguan", false },
                    { 24, 10, "Arteon", false },
                    { 25, 10, "Phaiton", false },
                    { 26, 11, "Moka", false },
                    { 27, 12, "Megane", false },
                    { 28, 13, "308", false },
                    { 29, 13, "508", false },
                    { 16, 5, "CX-50", false },
                    { 1, 1, "3-er series", false }
                });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "CityId", "IsDeleted", "StreetName" },
                values: new object[,]
                {
                    { 1, 1, false, "Sofia_Address" },
                    { 26, 26, false, "Coventry_Address" },
                    { 25, 25, false, "London_Address" },
                    { 24, 24, false, "Genoa_Address" },
                    { 23, 23, false, "Palermo_Address" },
                    { 22, 22, false, "Milan_Address" },
                    { 21, 21, false, "Rome_Address" },
                    { 20, 20, false, "Valencia_Address" },
                    { 19, 19, false, "Sevillia_Address" },
                    { 18, 18, false, "Barcelona_Address" },
                    { 17, 17, false, "Madrid_Address" },
                    { 16, 16, false, "Lagos_Address" },
                    { 15, 15, false, "Faro_Address" },
                    { 14, 14, false, "Porto_Address" },
                    { 13, 13, false, "Lisbon_Address" },
                    { 12, 12, false, "Bordo_Address" },
                    { 11, 11, false, "Molsheim_Address" },
                    { 10, 10, false, "Marseille_Address" },
                    { 9, 9, false, "Paris_Address" },
                    { 8, 8, false, "Stuttgart_Address" },
                    { 7, 7, false, "Hamburg_Address" },
                    { 6, 6, false, "Munich_Address" },
                    { 5, 5, false, "Berlin_Address" },
                    { 4, 4, false, "Varna_Address" },
                    { 3, 3, false, "Burgas_Address" },
                    { 2, 2, false, "Plovdiv_Address" },
                    { 27, 27, false, "Manchester_Address" },
                    { 28, 28, false, "Liverpool_Address" }
                });

            migrationBuilder.InsertData(
                table: "Admins",
                columns: new[] { "Id", "AddressId", "AvatarId", "Email", "FirstName", "Gender", "IsDeleted", "JobLevel", "LastName", "Password", "PhoneNumber", "RoleId" },
                values: new object[,]
                {
                    { 1, 1, 1, "batman@tangratravel.com", "Bruce", "Man", false, "Mid", "Wayne", "Petar123!", "0889000001", 1 },
                    { 2, 1, 2, "lion@tangratravel.com", "Vasil", "Man", false, "Senior", "Levski", "Levski123!", "0889000002", 1 },
                    { 3, 1, 3, "superman@tangratravel.com", "Clark", "NaN", false, "Junior", "Kent", "Smalville123!", "0889000003", 1 },
                    { 4, 1, 4, "wonder_woman@tangratravel.com", "Diana", "Woman", false, "Junior", "Prince", "Wonder1990!", "0889000004", 1 }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "AddressId", "AvatarId", "ConfirmationString", "Email", "FirstName", "Gender", "IsBlocked", "IsDeleted", "IsVerified", "LastName", "Password", "PhoneNumber", "Rating", "RoleId", "Username" },
                values: new object[,]
                {
                    { 17, 12, 11, "1312ASDAS80", "jWick@dir.bg", "John", "Man", false, false, true, "Wick", "Troy1985!", "0878726747", 19.0, 2, "BoogeyMan" },
                    { 15, 12, 9, "1312ASDAS80", "chrisBosh2022@dir.bg", "Chris", "Man", false, false, true, "Bosh", "Troy1985!", "0878726788", 14.0, 2, "CBosh" },
                    { 14, 12, 8, "1312ASDAS80", "tonyP2022@dir.bg", "Tony", "Man", false, false, true, "Parker", "Troy1985!", "0878726719", 22.0, 2, "TonyP" },
                    { 8, 12, null, "1312ASDAS80", "elena@dir.bg", "Elena", "Woman", false, false, true, "Dimitrova", "Troy1985!", "0878726712", 3.0, 3, "leni" },
                    { 18, 11, 12, "1312ASDAS80", "jojojojo@dir.bg", "Jonhatan", "Man", false, false, true, "Joestar", "Troy1985!", "0878726787", 77.0, 3, "JoJoTop" },
                    { 9, 11, null, "1312ASDAS80", "jonDoe2022@gmail.bg", "John", "Man", false, false, true, "Doe", "Troy1985!", "0878726713", 4.0, 3, "Jojo" },
                    { 19, 10, 13, "1312ASDAS80", "diobrando2022@dir.bg", "Dio", "Man", false, false, true, "Brando", "Troy1985!", "0878726797", 43.0, 3, "BigEvil" },
                    { 12, 10, null, "1312ASDAS80", "paulGeorge2022@dir.bg", "Paul", "Man", false, false, true, "George", "Troy1985!", "0878726722", 10.0, 2, "PlayoffP" },
                    { 10, 9, 5, "1312ASDAS80", "lbj2022@dir.bg", "Lebron", "Man", false, false, true, "James", "Troy1985!", "0878726714", 12.0, 3, "Lebron" },
                    { 21, 8, 15, "1312ASDAS80", "pepperpots@dir.bg", "Pepper", "Woman", false, false, true, "Pots", "Troy1985!", "0878726776", 9.0, 2, "Mrs. IronWoman" },
                    { 5, 7, null, "1312ASDAS80", "dk@steal.bg", "Dimitar", "Man", false, false, true, "Kevlarov", "!Steal15", "0878123533", 5.0, 3, "passenger" },
                    { 20, 12, 14, "1312ASDAS80", "natRomanoff@dir.bg", "Natasha", "Woman", false, false, true, "Romanoff", "Troy1985!", "0878726745", 19.0, 2, "Black Widow" },
                    { 13, 5, 7, "1312ASDAS80", "timiDunkan@dir.bg", "Tim", "Man", false, false, true, "Dunkan", "Troy1985!", "0878726733", 9.0, 2, "BigFundamentals" },
                    { 4, 5, null, "1312ASDAS80", "queen@abv.com", "Anastasiq", "Woman", false, false, true, "Penkova", "Princes90!", "0878911008", 0.0, 3, "The Princess" },
                    { 2, 5, null, "1312ASDAS80", "racing@google.com", "Michail", "NaN", false, false, true, "Machov", "@FirstOrLast1", "0878581757", 0.0, 2, "racer" },
                    { 16, 3, 10, "1312ASDAS80", "antonyDavis2022@dir.bg", "Antony", "Man", false, false, true, "Davis", "Troy1985!", "0878726734", 17.0, 2, "AD3" },
                    { 3, 3, null, "1312ASDAS80", "007@google.com", "James", "Man", false, false, true, "Bond", "Bond007!", "0888911007", 3.5, 2, "Agend007" },
                    { 7, 1, null, "1312ASDAS80", "stanchaka@abv.bg", "Stanimir", "NaN", false, false, true, "Stankov", "Hello1World!", "0879126756", 2.0, 3, "stanimir88" },
                    { 6, 1, null, "1312ASDAS80", "aliexpress@express.com", "Ali", "NaN", false, false, true, "Mehmedov", "Ali1234!", "0878126889", 1.0, 3, "ali-express" },
                    { 1, 1, null, "1312ASDAS80", "stoyan.stavrev@abv.bg", "Stoyan", "Man", false, false, true, "Stavrev", "Stoyan123!", "0878521756", 0.0, 2, "fuzzy95" },
                    { 11, 8, 6, "1312ASDAS80", "cp32022@dir.bg", "Chris", "Man", false, false, true, "Paul", "Troy1985!", "0878726709", 6.0, 2, "CP3" },
                    { 22, 12, 16, "1312ASDAS80", "captamerica@dir.bg", "Steve", "Man", false, false, true, "Rogers", "Troy1985!", "0878726739", 104.0, 2, "Cap" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "CustomerId", "IsDeleted", "ModelId", "RegistrationNumber", "Seats" },
                values: new object[,]
                {
                    { 1, 1, false, 7, "CB1122AK", 4 },
                    { 13, 21, false, 2, "CA1234CA", 4 },
                    { 11, 19, false, 30, "CB5218BK", 5 },
                    { 4, 11, false, 29, "CB2215BK", 4 },
                    { 7, 14, false, 19, "CO2266ML", 5 },
                    { 6, 13, false, 16, "A115BK", 4 },
                    { 2, 2, false, 22, "A1155MB", 5 },
                    { 9, 16, false, 35, "PB1598BK", 4 },
                    { 8, 15, false, 26, "B2215MP", 4 },
                    { 10, 17, false, 27, "E1255LK", 6 },
                    { 3, 3, false, 4, "BOND007", 4 },
                    { 12, 20, false, 22, "PK1523AM", 6 },
                    { 14, 22, false, 3, "CB8896PM", 5 },
                    { 5, 12, false, 21, "CB2867AK", 6 }
                });

            migrationBuilder.InsertData(
                table: "Travels",
                columns: new[] { "Id", "DepartureTime", "DriverId", "EndAddressId", "FreeSeats", "IsDeleted", "StartAddressId", "TravelStatus" },
                values: new object[,]
                {
                    { 15, new DateTime(2021, 12, 24, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4971), 12, 2, 2, false, 3, "Open" },
                    { 7, new DateTime(2021, 12, 19, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4733), 14, 4, 4, false, 1, "Open" },
                    { 18, new DateTime(2022, 12, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), 17, 4, 5, false, 1, "Open" },
                    { 10, new DateTime(2021, 12, 29, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4751), 20, 9, 4, false, 7, "Travelling" },
                    { 8, new DateTime(2022, 1, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 15, 1, 2, false, 10, "Open" },
                    { 13, new DateTime(2021, 12, 19, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4903), 11, 4, 4, false, 1, "Open" },
                    { 12, new DateTime(2022, 12, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, 4, 5, false, 1, "Open" },
                    { 14, new DateTime(2022, 1, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 13, 1, 2, false, 10, "Open" },
                    { 3, new DateTime(2021, 12, 24, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4577), 2, 2, 2, false, 3, "Open" },
                    { 9, new DateTime(2021, 12, 24, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4744), 16, 2, 2, false, 3, "Open" },
                    { 6, new DateTime(2022, 12, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 4, 5, false, 1, "Open" },
                    { 2, new DateTime(2022, 1, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, 2, false, 10, "Completed" },
                    { 16, new DateTime(2021, 12, 29, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4991), 1, 9, 4, false, 7, "Open" },
                    { 5, new DateTime(2022, 11, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 4, 3, false, 1, "Travelling" },
                    { 4, new DateTime(2021, 12, 29, 21, 13, 0, 973, DateTimeKind.Local).AddTicks(4656), 1, 9, 4, false, 7, "Open" },
                    { 1, new DateTime(2021, 12, 19, 21, 13, 0, 949, DateTimeKind.Local).AddTicks(7381), 1, 4, 4, false, 1, "Open" },
                    { 11, new DateTime(2022, 11, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, 4, 3, false, 1, "Completed" },
                    { 17, new DateTime(2022, 11, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, 4, 3, false, 1, "Open" }
                });

            migrationBuilder.InsertData(
                table: "Feedbacks",
                columns: new[] { "Id", "AuthorId", "IsDeleted", "Rating", "TargetId", "TravelId" },
                values: new object[,]
                {
                    { 3, 6, false, 2, 3, 2 },
                    { 4, 3, false, 1, 6, 2 },
                    { 2, 3, false, 5, 5, 2 },
                    { 1, 5, false, 5, 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "TravelComments",
                columns: new[] { "Id", "Comment", "IsDeleted", "TravelId" },
                values: new object[,]
                {
                    { 3, "Smoking is ok", false, 3 },
                    { 6, "No children", false, 6 },
                    { 2, "No luggage", false, 2 },
                    { 1, "No smoking", false, 1 },
                    { 5, "No dogs allowed", false, 5 },
                    { 4, "Luggage up to 20 kg", false, 4 }
                });

            migrationBuilder.InsertData(
                table: "TravelPassengers",
                columns: new[] { "PassengerId", "TravelId", "ApplyStatus", "Id", "IsDeleted" },
                values: new object[,]
                {
                    { 6, 3, "Accepted", 7, false },
                    { 8, 5, "Accepted", 4, false },
                    { 5, 2, "Accepted", 1, false },
                    { 6, 2, "Accepted", 6, false },
                    { 6, 1, "Applied", 2, false },
                    { 7, 3, "Rejected", 3, false },
                    { 6, 5, "Canceled", 5, false },
                    { 5, 3, "Accepted", 8, false }
                });

            migrationBuilder.InsertData(
                table: "FeedbackComments",
                columns: new[] { "Id", "Comment", "FeedbackId", "IsDeleted" },
                values: new object[,]
                {
                    { 1, "Great ride with a great driver!", 1, false },
                    { 2, "Great ride with a great driver!", 2, false },
                    { 3, "Very rude driver!!!", 3, false },
                    { 4, "This person had incredibly stinky legs!!!", 4, false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CityId",
                table: "Addresses",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Admins_AddressId",
                table: "Admins",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Admins_AvatarId",
                table: "Admins",
                column: "AvatarId",
                unique: true,
                filter: "[AvatarId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Admins_Email",
                table: "Admins",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Admins_PhoneNumber",
                table: "Admins",
                column: "PhoneNumber",
                unique: true,
                filter: "[PhoneNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Admins_RoleId",
                table: "Admins",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Brands_Name",
                table: "Brands",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cars_CustomerId",
                table: "Cars",
                column: "CustomerId",
                unique: true,
                filter: "[CustomerId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ModelId",
                table: "Cars",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_RegistrationNumber",
                table: "Cars",
                column: "RegistrationNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Cities_CountryId",
                table: "Cities",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Countries_Name",
                table: "Countries",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_AddressId",
                table: "Customers",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_AvatarId",
                table: "Customers",
                column: "AvatarId",
                unique: true,
                filter: "[AvatarId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_Email",
                table: "Customers",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_PhoneNumber",
                table: "Customers",
                column: "PhoneNumber",
                unique: true,
                filter: "[PhoneNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_RoleId",
                table: "Customers",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_Username",
                table: "Customers",
                column: "Username",
                unique: true,
                filter: "[Username] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackComments_FeedbackId",
                table: "FeedbackComments",
                column: "FeedbackId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Feedbacks_AuthorId",
                table: "Feedbacks",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Feedbacks_TargetId",
                table: "Feedbacks",
                column: "TargetId");

            migrationBuilder.CreateIndex(
                name: "IX_Feedbacks_TravelId",
                table: "Feedbacks",
                column: "TravelId");

            migrationBuilder.CreateIndex(
                name: "IX_Models_BrandId",
                table: "Models",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_TravelComments_TravelId",
                table: "TravelComments",
                column: "TravelId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TravelPassengers_TravelId",
                table: "TravelPassengers",
                column: "TravelId");

            migrationBuilder.CreateIndex(
                name: "IX_Travels_DriverId",
                table: "Travels",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Travels_EndAddressId",
                table: "Travels",
                column: "EndAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Travels_StartAddressId",
                table: "Travels",
                column: "StartAddressId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Admins");

            migrationBuilder.DropTable(
                name: "Cars");

            migrationBuilder.DropTable(
                name: "FeedbackComments");

            migrationBuilder.DropTable(
                name: "TravelComments");

            migrationBuilder.DropTable(
                name: "TravelPassengers");

            migrationBuilder.DropTable(
                name: "Models");

            migrationBuilder.DropTable(
                name: "Feedbacks");

            migrationBuilder.DropTable(
                name: "Brands");

            migrationBuilder.DropTable(
                name: "Travels");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Avatars");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
