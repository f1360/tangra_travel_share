﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class RoleConfigurator : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(r => r.Access)
                   .HasConversion<string>()
                   .HasMaxLength(25);
        }
    }
}