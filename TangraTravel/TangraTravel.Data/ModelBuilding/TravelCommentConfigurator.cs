﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class TravelCommentConfigurator : IEntityTypeConfiguration<TravelComment>
    {
        public void Configure(EntityTypeBuilder<TravelComment> builder)
        {
            builder.HasOne(x => x.Travel)
                   .WithOne(x => x.TravelComment)
                   .HasForeignKey<TravelComment>(x => x.TravelId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}