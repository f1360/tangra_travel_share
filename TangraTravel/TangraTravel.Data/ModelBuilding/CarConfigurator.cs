﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class CarConfigurator : IEntityTypeConfiguration<Car>
    {
        public void Configure(EntityTypeBuilder<Car> builder)
        {
            builder.HasIndex(x => x.RegistrationNumber);

            builder.HasOne(x => x.Model)
                   .WithMany(x => x.Cars)
                   .HasForeignKey(x => x.ModelId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Customer)
                   .WithOne(x => x.Car)
                   .HasForeignKey<Car>(x => x.CustomerId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}