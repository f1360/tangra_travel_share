﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class AvatarConfigurator : IEntityTypeConfiguration<Avatar>
    {
        public void Configure(EntityTypeBuilder<Avatar> builder)
        {
            builder.HasOne(x => x.Admin)
                   .WithOne(x => x.Avatar)
                   .HasForeignKey<Admin>(x => x.AvatarId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Customer)
                   .WithOne(x => x.Avatar)
                   .HasForeignKey<Customer>(x => x.AvatarId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}