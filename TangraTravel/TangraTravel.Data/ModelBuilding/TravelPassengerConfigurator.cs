﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class TravelPassengerConfigurator : IEntityTypeConfiguration<TravelPassenger>
    {
        public void Configure(EntityTypeBuilder<TravelPassenger> builder)
        {
            builder.HasKey(x => new { x.PassengerId, x.TravelId });

            builder.HasOne(tp => tp.Passenger)
                   .WithMany(c => c.TravelsAsPassenger)
                   .HasForeignKey(t => t.PassengerId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Travel)
                   .WithMany(a => a.TravelPassengers)
                   .HasForeignKey(t => t.TravelId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.Property(tp => tp.ApplyStatus)
                   .HasConversion<string>()
                   .HasMaxLength(25);

            builder.Property(tp => tp.Id)
                   .ValueGeneratedOnAdd();
        }
    }
}
