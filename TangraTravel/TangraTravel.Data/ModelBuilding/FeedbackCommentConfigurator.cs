﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class FeedbackCommentConfigurator : IEntityTypeConfiguration<FeedbackComment>
    {
        public void Configure(EntityTypeBuilder<FeedbackComment> builder)
        {
            builder.HasOne(x => x.Feedback)
                   .WithOne(x => x.FeedbackComment)
                   .HasForeignKey<FeedbackComment>(x => x.FeedbackId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}