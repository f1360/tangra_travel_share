﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class CustomerConfigurator : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {            
            builder.HasIndex(x => x.Email)
                   .IsUnique();            
            builder.HasIndex(x => x.Username)
                   .IsUnique();
            builder.HasIndex(x => x.PhoneNumber)
                   .IsUnique();

            builder.HasOne(x => x.Role)
                   .WithMany(x => x.Customers)
                   .HasForeignKey(x => x.RoleId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Address)
                   .WithMany(x => x.Customers)
                   .HasForeignKey(x => x.AddressId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.Gender)
                   .HasConversion<string>()
                   .HasMaxLength(15);
        }
    }
}