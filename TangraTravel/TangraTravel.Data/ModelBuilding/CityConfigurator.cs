﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class CityConfigurator : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.HasOne(ci => ci.Country)
                   .WithMany(co => co.Cities)
                   .HasForeignKey(ci => ci.CountryId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}