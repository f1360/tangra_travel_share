﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class FeedbackConfigurator : IEntityTypeConfiguration<Feedback>
    {
        public void Configure(EntityTypeBuilder<Feedback> builder)
        {
            builder.HasOne(x => x.Travel)
                   .WithMany(x => x.Feedbacks)
                   .HasForeignKey(x => x.TravelId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Author)
                   .WithMany(x => x.FeedbacksGiven)
                   .HasForeignKey(x => x.AuthorId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Target)
                   .WithMany(x => x.FeedbacksReceived)
                   .HasForeignKey(x => x.TargetId)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}