﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class BrandConfigurator : IEntityTypeConfiguration<Brand>
    {
        public void Configure(EntityTypeBuilder<Brand> builder)
        {
            builder.HasIndex(x => x.Name)
                   .IsUnique();
        }
    }
}