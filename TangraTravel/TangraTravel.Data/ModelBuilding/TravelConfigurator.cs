﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class TravelConfigurator : IEntityTypeConfiguration<Travel>
    {
        public void Configure(EntityTypeBuilder<Travel> builder)
        {
            builder.HasOne(t => t.Driver)
                   .WithMany(c => c.Travels)
                   .HasForeignKey(t => t.DriverId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.StartAddress)
                   .WithMany(a => a.StartPointTravels)
                   .HasForeignKey(t => t.StartAddressId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.EndAddress)
                   .WithMany(a => a.EndPointTravels)
                   .HasForeignKey(t => t.EndAddressId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.TravelStatus)
                   .HasConversion<string>()
                   .HasMaxLength(30);
        }
    }
}