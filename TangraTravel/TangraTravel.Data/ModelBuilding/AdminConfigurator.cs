﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.ModelBuilding
{
    internal class AdminConfigurator : IEntityTypeConfiguration<Admin>
    {
        public void Configure(EntityTypeBuilder<Admin> builder)
        {
            builder.HasOne(x => x.Address)
                   .WithMany(x => x.Admins)
                   .HasForeignKey(x => x.AddressId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Role)
                   .WithMany(x => x.Admins)
                   .HasForeignKey(x => x.RoleId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(x => x.Email).IsUnique();

            builder.HasIndex(x => x.PhoneNumber).IsUnique();

            builder.Property(x => x.JobLevel)
                   .HasConversion<string>()
                   .HasMaxLength(25);

            builder.Property(x => x.Gender)
                   .HasConversion<string>()
                   .HasMaxLength(15);
        }
    }
}