﻿using System;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;

namespace TangraTravel.Data.DataBase
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var roles = new List<Role>
            {
                new Role{Id = 1, Access = Access.Admin},
                new Role{Id = 2, Access = Access.Driver},
                new Role{Id = 3, Access = Access.Passenger},
            };
            var brands = new List<Brand>
            {
                new Brand{Id = 1, Name = "BMW"},
                new Brand{Id = 2, Name = "Mercedes-Benz"},
                new Brand{Id = 3, Name = "Audi"},
                new Brand{Id = 4, Name = "Toyota"},
                new Brand{Id = 5, Name = "Mazda"},
                new Brand{Id = 6, Name = "Honda"},
                new Brand{Id = 7, Name = "Ford"},
                new Brand{Id = 8, Name = "GMC"},
                new Brand{Id = 9, Name = "Cadillac"},
                new Brand{Id = 10, Name = "VW"},
                new Brand{Id = 11, Name = "Opel"},
                new Brand{Id = 12, Name = "Renault"},
                new Brand{Id = 13, Name = "Peugeot"},
                new Brand{Id = 14, Name = "Citroen"},
                new Brand{Id = 15, Name = "Skoda"},
            };
            var models = new List<Model>
            {
                new Model{Id = 1,BrandId = 1, BrandModel = "3-er series" },
                new Model{Id = 2,BrandId = 1, BrandModel = "5-er series" },
                new Model{Id = 3,BrandId = 1, BrandModel = "X3 series" },
                new Model{Id = 4,BrandId = 1, BrandModel = "X5 series" },
                new Model{Id = 5,BrandId = 2, BrandModel = "C-class" },
                new Model{Id = 6,BrandId = 2, BrandModel = "E-class" },
                new Model{Id = 7,BrandId = 2, BrandModel = "GLE" },
                new Model{Id = 8,BrandId = 2, BrandModel = "GL" },
                new Model{Id = 9,BrandId = 3, BrandModel = "A4" },
                new Model{Id = 10,BrandId = 3, BrandModel = "A6" },
                new Model{Id = 11,BrandId = 3, BrandModel = "Q3" },
                new Model{Id = 12,BrandId = 3, BrandModel = "Q7" },
                new Model{Id = 13,BrandId = 4, BrandModel = "Avensis" },
                new Model{Id = 14,BrandId = 5, BrandModel = "6" },
                new Model{Id = 15,BrandId = 5, BrandModel = "CX-30" },
                new Model{Id = 16,BrandId = 5, BrandModel = "CX-50" },
                new Model{Id = 17,BrandId = 6, BrandModel = "Jazz" },
                new Model{Id = 18,BrandId = 7, BrandModel = "B-Max" },
                new Model{Id = 19,BrandId = 8, BrandModel = "Terrain" },
                new Model{Id = 20,BrandId = 8, BrandModel = "Acadia" },
                new Model{Id = 21,BrandId = 9, BrandModel = "Escalade" },
                new Model{Id = 22,BrandId = 10, BrandModel = "Passat" },
                new Model{Id = 23,BrandId = 10, BrandModel = "Tiguan" },
                new Model{Id = 24,BrandId = 10, BrandModel = "Arteon" },
                new Model{Id = 25,BrandId = 10, BrandModel = "Phaiton" },
                new Model{Id = 26,BrandId = 11, BrandModel = "Moka" },
                new Model{Id = 27,BrandId = 12, BrandModel = "Megane" },
                new Model{Id = 28,BrandId = 13, BrandModel = "308" },
                new Model{Id = 29,BrandId = 13, BrandModel = "508" },
                new Model{Id = 30,BrandId = 13, BrandModel = "3008" },
                new Model{Id = 31,BrandId = 13, BrandModel = "5008" },
                new Model{Id = 32,BrandId = 14, BrandModel = "C-Cross" },
                new Model{Id = 33,BrandId = 15, BrandModel = "Superb" },
                new Model{Id = 34,BrandId = 15, BrandModel = "Rapid" },
                new Model{Id = 35,BrandId = 15, BrandModel = "Kodiac" }
            };

            var info = new List<string>()
            {
                "Bulgaria", "Germany", "France", "Portugal", "Spain", "Italy", "England"
            };
            var countries = new List<Country>();
            int index = 1;
            for (int i = 0; i < info.Count; i++)
            {
                countries.Add(new Country { Id = index++, Name = info[i] });
            }

            info = new List<string>()
            { "Sofia, Plovdiv, Burgas, Varna",
              "Berlin, Munich, Hamburg, Stuttgart",
              "Paris, Marseille, Lyon, Bordo",
              "Lisbon, Porto, Faro, Lagos",
              "Madrid, Barcelona, Sevillia, Valencia",
              "Rome, Milan, Palermo, Genoa",
              "London, Coventry, Manchester, Liverpool"
            };
            var cities = new List<City>();
            index = 1;
            for (int i = 0; i < countries.Count; i++)
            {
                string[] text = info[i].Split(", ");
                var country = countries[i].Id;
                for (int j = 0; j < text.Length; j++)
                {
                    cities.Add(new City()
                    {
                        Id = index++,
                        Name = text[j],
                        CountryId = country
                    });
                }
            }

            info = new List<string>()
            { "Sofia_Address", "Plovdiv_Address", "Burgas_Address", "Varna_Address",
              "Berlin_Address", "Munich_Address", "Hamburg_Address", "Stuttgart_Address",
              "Paris_Address", "Marseille_Address", "Molsheim_Address", "Bordo_Address",
              "Lisbon_Address", "Porto_Address", "Faro_Address", "Lagos_Address",
              "Madrid_Address", "Barcelona_Address", "Sevillia_Address", "Valencia_Address",
              "Rome_Address", "Milan_Address", "Palermo_Address", "Genoa_Address",
              "London_Address", "Coventry_Address", "Manchester_Address", "Liverpool_Address"
            };
            var addresses = new List<Address>();
            index = 1;
            for (int i = 0; i < cities.Count; i++)
            {
                var cityId = cities[i].Id;
                var street = info[i];
                addresses.Add(new Address
                {
                    Id = index++,
                    StreetName = street,
                    CityId = cityId
                });
            }

            var avatars = new List<Avatar>
            {
                new Avatar
                {
                   Id = 1,
                   FileName = "batman@tangratravel.com",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1638347229/Tangra_Travel/Batman_kx6m3o.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Batman"
                },
                new Avatar
                {
                   Id = 2,
                   FileName = "lion@tangratravel.com",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1638347229/Tangra_Travel/Vasil_Levski_wztuea.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Vasil Levski"
                },
                new Avatar
                {
                   Id = 3,
                   FileName = "superman@tangratravel.com",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1638348087/Tangra_Travel/Superman_z8pkde.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Superman"  
                },
                new Avatar
                {
                   Id = 4,
                   FileName = "wonder_woman@tangratravel.com",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1638347230/Tangra_Travel/Wonder_Woman_btqdvo.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Wonder Woman"
                },
                new Avatar
                {
                   Id = 5,
                   FileName = "lbj2022@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/lebronjames_yut0bc.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Lebron James"
                },
                new Avatar
                {
                   Id = 6,
                   FileName = "cp32022@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/chrispaul_rfuaui.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Chris Paul"
                },
                new Avatar
                {
                   Id = 7,
                   FileName = "timiDunkan@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/timdunkan_zlrqu2.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Tim Dunkan"
                },
                new Avatar
                {
                   Id = 8,
                   FileName = "tonyP2022@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/tonyparker_kytsbt.png",
                   PublicId = "no-deleting",
                   Alternative = "Tony Parker"
                },
                new Avatar
                {
                   Id = 9,
                   FileName = "chrisBosh2022@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/chrisbosh_b40odx.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Chris Bosh"
                },
                new Avatar
                {
                   Id = 10,
                   FileName = "antonyDavis2022@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/antonydavis_c0wv2b.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Antony Davis"
                },
                new Avatar
                {
                   Id = 11,
                   FileName = "jWick@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/johnwick_w3gejb.jpg",
                   PublicId = "no-deleting",
                   Alternative = "John Wick"
                },
                new Avatar
                {
                   Id = 12,
                   FileName = "jojojojo@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/johanatajoestar_wimjib.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Jonhatan Joestar"
                },
                new Avatar
                {
                   Id = 13,
                   FileName = "diobrando2022@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/diobrando_inqihb.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Dio Brando"
                },
                new Avatar
                {
                   Id = 14,
                   FileName = "natRomanoff@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/blackwidow_fcl6ol.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Natasha Romanoff"
                },
                new Avatar
                {
                   Id = 15,
                   FileName = "pepperpots@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065865/Tangra_Travel/ironman_e8ajf1.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Pepper Pots"
                },
                new Avatar
                {
                   Id = 16,
                   FileName = "captamerica@dir.bg",
                   PictureUri = "https://res.cloudinary.com/batcave2021/image/upload/v1639065864/Tangra_Travel/captainamerica_q5a9sf.jpg",
                   PublicId = "no-deleting",
                   Alternative = "Steve Rogers"
                },

            };
            var admins = new List<Admin>
            {
                new Admin
                {
                    Id = 1,
                    FirstName = "Bruce",
                    LastName = "Wayne",
                    Email = "batman@tangratravel.com",
                    Password = "Petar123!",
                    RoleId = 1,
                    PhoneNumber = "0889000001",
                    AddressId = 1,
                    JobLevel = JobLevel.Mid,
                    Gender = Gender.Man,
                    AvatarId = 1
                },
                new Admin
                {
                    Id = 2,
                    FirstName = "Vasil",
                    LastName = "Levski",
                    Email = "lion@tangratravel.com",
                    Password = "Levski123!",
                    RoleId = 1,
                    PhoneNumber = "0889000002",
                    AddressId = 1,
                    JobLevel = JobLevel.Senior,
                    Gender = Gender.Man,
                    AvatarId = 2
                },
                new Admin
                {
                    Id = 3,
                    FirstName = "Clark",
                    LastName = "Kent",
                    Email = "superman@tangratravel.com",
                    Password = "Smalville123!",
                    RoleId = 1,
                    PhoneNumber = "0889000003",
                    AddressId = 1,
                    AvatarId = 3
                },
                new Admin
                {
                    Id = 4,
                    FirstName = "Diana",
                    LastName = "Prince",
                    Email = "wonder_woman@tangratravel.com",
                    Password = "Wonder1990!",
                    RoleId = 1,
                    PhoneNumber = "0889000004",
                    AddressId = 1,
                    Gender = Gender.Woman,
                    AvatarId = 4
                }
            };

            var customers = new List<Customer>()
            {
                new Customer
                {
                  Id = 1,
                  FirstName = "Stoyan",
                  LastName = "Stavrev",
                  Email = "stoyan.stavrev@abv.bg",
                  Password = "Stoyan123!",
                  PhoneNumber = "0878521756",
                  RoleId = 2,
                  Username = "fuzzy95",
                  IsVerified = true,
                  AddressId = 1,
                  ConfirmationString = "1312ASDAS80",
                  Gender = Gender.Man
                },
                new Customer
                {
                  Id = 2,
                  FirstName = "Michail",
                  LastName = "Machov",
                  Email = "racing@google.com",
                  Password = "@FirstOrLast1",
                  PhoneNumber = "0878581757",
                  RoleId = 2,
                  Username = "racer",
                  IsVerified = true,
                  AddressId = 5,
                  ConfirmationString = "1312ASDAS80"
                },
                new Customer
                {
                  Id = 3,
                  FirstName = "James",
                  LastName = "Bond",
                  Email = "007@google.com",
                  Password = "Bond007!",
                  PhoneNumber = "0888911007",
                  RoleId = 2,
                  Username = "Agend007",
                  IsVerified = true,
                  AddressId = 3,
                  ConfirmationString = "1312ASDAS80",
                  Gender = Gender.Man,
                  Rating = 3.5
                },
                new Customer
                {
                  Id = 4,
                  FirstName = "Anastasiq",
                  LastName = "Penkova",
                  Email = "queen@abv.com",
                  Password = "Princes90!",
                  PhoneNumber = "0878911008",
                  RoleId = 3,
                  Username = "The Princess",
                  IsVerified = true,
                  AddressId = 5,
                  ConfirmationString = "1312ASDAS80",
                  Gender = Gender.Woman
                },
                new Customer
                {
                  Id = 5,
                  FirstName = "Dimitar",
                  LastName = "Kevlarov",
                  Email = "dk@steal.bg",
                  Password = "!Steal15",
                  PhoneNumber = "0878123533",
                  RoleId = 3,
                  Username = "passenger",
                  IsVerified = true,
                  AddressId = 7,
                  ConfirmationString = "1312ASDAS80",
                  Gender = Gender.Man,
                  Rating = 5
                },
                new Customer
                {
                  Id = 6,
                  FirstName = "Ali",
                  LastName = "Mehmedov",
                  Email = "aliexpress@express.com",
                  Password = "Ali1234!",
                  PhoneNumber = "0878126889",
                  RoleId = 3,
                  Username = "ali-express",
                  IsVerified = true,
                  AddressId = 1,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 1
                },
                new Customer
                {
                  Id = 7,
                  FirstName = "Stanimir",
                  LastName = "Stankov",
                  Email = "stanchaka@abv.bg",
                  Password = "Hello1World!",
                  PhoneNumber = "0879126756",
                  RoleId = 3,
                  Username = "stanimir88",
                  IsVerified = true,
                  AddressId = 1,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 2
                },
                new Customer
                {
                  Id = 8,
                  FirstName = "Elena",
                  LastName = "Dimitrova",
                  Email = "elena@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726712",
                  RoleId = 3,
                  Username = "leni",
                  IsVerified = true,
                  AddressId = 12,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 3,
                  Gender = Gender.Woman
                },
                new Customer
                {
                  Id = 9,
                  FirstName = "John",
                  LastName = "Doe",
                  Email = "jonDoe2022@gmail.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726713",
                  RoleId = 3,
                  Username = "Jojo",
                  IsVerified = true,
                  AddressId = 11,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 4,
                  Gender = Gender.Man
                },
                new Customer
                {
                  Id = 10,
                  FirstName = "Lebron",
                  LastName = "James",
                  Email = "lbj2022@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726714",
                  RoleId = 3,
                  Username = "Lebron",
                  IsVerified = true,
                  AddressId = 9,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 12,
                  Gender = Gender.Man,
                  AvatarId = 5
                },
                new Customer
                {
                  Id = 11,
                  FirstName = "Chris",
                  LastName = "Paul",
                  Email = "cp32022@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726709",
                  RoleId = 2,
                  Username = "CP3",
                  IsVerified = true,
                  AddressId = 8,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 6,
                  Gender = Gender.Man,
                  AvatarId = 6
                },
                new Customer
                {
                  Id = 12,
                  FirstName = "Paul",
                  LastName = "George",
                  Email = "paulGeorge2022@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726722",
                  RoleId = 2,
                  Username = "PlayoffP",
                  IsVerified = true,
                  AddressId = 10,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 10,
                  Gender = Gender.Man,
                },
                new Customer
                {
                  Id = 13,
                  FirstName = "Tim",
                  LastName = "Dunkan",
                  Email = "timiDunkan@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726733",
                  RoleId = 2,
                  Username = "BigFundamentals",
                  IsVerified = true,
                  AddressId = 5,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 9,
                  Gender = Gender.Man,
                  AvatarId = 7
                },
                new Customer
                {
                  Id = 14,
                  FirstName = "Tony",
                  LastName = "Parker",
                  Email = "tonyP2022@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726719",
                  RoleId = 2,
                  Username = "TonyP",
                  IsVerified = true,
                  AddressId = 12,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 22,
                  Gender = Gender.Man,
                  AvatarId = 8
                },
                new Customer
                {
                  Id = 15,
                  FirstName = "Chris",
                  LastName = "Bosh",
                  Email = "chrisBosh2022@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726788",
                  RoleId = 2,
                  Username = "CBosh",
                  IsVerified = true,
                  AddressId = 12,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 14,
                  Gender = Gender.Man,
                  AvatarId = 9
                },
                new Customer
                {
                  Id = 16,
                  FirstName = "Antony",
                  LastName = "Davis",
                  Email = "antonyDavis2022@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726734",
                  RoleId = 2,
                  Username = "AD3",
                  IsVerified = true,
                  AddressId = 3,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 17,
                  Gender = Gender.Man,
                  AvatarId = 10
                },
                new Customer
                {
                  Id = 17,
                  FirstName = "John",
                  LastName = "Wick",
                  Email = "jWick@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726747",
                  RoleId = 2,
                  Username = "BoogeyMan",
                  IsVerified = true,
                  AddressId = 12,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 19,
                  Gender = Gender.Man,
                  AvatarId = 11
                },
                new Customer
                {
                  Id = 18,
                  FirstName = "Jonhatan",
                  LastName = "Joestar",
                  Email = "jojojojo@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726787",
                  RoleId = 3,
                  Username = "JoJoTop",
                  IsVerified = true,
                  AddressId = 11,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 77,
                  Gender = Gender.Man,
                  AvatarId = 12
                },
                new Customer
                {
                  Id = 19,
                  FirstName = "Dio",
                  LastName = "Brando",
                  Email = "diobrando2022@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726797",
                  RoleId = 3,
                  Username = "BigEvil",
                  IsVerified = true,
                  AddressId = 10,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 43,
                  Gender = Gender.Man,
                  AvatarId = 13
                },
                new Customer
                {
                  Id = 20,
                  FirstName = "Natasha",
                  LastName = "Romanoff",
                  Email = "natRomanoff@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726745",
                  RoleId = 2,
                  Username = "Black Widow",
                  IsVerified = true,
                  AddressId = 12,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 19,
                  Gender = Gender.Woman,
                  AvatarId = 14
                },
                new Customer
                {
                  Id = 21,
                  FirstName = "Pepper",
                  LastName = "Pots",
                  Email = "pepperpots@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726776",
                  RoleId = 2,
                  Username = "Mrs. IronWoman",
                  IsVerified = true,
                  AddressId = 8,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 9,
                  Gender = Gender.Woman,
                  AvatarId = 15
                },
                new Customer
                {
                  Id = 22,
                  FirstName = "Steve",
                  LastName = "Rogers",
                  Email = "captamerica@dir.bg",
                  Password = "Troy1985!",
                  PhoneNumber = "0878726739",
                  RoleId = 2,
                  Username = "Cap",
                  IsVerified = true,
                  AddressId = 12,
                  ConfirmationString = "1312ASDAS80",
                  Rating = 104,
                  Gender = Gender.Man,
                  AvatarId = 16
                } 
            };

            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    ModelId = 7,
                    RegistrationNumber = "CB1122AK",
                    Seats = 4,
                    CustomerId = 1
                },
                new Car
                {
                    Id = 2,
                    ModelId = 22,
                    RegistrationNumber = "A1155MB",
                    Seats = 5,
                    CustomerId = 2,
                },
                new Car
                {
                    Id = 3,
                    ModelId = 4,
                    RegistrationNumber = "BOND007",
                    Seats = 4,
                    CustomerId = 3,
                },
                new Car
                {
                    Id = 4,
                    ModelId = 29,
                    RegistrationNumber = "CB2215BK",
                    Seats = 4,
                    CustomerId = 11
                },
                new Car
                {
                    Id = 5,
                    ModelId = 21,
                    RegistrationNumber = "CB2867AK",
                    Seats = 6,
                    CustomerId = 12
                },
                new Car
                {
                    Id = 6,
                    ModelId = 16,
                    RegistrationNumber = "A115BK",
                    Seats = 4,
                    CustomerId = 13
                },
                new Car
                {
                    Id = 7,
                    ModelId = 19,
                    RegistrationNumber = "CO2266ML",
                    Seats = 5,
                    CustomerId = 14
                },
                new Car
                {
                    Id = 8,
                    ModelId = 26,
                    RegistrationNumber = "B2215MP",
                    Seats = 4,
                    CustomerId = 15
                },
                new Car
                {
                    Id = 9,
                    ModelId = 35,
                    RegistrationNumber = "PB1598BK",
                    Seats = 4,
                    CustomerId = 16
                },
                new Car
                {
                    Id = 10,
                    ModelId = 27,
                    RegistrationNumber = "E1255LK",
                    Seats = 6,
                    CustomerId = 17
                },
                new Car
                {
                    Id = 11,
                    ModelId = 30,
                    RegistrationNumber = "CB5218BK",
                    Seats = 5,
                    CustomerId = 19
                },
                new Car
                {
                    Id = 12,
                    ModelId = 22,
                    RegistrationNumber = "PK1523AM",
                    Seats = 6,
                    CustomerId = 20
                },
                new Car
                {
                    Id = 13,
                    ModelId = 2,
                    RegistrationNumber = "CA1234CA",
                    Seats = 4,
                    CustomerId = 21
                },
                new Car
                {
                    Id = 14,
                    ModelId = 3,
                    RegistrationNumber = "CB8896PM",
                    Seats = 5,
                    CustomerId = 22
                }
            };

            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DriverId = 1,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Now.AddDays(10),
                    FreeSeats = 4,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 2,
                    DriverId = 3,
                    StartAddressId = 10,
                    EndAddressId = 1,
                    DepartureTime = DateTime.Parse("01/12/2022"),
                    FreeSeats = 2,
                    TravelStatus = TravelStatus.Completed
                },
                new Travel
                {
                    Id = 3,
                    DriverId = 2,
                    StartAddressId = 3,
                    EndAddressId = 2,
                    DepartureTime = DateTime.Now.AddDays(15),
                    FreeSeats = 2,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 4,
                    DriverId = 1,
                    StartAddressId = 7,
                    EndAddressId = 9,
                    DepartureTime = DateTime.Now.AddDays(20),
                    FreeSeats = 4,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 5,
                    DriverId = 1,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Parse("11/13/2022"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Travelling
                },
                new Travel
                {
                    Id = 6,
                    DriverId = 3,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Parse("12/14/2022"),
                    FreeSeats = 5,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 7,
                    DriverId = 14,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Now.AddDays(10),
                    FreeSeats = 4,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 8,
                    DriverId = 15,
                    StartAddressId = 10,
                    EndAddressId = 1,
                    DepartureTime = DateTime.Parse("01/12/2022"),
                    FreeSeats = 2,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 9,
                    DriverId = 16,
                    StartAddressId = 3,
                    EndAddressId = 2,
                    DepartureTime = DateTime.Now.AddDays(15),
                    FreeSeats = 2,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 10,
                    DriverId = 20,
                    StartAddressId = 7,
                    EndAddressId = 9,
                    DepartureTime = DateTime.Now.AddDays(20),
                    FreeSeats = 4,
                    TravelStatus = TravelStatus.Travelling
                },
                new Travel
                {
                    Id = 11,
                    DriverId = 21,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Parse("11/13/2022"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Completed
                },
                new Travel
                {
                    Id = 12,
                    DriverId = 22,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Parse("12/14/2022"),
                    FreeSeats = 5,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 13,
                    DriverId = 11,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Now.AddDays(10),
                    FreeSeats = 4,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 14,
                    DriverId = 13,
                    StartAddressId = 10,
                    EndAddressId = 1,
                    DepartureTime = DateTime.Parse("01/12/2022"),
                    FreeSeats = 2,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 15,
                    DriverId = 12,
                    StartAddressId = 3,
                    EndAddressId = 2,
                    DepartureTime = DateTime.Now.AddDays(15),
                    FreeSeats = 2,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 16,
                    DriverId = 1,
                    StartAddressId = 7,
                    EndAddressId = 9,
                    DepartureTime = DateTime.Now.AddDays(20),
                    FreeSeats = 4,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 17,
                    DriverId = 22,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Parse("11/13/2022"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open
                },
                new Travel
                {
                    Id = 18,
                    DriverId = 17,
                    StartAddressId = 1,
                    EndAddressId = 4,
                    DepartureTime = DateTime.Parse("12/14/2022"),
                    FreeSeats = 5,
                    TravelStatus = TravelStatus.Open
                }
            };

            var feedbacks = new List<Feedback>
            {
                new Feedback
                {
                    Id = 1,
                    Rating = 5,
                    TravelId = 2,
                    AuthorId = 5,
                    TargetId = 3
                },
                new Feedback
                {
                    Id = 2,
                    Rating = 5,
                    TravelId = 2,
                    AuthorId = 3,
                    TargetId = 5
                },
                new Feedback
                {
                    Id = 3,
                    Rating = 2,
                    TravelId = 2,
                    AuthorId = 6,
                    TargetId = 3
                },
                new Feedback
                {
                    Id = 4,
                    Rating = 1,
                    TravelId = 2,
                    AuthorId = 3,
                    TargetId = 6
                }
            };

            var feedbackComments = new List<FeedbackComment>
            {
                new FeedbackComment
                {
                    Id = 1,
                    Comment = "Great ride with a great driver!",
                    FeedbackId = 1
                },
                new FeedbackComment
                {
                    Id = 2,
                    Comment = "Great ride with a great driver!",
                    FeedbackId = 2
                },
                new FeedbackComment
                {
                    Id = 3,
                    Comment = "Very rude driver!!!",
                    FeedbackId = 3
                },
                new FeedbackComment
                {
                    Id = 4,
                    Comment = "This person had incredibly stinky legs!!!",
                    FeedbackId = 4
                }
            };

            var travelComments = new List<TravelComment>
            {
                new TravelComment
                {
                    Id = 1,
                    Comment = "No smoking",
                    TravelId = 1
                },
                new TravelComment
                {
                    Id = 2,
                    Comment = "No luggage",
                    TravelId = 2
                },
                new TravelComment
                {
                    Id = 3,
                    Comment = "Smoking is ok",
                    TravelId = 3
                },
                new TravelComment
                {
                    Id = 4,
                    Comment = "Luggage up to 20 kg",
                    TravelId = 4
                },new TravelComment
                {
                    Id = 5,
                    Comment = "No dogs allowed",
                    TravelId = 5
                },
                new TravelComment
                {
                    Id = 6,
                    Comment = "No children",
                    TravelId = 6
                },
            };

            var travelPassengers = new List<TravelPassenger>
            {
                new TravelPassenger
                {
                    Id = 1,
                    PassengerId = 5,
                    TravelId = 2,
                    ApplyStatus = ApplyStatus.Accepted
                },
                new TravelPassenger
                {
                    Id = 2,
                    PassengerId = 6,
                    TravelId = 1,
                    ApplyStatus = ApplyStatus.Applied
                },
                new TravelPassenger
                {
                    Id = 3,
                    PassengerId = 7,
                    TravelId = 3,
                    ApplyStatus = ApplyStatus.Rejected
                },
                new TravelPassenger
                {
                    Id = 4,
                    PassengerId = 8,
                    TravelId = 5,
                    ApplyStatus = ApplyStatus.Accepted
                },
                new TravelPassenger
                {
                    Id = 5,
                    PassengerId = 6,
                    TravelId = 5,
                    ApplyStatus = ApplyStatus.Canceled
                },
                new TravelPassenger
                {
                    Id = 6,
                    PassengerId = 6,
                    TravelId = 2,
                    ApplyStatus = ApplyStatus.Accepted
                },
                new TravelPassenger
                {
                    Id = 7,
                    PassengerId = 6,
                    TravelId = 3,
                    ApplyStatus = ApplyStatus.Accepted
                },
                new TravelPassenger
                {
                    Id = 8,
                    PassengerId = 5,
                    TravelId = 3,
                    ApplyStatus = ApplyStatus.Accepted
                },
            };


            modelBuilder.Entity<Role>().HasData(roles);
            modelBuilder.Entity<Brand>().HasData(brands);
            modelBuilder.Entity<Model>().HasData(models);
            modelBuilder.Entity<Avatar>().HasData(avatars);
            modelBuilder.Entity<Admin>().HasData(admins);
            modelBuilder.Entity<Customer>().HasData(customers);
            modelBuilder.Entity<Car>().HasData(cars);
            modelBuilder.Entity<Travel>().HasData(travels);
            modelBuilder.Entity<Country>().HasData(countries);
            modelBuilder.Entity<City>().HasData(cities);
            modelBuilder.Entity<Address>().HasData(addresses);
            modelBuilder.Entity<Feedback>().HasData(feedbacks);
            modelBuilder.Entity<FeedbackComment>().HasData(feedbackComments);
            modelBuilder.Entity<TravelComment>().HasData(travelComments);
            modelBuilder.Entity<TravelPassenger>().HasData(travelPassengers);
        }
    }
}