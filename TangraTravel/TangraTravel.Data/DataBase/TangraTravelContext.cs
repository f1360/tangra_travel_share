﻿using System.Threading;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using TangraTravel.Data.Models;

namespace TangraTravel.Data.DataBase
{
    public class TangraTravelContext : DbContext
    {
        public TangraTravelContext()
        {

        }
        public TangraTravelContext(DbContextOptions<TangraTravelContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Avatar> Avatars { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Model> Models { get; set; }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<FeedbackComment> FeedbackComments { get; set; }
        public virtual DbSet<Travel> Travels { get; set; }
        public virtual DbSet<TravelComment> TravelComments { get; set; }
        public virtual DbSet<TravelPassenger> TravelPassengers { get; set; }
        public virtual DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Admin>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Customer>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Brand>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Model>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Car>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Country>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<City>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Address>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Feedback>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<FeedbackComment>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Travel>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<TravelComment>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<TravelPassenger>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Role>().HasQueryFilter(x => !x.IsDeleted);

            modelBuilder.Seed();
        }

        /// <summary>
        /// Ovveride SaveChanges for implementing SoftDelete
        /// </summary>
        /// <returns></returns>
        /// <Author>Nikolay Velikov</Author>
        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        /// <summary>
        /// Ovveride SaveChangesAsync for implementing SoftDelete
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <Author>Nikolay Velikov</Author>
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        /// <summary>
        /// Method for validation if some resource in DataBase had been deleted (IsDeleted = true)
        /// </summary>
        /// <Author>Nikolay Velikov</Author>
        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                var type = entry.Entity.GetType().Name;                
                if (type == nameof(Avatar))
                {
                    continue;
                }
                
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }
            }
        }
    }
}