﻿using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class FeedbackComment : Entity
    {
        [Required, MaxLength(255, ErrorMessage = "{0} value must be between and {1} symbols!")]
        public string Comment { get; set; }

        public int FeedbackId { get; set; }
        public Feedback Feedback { get; set; }
    }
}