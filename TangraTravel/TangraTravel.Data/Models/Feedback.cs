﻿using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class Feedback : Entity
    {
        [Range(0, 5, ErrorMessage = "{0} value can be between {2} to {1}")]
        public int Rating { get; set; }

        public FeedbackComment FeedbackComment { get; set; }

        public int TravelId { get; set; }
        public Travel Travel { get; set; }

        public int AuthorId { get; set; }
        public Customer Author { get; set; }

        public int TargetId { get; set; }
        public Customer Target { get; set; }
    }
}