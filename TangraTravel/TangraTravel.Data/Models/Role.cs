﻿using System.Collections.Generic;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class Role : Entity
    {
        public Role()
        {
            this.Admins = new HashSet<Admin>();
            this.Customers = new HashSet<Customer>();
        }
                
        public Access Access { get; set; }

        public ICollection<Admin> Admins { get; set; }

        public ICollection<Customer> Customers { get; set; }
    }
}