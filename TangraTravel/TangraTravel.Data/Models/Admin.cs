﻿using TangraTravel.Data.Enum;
using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class Admin : User
    {
        public Admin()
        {
            this.JobLevel = JobLevel.Junior;
        }

        public JobLevel JobLevel { get; set; }
    }
}