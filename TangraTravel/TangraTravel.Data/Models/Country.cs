﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
   public class Country : Entity
    {
        public Country()
        {
            this.Cities = new HashSet<City>();
        }

        [Required, StringLength(50, MinimumLength = 3, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Name { get; set; }

        public ICollection<City> Cities { get; set; }
    }
}