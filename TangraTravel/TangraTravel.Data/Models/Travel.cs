﻿using System;
using System.Collections.Generic;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class Travel : Entity
    {
        public Travel()
        {
            this.Feedbacks = new HashSet<Feedback>();
            this.TravelPassengers = new HashSet<TravelPassenger>();
        }

        public int DriverId { get; set; }
        public Customer Driver { get; set; }

        public int StartAddressId { get; set; }
        public Address StartAddress { get; set; }

        public int EndAddressId { get; set; }
        public Address EndAddress { get; set; }
        
        public DateTime DepartureTime { get; set; }

        public int FreeSeats { get; set; }
        
        public TravelStatus TravelStatus { get; set; }

        public TravelComment TravelComment { get; set; }

        public ICollection<TravelPassenger> TravelPassengers { get; set; }

        public ICollection<Feedback> Feedbacks { get; set; }
    }
}