﻿using TangraTravel.Data.Enum;
using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class TravelPassenger : Entity
    {
        public int PassengerId { get; set; }
        public Customer Passenger { get; set; }

        public int TravelId { get; set; }
        public Travel Travel { get; set; }
                
        public ApplyStatus ApplyStatus { get; set; }
    }
}