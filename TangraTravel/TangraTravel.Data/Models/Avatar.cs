﻿using System.ComponentModel.DataAnnotations;

namespace TangraTravel.Data.Models
{
    public class Avatar
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(50, MinimumLength = 3, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string FileName { get; set; }

        [Required]
        public string PictureUri { get; set; }

        [Required]
        public string PublicId { get; set; }

        [Required, StringLength(50, MinimumLength = 3, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Alternative { get; set; }

        public Customer Customer { get; set; }

        public Admin Admin { get; set; }
    }
}