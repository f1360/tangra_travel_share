﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class  Model : Entity
    {
        public Model()
        {
            this.Cars = new HashSet<Car>();
        }

        [Required, StringLength(30, MinimumLength = 1, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string BrandModel { get; set; }

        public int BrandId { get; set; }
        public Brand Brand { get; set; }

        public ICollection<Car> Cars { get; set; }
    }
}