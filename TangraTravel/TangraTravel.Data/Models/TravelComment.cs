﻿using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class TravelComment : Entity
    {      
        [Required, MaxLength(255, ErrorMessage = "{0} value cannot be over {1} symbols!")]
        public string Comment { get; set; }

        public int TravelId { get; set; }
        public Travel Travel { get; set; }
    }
}