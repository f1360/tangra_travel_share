﻿using System.ComponentModel.DataAnnotations;

namespace TangraTravel.Data.Models.Abstract
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }

        public bool IsDeleted { get; set; }
    }
}