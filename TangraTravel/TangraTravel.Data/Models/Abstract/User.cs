﻿using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Enum;

namespace TangraTravel.Data.Models.Abstract
{
    public abstract class User : Entity
    {
        protected User()
        {
            this.Gender = Gender.NaN;
        }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string LastName { get; set; }

        public Gender Gender { get; set; }

        [MaxLength(100, ErrorMessage = "Email length must be {1}!"), EmailAddress(ErrorMessage = "Invalid value for {0}")]
        public string Email { get; set; }

        [Required, MinLength(8, ErrorMessage = "Password length must be at least {1} characters!")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Password is invalid!")]
        public string Password { get; set; }

        public int AddressId { get; set; }
        public Address Address { get; set; }

        [StringLength(10, ErrorMessage = "Phone number is invalid!")]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Phone number is invalid!")]
        public string PhoneNumber { get; set; }

        public int RoleId { get; set; }
        public Role Role { get; set; }

        public int? AvatarId { get; set; }
        public Avatar Avatar { get; set; }
    }
}