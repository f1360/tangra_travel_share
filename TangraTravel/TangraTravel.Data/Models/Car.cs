﻿using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class Car : Entity
    {
        public int ModelId { get; set; }
        public Model Model { get; set; }

        [StringLength(10, MinimumLength = 4, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string RegistrationNumber { get; set; }

        [Range(1, 8, ErrorMessage = "Seats can be between 1 to 10 places!")]
        public int Seats { get; set; }

        public int? CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}