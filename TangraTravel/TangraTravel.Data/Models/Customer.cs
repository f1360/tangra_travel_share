﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class Customer : User
    {
        public Customer()
        {
            this.Rating = 0;
            this.IsVerified = false;
            this.IsBlocked = false;
            this.FeedbacksGiven = new HashSet<Feedback>();
            this.FeedbacksReceived = new HashSet<Feedback>();
            this.Travels = new HashSet<Travel>();
            this.TravelsAsPassenger = new HashSet<TravelPassenger>();
        }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Username { get; set; }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string ConfirmationString { get; set; }

        public bool IsVerified { get; set; }

        public bool IsBlocked { get; set; }

        [Range(5, 0, ErrorMessage = "{0} value can be between {2} to {1}")]
        public double Rating { get; set; }

        public Car Car { get; set; }

        public ICollection<Feedback> FeedbacksGiven { get; set; }

        public ICollection<Feedback> FeedbacksReceived { get; set; }

        public ICollection<Travel> Travels { get; set; }

        public ICollection<TravelPassenger> TravelsAsPassenger { get; set; }
    }
}