﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class  Address : Entity
    {
        public Address()
        {
            this.Customers = new HashSet<Customer>();
            this.Admins = new HashSet<Admin>();
            this.StartPointTravels = new HashSet<Travel>();
            this.EndPointTravels = new HashSet<Travel>();
        }

        [Required, StringLength(50, MinimumLength = 3, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string StreetName { get; set; }

        public int CityId { get; set; }
        public City City { get; set; }

        public ICollection<Customer> Customers { get; set; }

        public ICollection<Admin> Admins { get; set; }

        public ICollection<Travel> StartPointTravels { get; set; }

        public ICollection<Travel> EndPointTravels { get; set; }
    }
}