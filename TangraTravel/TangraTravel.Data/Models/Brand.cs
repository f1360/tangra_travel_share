﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class Brand : Entity
    {
        public Brand()
        {
            this.Models = new HashSet<Model>();
        }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Name { get; set; }

        public ICollection<Model> Models { get; set; }
    }
}