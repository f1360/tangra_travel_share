﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Data.Models
{
    public class City : Entity
    {
        public City()
        {
            this.Adresses = new HashSet<Address>();
        }

        [Required, StringLength(50, MinimumLength = 3, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Name { get; set; }
                
        public int CountryId { get; set; }
        public Country Country { get; set; }

        public ICollection<Address> Adresses { get; set; }
    }
}