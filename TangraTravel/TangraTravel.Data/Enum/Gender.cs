﻿namespace TangraTravel.Data.Enum
{
    public enum Gender
    {
        Man,
        Woman,
        NaN
    }
}