﻿namespace TangraTravel.Data.Enum
{
    public enum Access
    {
        Admin,
        Driver,
        Passenger
    }
}