﻿namespace TangraTravel.Data.Enum
{
    public enum ApplyStatus : int
    {
        Applied = 0,
        Accepted = 1,
        Canceled = 2,
        Rejected = 3
    }
}