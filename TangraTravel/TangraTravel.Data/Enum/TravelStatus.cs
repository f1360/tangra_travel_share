﻿namespace TangraTravel.Data.Enum
{
    public enum TravelStatus
    {
        Open,
        Travelling,
        Completed
    }
}