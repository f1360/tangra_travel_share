﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Service;

namespace TangraTravel.Service.Test.AddressServiceTest
{
    [TestClass]
    public class Get_Address
    {
        [TestMethod]
        public async Task Should_Return_PreparedSelectList()
        {
            int expectedId = 1;
            string expectedMessage = "Sofia_Address, Sofia, Bulgaria";
            var coutryMock = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                }
            };
            var citiesMock = new List<City>
            {
                new City
                {
                    Id = 1,
                    CountryId = 1,
                    Country = coutryMock[0],
                    Name = "Sofia"
                }
            };
            var addressesMock = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = 1,
                    City = citiesMock[0],
                    StreetName = "Sofia_Address"
                }
            };

            var countryMock = coutryMock.AsQueryable().BuildMockDbSet();
            var cityMock = citiesMock.AsQueryable().BuildMockDbSet();
            var addressMock = addressesMock.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Countries).Returns(countryMock.Object);
            dbMock.Setup(context => context.Cities).Returns(cityMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressMock.Object);

            var service = new AddressService(dbMock.Object);
            var result = await service.PrepareAddressInformationAsync();
            int actualId = 0;
            string actualMessage = string.Empty;
            foreach (var item in result)
            {
                actualMessage = item.Text;
                actualId = int.Parse(item.Value);
            }

            Assert.AreEqual(expectedId, actualId);
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task Should_Return_PreparedAddressDto()
        {
            string expectedAddressMessage = "Sofia_Address";
            string expectedCityMessage = "Sofia";
            string expectedCountryMessage = "Bulgaria";
            var coutryMock = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                }
            };
            var citiesMock = new List<City>
            {
                new City
                {
                    Id = 1,
                    CountryId = 1,
                    Country = coutryMock[0],
                    Name = "Sofia"
                }
            };
            var addressesMock = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = 1,
                    City = citiesMock[0],
                    StreetName = "Sofia_Address"
                }
            };

            var countryMock = coutryMock.AsQueryable().BuildMockDbSet();
            var cityMock = citiesMock.AsQueryable().BuildMockDbSet();
            var addressMock = addressesMock.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Countries).Returns(countryMock.Object);
            dbMock.Setup(context => context.Cities).Returns(cityMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressMock.Object);

            var service = new AddressService(dbMock.Object);
            var result = await service.GetAddressInformation();
            var ids = new List<int>();
            var actualMessages = new List<string>();
            foreach (var item in result.Addresses)
            {
                actualMessages.Add(item.Text);
                ids.Add(int.Parse(item.Value));
            }
            foreach (var item in result.Cities)
            {
                actualMessages.Add(item.Text);
                ids.Add(int.Parse(item.Value));
            }
            foreach (var item in result.Countires)
            {
                actualMessages.Add(item.Text);
                ids.Add(int.Parse(item.Value));
            }
           

            Assert.AreEqual(1, ids[0]);
            Assert.AreEqual(1, ids[1]);
            Assert.AreEqual(1, ids[2]);
            Assert.AreEqual(expectedAddressMessage, actualMessages[0]);
            Assert.AreEqual(expectedCityMessage, actualMessages[1]);
            Assert.AreEqual(expectedCountryMessage, actualMessages[2]);
        }
    }
}