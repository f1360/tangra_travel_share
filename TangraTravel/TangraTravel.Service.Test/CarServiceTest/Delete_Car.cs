﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Test.CarServiceTest
{
    [TestClass]
    public class Delete_Car
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private ICustomerService customerService;
        private IAvatarService avatarService;

        [TestInitialize]
        public void SetUp()
        {
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "brand"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    Brand = brands[0],
                    BrandId = 1,
                    BrandModel = "model"
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                   Id = 1,
                   CustomerId = 1,
                   Customer =  new Customer
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        Username = "username",
                        Car = new Car
                        {
                           Id = 1,
                           CustomerId = 1,
                           Model = models[0],
                           ModelId = 1,
                           RegistrationNumber = "registration",
                           Seats = 3
                        }
                    },
                   Model = models[0],
                   ModelId = 1,
                   RegistrationNumber = "registration",
                   Seats = 3
                },
                new Car
                {
                   Id = 2,
                   CustomerId = 2,
                   Customer =  new Customer
                    {
                        Id = 1,
                        FirstName = "firstname1",
                        LastName = "lastname1",
                        Username = "username1",
                        Car = new Car
                        {
                           Id = 2,
                           CustomerId = 2,
                           Model = models[0],
                           ModelId = 1,
                           RegistrationNumber = "registration1",
                           Seats = 4
                        }
                    },
                   Model = models[0],
                   ModelId = 1,
                   RegistrationNumber = "registration",
                   Seats = 3
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    TravelStatus = TravelStatus.Open,
                    Driver = new Customer
                             {
                                 Id = 1,
                                 FirstName = "firstname",
                                 LastName = "lastname",
                                 Username = "username",                                  
                             },
                    DriverId = 1,
                    
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    Username = "username"
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    Username = "username1",
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var brandMock = brands.AsQueryable().BuildMockDbSet();
            var modelMock = models.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.Brands).Returns(brandMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelMock.Object);
            dbMock.Setup(context => context.Models).Returns(modelMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetCarAvatar(It.IsAny<string>()))
                             .Returns(new CarAvatarDto { ImageUrl = "fakeUrl", Alt = "fakeAlt" });
            this.avatarService = fakeAvatarService.Object;

            var fakeCustomerService = new Mock<ICustomerService>();
            this.customerService = fakeCustomerService.Object;
        }

        [TestMethod]
        public async Task Should_Delete_Car()
        {
            int customerId = 2;
            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            await service.DeleteAsync(customerId);
            var car = this.fakeDb.Object.Cars.FirstOrDefault(x => x.Id == 2);

            this.fakeDb.Verify(db => db.Cars.Remove(It.IsAny<Car>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.AreEqual(null, car.RegistrationNumber);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CarNotExist()
        {
            string expected = "There is no such car in the system!";
            int customerId = 15;

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.DeleteAsync(customerId));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_DriverHasUnfinishedTravels()
        {
            string expected = "You cannot delete a car, because you still have active travels!";
            int customerId = 1;

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.DeleteAsync(customerId));

            Assert.AreEqual(expected, result.Message);
        }
    }
}