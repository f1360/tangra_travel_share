﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Test.CarServiceTest
{
    [TestClass]
    public class Update_Car
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private ICustomerService customerService;
        private IAvatarService avatarService;

        [TestInitialize]
        public void SetUp()
        {
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "brand"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    Brand = brands[0],
                    BrandId = 1,
                    BrandModel = "model"
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                   Id = 1,
                   CustomerId = 1,
                   Customer =  new Customer
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        Username = "username",
                        Car = new Car
                        {
                           Id = 1,
                           CustomerId = 1,
                           Model = models[0],
                           ModelId = 1,
                           RegistrationNumber = "registration",
                           Seats = 3
                        }
                    },
                   Model = models[0],
                   ModelId = 1,
                   RegistrationNumber = "registration",
                   Seats = 3
                },
                new Car
                {
                   Id = 2,
                   CustomerId = 2,
                   Customer =  new Customer
                    {
                        Id = 1,
                        FirstName = "firstname1",
                        LastName = "lastname1",
                        Username = "username1",
                        Car = new Car
                        {
                           Id = 2,
                           CustomerId = 2,
                           Model = models[0],
                           ModelId = 1,
                           RegistrationNumber = "REGISTRATION1",
                           Seats = 4
                        }
                    },
                   Model = models[0],
                   ModelId = 1,
                   RegistrationNumber = "REGISTRATION1",
                   Seats = 3
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    Username = "username",
                    //Car = new Car
                    //{
                    //   Id = 1,
                    //   CustomerId = 1,
                    //   Model = models[0],
                    //   ModelId = 1,
                    //   RegistrationNumber = "registration",
                    //   Seats = 3
                    //}
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    Username = "username1",
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var brandMock = brands.AsQueryable().BuildMockDbSet();
            var modelMock = models.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.Brands).Returns(brandMock.Object);
            dbMock.Setup(context => context.Models).Returns(modelMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetCarAvatar(It.IsAny<string>())).Returns(new CarAvatarDto { ImageUrl = "fakeUrl", Alt = "fakeAlt" });
            this.avatarService = fakeAvatarService.Object;

            var fakeCustomerService = new Mock<ICustomerService>();
            this.customerService = fakeCustomerService.Object;
        }

        [TestMethod]
        public async Task Get_UpdatedCar()
        {
            string expected = "NEWREGISTER";
            int customerId = 1;

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            var result = await service.UpdateAsync(customerId, new CarUpdateDto { RegistrationNumber = "newRegister" });

            Assert.AreEqual(customerId, result.CustomerId);
            Assert.AreEqual(expected, result.RegistrationNumber);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CarNotExist()
        {
            string expectedMessage = "There is no such car in the system!";

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.UpdateAsync(customerId: 15, new CarUpdateDto { RegistrationNumber = "fake" }));

            Assert.AreEqual(expectedMessage, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_NewRegisterNumberExist()
        {
            string registration = "registration1";
            string expectedMessage = $"Car with registration: {registration.ToUpper()} already exist in database";

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.UpdateAsync(customerId: 1, new CarUpdateDto { RegistrationNumber = registration }));

            Assert.AreEqual(expectedMessage, result.Message);
        }
    }
}