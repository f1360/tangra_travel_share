﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Test.CarServiceTest
{
    [TestClass]
    public class Create_Car
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private ICustomerService customerService;
        private IAvatarService avatarService;

        [TestInitialize]
        public void SetUp()
        {
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "brand"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    Brand = brands[0],
                    BrandId = 1,
                    BrandModel = "model"
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                   Id = 1,
                   CustomerId = 1,
                   Customer =  new Customer
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        Username = "username",
                        Car = new Car
                        {
                           Id = 1,
                           CustomerId = 1,
                           Model = models[0],
                           ModelId = 1,
                           RegistrationNumber = "REGISTRATION",
                           Seats = 3
                        }
                    },
                   Model = models[0],
                   ModelId = 1,
                   RegistrationNumber = "REGISTRATION",
                   Seats = 3
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    Username = "username"
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    Username = "username1",
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var brandMock = brands.AsQueryable().BuildMockDbSet();
            var modelMock = models.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.Brands).Returns(brandMock.Object);
            dbMock.Setup(context => context.Models).Returns(modelMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);

            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetCarAvatar(It.IsAny<string>()))
                             .Returns(new CarAvatarDto { ImageUrl = "fakeUrl", Alt = "fakeAlt" });
            this.avatarService = fakeAvatarService.Object;

            var fakeCustomerService = new Mock<ICustomerService>();
            this.customerService = fakeCustomerService.Object;
        }

        [TestMethod]
        [DataRow("regisTration123", 4)]
        public async Task Should_Create_Car(string registrationNumber, int seats)
        {
            //Link: https://stackoverflow.com/questions/49142556/how-do-i-mock-addasync
            int customerId = 2;
            var inputs = new CarCreateDto
            {
                ModelId = 1,
                RegistrationNumber = registrationNumber,
                Seats = seats
            };

            var cars = new List<Car>();
            this.fakeDb.Setup(x => x.Cars.AddAsync(It.IsAny<Car>(), It.IsAny<CancellationToken>()))
                .Callback((Car car, CancellationToken token) => { cars.Add(car); });

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService); 
            await service.CreateAsync(customerId, inputs);            

            this.fakeDb.Verify(db => db.Cars.AddAsync(It.IsAny<Car>(), It.IsAny<CancellationToken>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.AreEqual(registrationNumber.ToUpper(), cars[0].RegistrationNumber);
            Assert.AreEqual(seats, cars[0].Seats);
            Assert.AreEqual(customerId, cars[0].CustomerId);
            Assert.AreEqual(1, cars[0].ModelId);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_RegistrationNumberAlreadyExists()
        {
            string expected = "Car with registration: REGISTRATION already exist in database";
            int customerId = 2;
            var inputs = new CarCreateDto
            {
                ModelId = 1,
                RegistrationNumber = "REGISTRATION",
                Seats = 4
            };

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.CreateAsync(customerId, inputs));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_ModelNotExists()
        {
            string expected = "Selected model does not exist in database!";
            int customerId = 2;
            var inputs = new CarCreateDto
            {
                ModelId = 15,
                RegistrationNumber = "newRegister",
                Seats = 4
            };

            var service = new CarService(this.fakeDb.Object, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.CreateAsync(customerId, inputs));

            Assert.AreEqual(expected, result.Message);
        }
    }
}