﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Test.CarServiceTest
{
    [TestClass]
    public class Get_Car
    {
        private TangraTravelContext fakeDb;
        private ICustomExpression func = new CustomExpression();
        private ICustomerService customerService;
        private IAvatarService avatarService;

        [TestInitialize]
        public void SetUp()
        {
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "brand"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    Brand = brands[0],
                    BrandId = 1,
                    BrandModel = "model"
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                   Id = 1,
                   CustomerId = 1,
                   Customer =  new Customer
                    {
                        Id = 1,
                        FirstName = "firstname",
                        LastName = "lastname",
                        Username = "username",
                        Car = new Car
                        {
                           Id = 1,
                           CustomerId = 1,
                           Model = models[0],
                           ModelId = 1,
                           RegistrationNumber = "registration",
                           Seats = 3
                        }
                    },
                   Model = models[0],
                   ModelId = 1,
                   RegistrationNumber = "registration",
                   Seats = 3
                },
                new Car
                {
                   Id = 2,
                   CustomerId = 2,
                   Customer =  new Customer
                    {
                        Id = 1,
                        FirstName = "firstname1",
                        LastName = "lastname1",
                        Username = "username1",
                        Car = new Car
                        {
                           Id = 2,
                           CustomerId = 2,
                           Model = models[0],
                           ModelId = 1,
                           RegistrationNumber = "registration1",
                           Seats = 4
                        }
                    },
                   Model = models[0],
                   ModelId = 1,
                   RegistrationNumber = "registration",
                   Seats = 3
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    Username = "username",
                    //Car = new Car
                    //{
                    //   Id = 1,
                    //   CustomerId = 1,
                    //   Model = models[0],
                    //   ModelId = 1,
                    //   RegistrationNumber = "registration",
                    //   Seats = 3
                    //}
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    Username = "username1",
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var brandMock = brands.AsQueryable().BuildMockDbSet();
            var modelMock = models.AsQueryable().BuildMockDbSet();
            var carMock = cars.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.Brands).Returns(brandMock.Object);
            dbMock.Setup(context => context.Models).Returns(modelMock.Object);
            dbMock.Setup(context => context.Cars).Returns(carMock.Object);
            this.fakeDb = dbMock.Object;


            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetCarAvatar(It.IsAny<string>())).Returns(new CarAvatarDto { ImageUrl = "fakeUrl", Alt = "fakeAlt" });
            this.avatarService = fakeAvatarService.Object;

            var fakeCustomerService = new Mock<ICustomerService>();
            this.customerService = fakeCustomerService.Object;
        }

        [TestMethod]
        public async Task Get_CarOfSelectedUser()
        {
            int customerId = 1;
            int carId = 1;
            string brand = "brand";
            string model = "model";
            string registration = "registration";
            int seats = 3;
            string owner = $"Full Name: firstname lastname, Username: username";
            string avatarUrl = "fakeUrl";
            string alt = "fakeAlt";

            var service = new CarService(this.fakeDb, this.func, this.customerService, this.avatarService);

            var result = await service.GetByUerIdAsync(customerId);
            Assert.AreEqual(customerId, result.CustomerId);
            Assert.AreEqual(carId, result.CarId);
            Assert.AreEqual(brand, result.Brand);
            Assert.AreEqual(model, result.Model);
            Assert.AreEqual(registration, result.RegistrationNumber);
            Assert.AreEqual(seats, result.Seats);
            Assert.AreEqual(owner, result.Owner);
            Assert.AreEqual(avatarUrl, result.Avatar.ImageUrl);
            Assert.AreEqual(alt, result.Avatar.Alt);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CarIsNotBelongToCustomer()
        {
            string expected = "There is no such car in the system!";

            var service = new CarService(this.fakeDb, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.GetByUerIdAsync(5));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Get_CarOfSelectedCarId()
        {
            int customerId = 1;
            int carId = 1;
            string brand = "brand";
            string model = "model";
            string registration = "registration";
            int seats = 3;
            string owner = $"Full Name: firstname lastname, Username: username";
            string avatarUrl = "fakeUrl";
            string alt = "fakeAlt";

            var service = new CarService(this.fakeDb, this.func, this.customerService, this.avatarService);
            var result = await service.GetByCarIdAsync(carId);

            Assert.AreEqual(customerId, result.CustomerId);
            Assert.AreEqual(carId, result.CarId);
            Assert.AreEqual(brand, result.Brand);
            Assert.AreEqual(model, result.Model);
            Assert.AreEqual(registration, result.RegistrationNumber);
            Assert.AreEqual(seats, result.Seats);
            Assert.AreEqual(owner, result.Owner);
            Assert.AreEqual(avatarUrl, result.Avatar.ImageUrl);
            Assert.AreEqual(alt, result.Avatar.Alt);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CarNotExist()
        {
            string expected = "There is no such car in the system!";

            var service = new CarService(this.fakeDb, this.func, this.customerService, this.avatarService);
            var result = await Assert.ThrowsExceptionAsync<CarException>(() => service.GetByCarIdAsync(5));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        [DataRow(1, 1, 1)]
        [DataRow(2, 1, 5)]
        [DataRow(2, 1, null)]
        public async Task Get_CarsByPage(int count, int page, int? perPage)
        {
            var service = new CarService(this.fakeDb, this.func, this.customerService, this.avatarService);
            var result = await service.GetByPageAsync(page, perPage);

            Assert.AreEqual(count, result.Count);
        }

        [TestMethod]
        [DataRow(2, 1)]
        [DataRow(1, 5)]
        public async Task Get_MaxPage(int count, int byPage)
        {
            var service = new CarService(this.fakeDb, this.func, this.customerService, this.avatarService);
            int maxPage = await service.MaxPage(byPage);

            Assert.AreEqual(count, maxPage);
        }

        [TestMethod]
        public async Task Get_BrandsAndModels_In_SelectedList()
        {
            var service = new CarService(this.fakeDb, this.func, this.customerService, this.avatarService);
            var result = await service.TakeBrandsAndModelsAsync();

            Assert.AreEqual(1, result.Count());
        }
    }
}