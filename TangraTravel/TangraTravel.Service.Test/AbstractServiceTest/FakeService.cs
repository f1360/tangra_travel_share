﻿using System.Threading.Tasks;

using TangraTravel.Data.DataBase;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Service.Abstract;

namespace TangraTravel.Service.Test.AbstractServiceTest
{
    public class FakeService : AbstractService
    {
        public FakeService(TangraTravelContext db)
            : base(db)
        {
        }

        public async Task ValidationUniquePropertiesTestAsync(bool isAdmin, string email, string phone, string username = null)
        {
            await this.ValidationUniquePropertiesAsync(isAdmin, email, phone, username);
        }

        public string RemoveSpaceTest(string input)
        {
            return this.RemoveSpaces(input);
        }

        public void ValidationForLogInTest(User user, string password)
        {
            this.ValidationForLogIn(user, password);
        }

        public void UserExistTest<T>(T user, bool logIn = false)
        {
            this.UserExist(user, logIn);
        }

        public void CarIsNullTest<T>(T car)
        {
            this.CarIsNull(car);
        }

        public void TravelExistsTest<T>(T travel)
        {
            this.TravelExists(travel);
        }

        public void TravelPassengerExistTest<T>(T travelPassenger)
        {
            this.TravelPassengerExist(travelPassenger);
        }
    }
}