﻿using System.Linq;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Exeception.TravelPassenger;

namespace TangraTravel.Service.Test.AbstractServiceTest
{
    [TestClass]
    public class Validation_AdditinalMethods
    {
        private TangraTravelContext fakeDb = null;

        [TestInitialize]
        public void SetUp()
        {
            var admins = new List<Admin>
            {
                new Admin
                {
                    Email = "fakeAdmin1@tangratravel.com",
                    PhoneNumber = "fakePhone1",
                },
                new Admin
                {
                    Email = "fakeAdmin2@tangratravel.com",
                    PhoneNumber = "fakePhone2",
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Email = "fakeCustomer1",
                    Username = "fakeCustomer1",
                    PhoneNumber = "fakePhone1",
                },
                new Customer
                {
                    Email = "fakeCustomer2",
                    Username = "fakeCustomer2",
                    PhoneNumber = "fakePhone2",
                }
            };

            var adminsMock = admins.AsQueryable().BuildMockDbSet();
            var customersMock = customers.AsQueryable().BuildMockDbSet();
            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Admins).Returns(adminsMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customersMock.Object);

            this.fakeDb = dbMock.Object;
        }

        [TestMethod]
        public void Should_Get_Input_WithoutEmptySpaceInSide_In_RemoveSpace()
        {
            string expected = "full";
            var service = new FakeService(this.fakeDb);

            string actual = service.RemoveSpaceTest(expected);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Should_Pass_UserExist()
        {
            var service = new FakeService(this.fakeDb);
            User user = new Customer();
            service.UserExistTest(user);
        }

        [TestMethod]
        public void Should_ThrowException_When_UserIsNull_UserExist()
        {
            var expected = "There is no user in the system!";
            var service = new FakeService(this.fakeDb);

            var result = Assert.ThrowsException<UserNotExistException>(() => service.UserExistTest<User>(null));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public void Should_ThrowException_When_EmailIsIncorrent_UserExist()
        {
            var expected = "There is no user with such email!";
            var service = new FakeService(this.fakeDb);

            var result = Assert.ThrowsException<EmailIncorrectException>(() => service.UserExistTest<User>(null, logIn: true));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public void Should_Pass_ValidationForLogIn()
        {
            var service = new FakeService(this.fakeDb);
            var customer = new Customer()
            {
                Password = "password"
            };
            service.ValidationForLogInTest(customer, password: customer.Password);
        }

        [TestMethod]
        public void Should_ThrowException_When_PasswordIsIncorect_VaslidationForLogIn()
        {
            var expected = "Password is incorrect!";
            var service = new FakeService(this.fakeDb);
            var customer = new Customer()
            {
                Password = "password"
            };

            var result = Assert.ThrowsException<PasswordIncorrectException>(() => service.ValidationForLogInTest(customer, password: "wrong"));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public void Should_Pass_CarIsNull()
        {
            var service = new FakeService(this.fakeDb);
            
            service.CarIsNullTest(new Car());
        }
        
        [TestMethod]
        public void Should_ThrowException_When_CarIsNull_In_CarIsNullMethod()
        {
            var expected = "There is no such car in the system!";
            var service = new FakeService(this.fakeDb);           

            var result = Assert.ThrowsException<CarException>(() => service.CarIsNullTest<Car>(null));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public void Should_Pass_TravelExist()
        {
            var service = new FakeService(this.fakeDb);

            service.TravelExistsTest(new Travel());
        }

        [TestMethod]
        public void Should_ThrowException_When_TravelIsNull_TravelExist()
        {
            var expected = "There is no such travel in the system!";
            var service = new FakeService(this.fakeDb);

            var result = Assert.ThrowsException<TravelDoesNotExistException>(() => service.TravelExistsTest<Travel>(null));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public void Should_Pass_TravelPassengerExist()
        {
            var service = new FakeService(this.fakeDb);

            service.TravelPassengerExistTest(new TravelPassenger());
        }

        [TestMethod]
        public void Should_ThrowException_When_TravelPassengerIsNull_TravelExist()
        {
            var expected = "There is no such travelPassenger record in the system!";
            var service = new FakeService(this.fakeDb);

            var result = Assert.ThrowsException<TravelPassengerDoesNotExistException>(() => service.TravelPassengerExistTest<TravelPassenger>(null));

            Assert.AreEqual(expected, result.Message);
        }
    }
}