﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Exeception.User;

namespace TangraTravel.Service.Test.AbstractServiceTest
{

    [TestClass]
    public class Validation_UniqueProperiesMethod
    {
        private TangraTravelContext fakeDb = null;

        [TestInitialize]
        public void SetUp()
        {
            var admins = new List<Admin>
            {
                new Admin
                {
                    Email = "fakeAdmin1@tangratravel.com",
                    PhoneNumber = "fakePhone1",
                },
                new Admin
                {
                    Email = "fakeAdmin2@tangratravel.com",
                    PhoneNumber = "fakePhone2",
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Email = "fakeCustomer1",
                    Username = "fakeCustomer1",
                    PhoneNumber = "fakePhone1",
                },
                new Customer
                {
                    Email = "fakeCustomer2",
                    Username = "fakeCustomer2",
                    PhoneNumber = "fakePhone2",
                }
            };

            var adminsMock = admins.AsQueryable().BuildMockDbSet();
            var customersMock = customers.AsQueryable().BuildMockDbSet();
            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Admins).Returns(adminsMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customersMock.Object);

            this.fakeDb = dbMock.Object;
        }

        [TestMethod]
        [DataRow(false, "email", "username")]
        [DataRow(true, "@tangratravel.com", null)]
        public async Task Method_Pass_WithoutException(bool isAdmin, string email, string username)
        {
            var service = new FakeService(this.fakeDb);
            await service.ValidationUniquePropertiesTestAsync(isAdmin, email, "phone", username);
        }

        [TestMethod]
        public async Task Method_ThrowExeption_When_EmailIsWrong_IsAdmin()
        {
            string expectedMessage = "Corporative email must be with domain: @tangratravel.com";
            var service = new FakeService(this.fakeDb);
            var a = await Assert.ThrowsExceptionAsync<InputExistException>(() => 
             service.ValidationUniquePropertiesTestAsync(true,"email","phone"));

            Assert.AreEqual(expectedMessage, a.Message);
        }

        [TestMethod]
        [DataRow("fakeAdmin1@tangratravel.com")]
        [DataRow("fakeAdmin2@tangratravel.com")]
        public async Task Method_ThrowExeption_When_EmailExistInDatabase_IsAdmin(string email)
        {
            string expectedMessage = "Email exist in the system!";
            var service = new FakeService(this.fakeDb);
            var a = await Assert.ThrowsExceptionAsync<InputExistException>(() =>
             service.ValidationUniquePropertiesTestAsync(true, email, "phone"));

            Assert.AreEqual(expectedMessage, a.Message);
        }

        [TestMethod]
        [DataRow("fakePhone1")]
        [DataRow("fakePhone2")]
        public async Task Method_ThrowExeption_When_PhoneExistInDatabase_IsAdmin(string phone)
        {
            string expectedMessage = "Phone exist in the system!";
            var service = new FakeService(this.fakeDb);
            var a = await Assert.ThrowsExceptionAsync<InputExistException>(() =>
             service.ValidationUniquePropertiesTestAsync(true, null, phone));

            Assert.AreEqual(expectedMessage, a.Message);
        }

        [TestMethod]
        [DataRow("fake@tangratravel.com")]
        public async Task Method_ThrowExeption_When_EmailFinishAsCompanyDomainInDatabase_IsCustomer(string email)
        {
            string expectedMessage = "Email is with corporative domain!";
            var service = new FakeService(this.fakeDb);
            var a = await Assert.ThrowsExceptionAsync<InputExistException>(() =>
             service.ValidationUniquePropertiesTestAsync(false, email,"phone"));

            Assert.AreEqual(expectedMessage, a.Message);
        }

        [TestMethod]
        [DataRow("fakeCustomer1")]
        [DataRow("fakeCustomer2")]
        public async Task Method_ThrowExeption_When_EmailExistInDatabase_IsCustomer(string email)
        {
            string expectedMessage = "Email exist in the system!";
            var service = new FakeService(this.fakeDb);
            var a = await Assert.ThrowsExceptionAsync<InputExistException>(() =>
             service.ValidationUniquePropertiesTestAsync(false, email, "phone"));

            Assert.AreEqual(expectedMessage, a.Message);
        }

        [TestMethod]
        [DataRow("fakeCustomer1")]
        [DataRow("fakeCustomer2")]
        public async Task Method_ThrowExeption_When_UsernameExistInDatabase_IsCustomer(string username)
        {
            string expectedMessage = "Username exist in the system!";
            var service = new FakeService(this.fakeDb);
            var a = await Assert.ThrowsExceptionAsync<InputExistException>(() =>
             service.ValidationUniquePropertiesTestAsync(false, null, null, username));

            Assert.AreEqual(expectedMessage, a.Message);
        }

        [TestMethod]
        [DataRow("fakePhone1")]
        [DataRow("fakePhone2")]
        public async Task Method_ThrowExeption_When_PhoneNumberExistInDatabase_IsCustomer(string phone)
        {
            string expectedMessage = "Phone exist in the system!";
            var service = new FakeService(this.fakeDb);
            var a = await Assert.ThrowsExceptionAsync<InputExistException>(() =>
             service.ValidationUniquePropertiesTestAsync(false, null, phone, null));

            Assert.AreEqual(expectedMessage, a.Message);
        }
    }
}