﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Mapper.Contracts;

namespace TangraTravel.Service.Test.TravelServiceTest
{
    [TestClass]
    public class Delete_Travel
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private Mock<ICustomerService> customerService;

        [TestInitialize]
        public void SetUp()
        {
            var countries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                },
                new City
                {
                    Id = 2,
                    Name = "Sofia",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = cities[0].Id,
                    City = cities[0],
                    StreetName = cities[0].Name + "_Street"
                },
                new Address
                {
                    Id = 2,
                    CityId = cities[1].Id,
                    City = cities[1],
                    StreetName = cities[1].Name + "_Street"
                }
            };
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "BMW"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    BrandModel = "X7 40i xDrive",
                    BrandId = brands[0].Id,
                    Brand = brands[0],
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    RegistrationNumber = "registration1",
                    Seats = 4,
                    ModelId = models[0].Id,
                    Model = models[0]
                },
                new Car
                {
                    Id = 2,
                    RegistrationNumber = "registration2",
                    Seats = 4,
                    ModelId = models[0].Id,
                    Model = models[0]
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "driverFirstName",
                    LastName = "driverLastName",
                    Car = cars[0],
                    Username = "driverUsername",
                    PhoneNumber = "driverPhone"
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "driverFirstName2",
                    LastName = "driverLastName2",
                    Car = cars[1],
                    Username = "driverUsername2",
                    PhoneNumber = "driverPhone2"
                },
                new Customer
                {
                    Id = 3,
                    FirstName = "passengerFirstName",
                    LastName = "passengerLastName",
                    Username = "passengerUsername",
                    PhoneNumber = "passengerPhone"
                }
            };
            var travelComments = new List<TravelComment>
            {
                new TravelComment
                {
                    Id = 1,
                    Comment = "Not smoke",
                    TravelId = 1
                },
                new TravelComment
                {
                    Id = 2,
                    Comment = "Not smoke and lugage",
                    TravelId = 2
                },
                new TravelComment
                {
                    Id = 3,
                    Comment = "Not smoke and lugage",
                    TravelId = 3
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date,
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Travelling,
                    TravelComment = travelComments[0],
                },
                new Travel
                {
                    Id = 2,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Parse("08/11/2020"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Completed,
                    TravelComment = travelComments[1],
                },
                new Travel
                {
                    Id = 3,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Parse("08/11/2020"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },
                new Travel
                {
                    Id = 4,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(50),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },

                new Travel
                {
                    Id = 5,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(50),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },
                new Travel
                {
                    Id = 6,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddMinutes(50),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>
                    {
                        new TravelPassenger
                        {
                            Id = 1,
                            ApplyStatus = ApplyStatus.Accepted,
                            TravelId = 6,
                            PassengerId = customers[2].Id,
                            Passenger = customers[2],
                        }
                    }
                },
                new Travel
                {
                    Id = 7,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddMonths(3),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },
                new Travel
                {
                    Id = 8,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(21),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },
                new Travel
                {
                    Id = 9,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(16),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },
                new Travel
                {
                    Id = 10,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(3),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },
                new Travel
                {
                    Id = 11,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddMonths(1),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                },
                new Travel
                {
                    Id = 12,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[1].Id,
                    StartAddress = addresses[1],
                    EndAddressId = addresses[0].Id,
                    EndAddress = addresses[0],
                    DepartureTime = DateTime.Now.Date.AddDays(19),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2]
                }
            };

            var contriesMock = countries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var brandsMock = brands.AsQueryable().BuildMockDbSet();
            var carsMock = cars.AsQueryable().BuildMockDbSet();
            var modelsMock = models.AsQueryable().BuildMockDbSet();
            var customersMock = customers.AsQueryable().BuildMockDbSet();
            var travelCommentsMock = travelComments.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();

            var mockDb = new Mock<TangraTravelContext>();
            mockDb.Setup(context => context.Countries).Returns(contriesMock.Object);
            mockDb.Setup(context => context.Cities).Returns(citiesMock.Object);
            mockDb.Setup(context => context.Addresses).Returns(addressesMock.Object);
            mockDb.Setup(context => context.Brands).Returns(brandsMock.Object);
            mockDb.Setup(context => context.Models).Returns(modelsMock.Object);
            mockDb.Setup(context => context.Cars).Returns(carsMock.Object);
            mockDb.Setup(context => context.Customers).Returns(customersMock.Object);
            mockDb.Setup(context => context.TravelComments).Returns(travelCommentsMock.Object);
            mockDb.Setup(context => context.Travels).Returns(travelMock.Object);

            this.fakeDb = mockDb;

            var mockCustomerService = new Mock<ICustomerService>();
            this.customerService = mockCustomerService;
        }

        [TestMethod]
        public async Task Delete_SelectedTravel()
        {
            int travelId = 1;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            await service.DeleteTravelAsync(travelId);

            this.fakeDb.Verify(db => db.Travels.Remove(It.IsAny<Travel>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TravelNotExist()
        {
            string expected = "There is no such travel in the system!";

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await Assert.ThrowsExceptionAsync<TravelDoesNotExistException>(() => service.DeleteTravelAsync(500));

            Assert.AreEqual(expected, result.Message);
        }
    }
}