﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.Service.Models.TravelCommentDtos;

namespace TangraTravel.Service.Test.TravelServiceTest
{
    [TestClass]
    public class Create_Travel
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private Mock<ICustomerService> customerService;

        [TestInitialize]
        public void SetUp()
        {
            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Access = Access.Driver
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Passenger
                }
            };
            var countries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                },
                new City
                {
                    Id = 2,
                    Name = "Sofia",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = cities[0].Id,
                    City = cities[0],
                    StreetName = cities[0].Name + "_Street"
                },
                new Address
                {
                    Id = 2,
                    CityId = cities[1].Id,
                    City = cities[1],
                    StreetName = cities[1].Name + "_Street"
                }
            };
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "BMW"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    BrandModel = "X7 40i xDrive",
                    BrandId = brands[0].Id,
                    Brand = brands[0],
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    RegistrationNumber = "registration1",
                    Seats = 5,
                    ModelId = models[0].Id,
                    Model = models[0]
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "driverFirstName",
                    LastName = "driverLastName",
                    Email = "user@fake.bg",
                    Username = "driverUsername",
                    PhoneNumber = "driverPhone",
                    RoleId = roles[0].Id,
                    Role = roles[0],
                    Car = cars[0]
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "driverFirstName2",
                    LastName = "driverLastName2",
                    Email = "passenger@fake.bg",
                    Username = "driverUsername2",
                    PhoneNumber = "driverPhone2",
                    RoleId = roles[1].Id,
                    Role = roles[1]
                },
                new Customer
                {
                    Id = 3,
                    FirstName = "passengerFirstName",
                    LastName = "passengerLastName",
                    Username = "passengerUsername",
                    PhoneNumber = "passengerPhone"
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 0,
                    Driver = customers[0],
                    StartAddress = addresses[0],
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date,
                    FreeSeats = 1,
                    TravelStatus = TravelStatus.Completed,
                    TravelComment = new TravelComment
                    {
                        Comment = "fake"
                    }
                }
            };

            var rolesMock = roles.AsQueryable().BuildMockDbSet();
            var contriesMock = countries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var brandsMock = brands.AsQueryable().BuildMockDbSet();
            var carsMock = cars.AsQueryable().BuildMockDbSet();
            var modelsMock = models.AsQueryable().BuildMockDbSet();
            var customersMock = customers.AsQueryable().BuildMockDbSet();
            var travelsMock = travels.AsQueryable().BuildMockDbSet();

            var mockDb = new Mock<TangraTravelContext>();
            mockDb.Setup(context => context.Roles).Returns(rolesMock.Object);
            mockDb.Setup(context => context.Countries).Returns(contriesMock.Object);
            mockDb.Setup(context => context.Cities).Returns(citiesMock.Object);
            mockDb.Setup(context => context.Addresses).Returns(addressesMock.Object);
            mockDb.Setup(context => context.Brands).Returns(brandsMock.Object);
            mockDb.Setup(context => context.Models).Returns(modelsMock.Object);
            mockDb.Setup(context => context.Cars).Returns(carsMock.Object);
            mockDb.Setup(context => context.Customers).Returns(customersMock.Object);
            mockDb.Setup(context => context.Travels).Returns(travelsMock.Object);
             
            this.fakeDb = mockDb;

            var mockCustomerService = new Mock<ICustomerService>();
            mockCustomerService.Setup(x => x.GetCarSeats(It.IsAny<int>())).ReturnsAsync(5);
            this.customerService = mockCustomerService;
        }

        [TestMethod]
        public async Task Create_TravelInSystem()
        {
            var travel = new Travel();
            var comment = new TravelComment();
            this.fakeDb.Setup(x => x.Travels.AddAsync(It.IsAny<Travel>(), It.IsAny<CancellationToken>()))
                .Callback((Travel t, CancellationToken token) =>
                {
                    travel.DriverId = t.DriverId;
                    travel.StartAddressId = t.StartAddressId;
                    travel.EndAddressId = t.EndAddressId;
                    travel.DepartureTime = t.DepartureTime;
                    travel.TravelStatus = t.TravelStatus;
                    travel.FreeSeats = t.FreeSeats;
                });
            this.fakeDb.Setup(x => x.TravelComments.AddAsync(It.IsAny<TravelComment>(), It.IsAny<CancellationToken>()))
                .Callback((TravelComment tc, CancellationToken token) =>
                {
                    comment.Comment = tc.Comment;
                    comment.TravelId = tc.TravelId;
                });

            DateTime time = DateTime.Now.Date;
            var input = new TravelCreateDto
            {
                DriverId = 1,
                StartAddressId = 1,
                EndAddressId = 2,
                DepartureTime = time.ToString("d"),
                Comment = "You cannot smoke in the car and bring pets"
            };

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);           
            var result = await service.CreateTravelAsync(input);

            this.fakeDb.Verify(db => db.Travels.AddAsync(It.IsAny<Travel>(), It.IsAny<CancellationToken>()), Times.Once);
            this.fakeDb.Verify(db => db.TravelComments.AddAsync(It.IsAny<TravelComment>(), It.IsAny<CancellationToken>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Exactly(2));

            Assert.IsInstanceOfType(result, typeof(TravelPresentDto));
            Assert.IsInstanceOfType(result.Comment, typeof(TravelCommentPresentDto));
            Assert.AreEqual(input.DriverId, travel.DriverId);
            Assert.AreEqual(input.StartAddressId, travel.StartAddressId);
            Assert.AreEqual(input.EndAddressId, travel.EndAddressId);
            Assert.AreEqual(time, travel.DepartureTime);
            Assert.AreEqual(TravelStatus.Open, travel.TravelStatus);
            Assert.AreEqual(5, travel.FreeSeats);
            Assert.AreEqual(input.Comment, comment.Comment);
        }
    }
}