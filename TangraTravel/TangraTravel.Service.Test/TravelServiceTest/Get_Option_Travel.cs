﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.TravelDtos;

namespace TangraTravel.Service.Test.TravelServiceTest
{
    [TestClass]
    public class Get_Option_Travel
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private Mock<ICustomerService> customerService;

        [TestInitialize]
        public void SetUp()
        {
            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Access = Access.Driver
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Passenger
                }
            };
            var countries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                },
                new City
                {
                    Id = 2,
                    Name = "Sofia",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = cities[0].Id,
                    City = cities[0],
                    StreetName = cities[0].Name + "_Street"
                },
                new Address
                {
                    Id = 2,
                    CityId = cities[1].Id,
                    City = cities[1],
                    StreetName = cities[1].Name + "_Street"
                }
            };
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "BMW"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    BrandModel = "X7 40i xDrive",
                    BrandId = brands[0].Id,
                    Brand = brands[0],
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    RegistrationNumber = "registration1",
                    Seats = 4,
                    ModelId = models[0].Id,
                    Model = models[0]
                },
                new Car
                {
                    Id = 2,
                    RegistrationNumber = "registration2",
                    Seats = 4,
                    ModelId = models[0].Id,
                    Model = models[0]
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "driverFirstName",
                    LastName = "driverLastName",
                    Car = cars[0],
                    Username = "driverUsername",
                    PhoneNumber = "driverPhone",
                    RoleId = roles[0].Id,
                    Role = roles[0],
                    FeedbacksGiven = new List<Feedback>
                    {
                        new Feedback
                        {
                            AuthorId = 1,
                            TargetId = 2,
                            TravelId = 3
                        }
                    }
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "driverFirstName2",
                    LastName = "driverLastName2",
                    Car = cars[1],
                    Username = "driverUsername2",
                    PhoneNumber = "driverPhone2",
                    RoleId = roles[1].Id,
                    Role = roles[1],
                    FeedbacksGiven = new List<Feedback>
                    {
                        new Feedback
                        {
                            AuthorId = 2,
                            TargetId = 1,
                            TravelId = 3
                        }
                    }
                },
                new Customer
                {
                    Id = 3,
                    FirstName = "driverFirstName3",
                    LastName = "driverLastName3",
                    Username = "driverUsername3",
                    PhoneNumber = "driverPhone3"
                },
                new Customer
                {
                    Id = 4,
                    FirstName = "driverFirstName4",
                    LastName = "driverLastName4",
                    Username = "driverUsername4",
                    PhoneNumber = "driverPhone4"
                }
            };
            var travelComments = new List<TravelComment>
            {
                new TravelComment
                {
                    Id = 1,
                    Comment = "Not smoke",
                    TravelId = 1
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Parse("08/11/2020"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Completed,
                    TravelComment = travelComments[0],
                    TravelPassengers = new List<TravelPassenger>
                    {
                        new TravelPassenger
                        {
                            Id = 1,
                            ApplyStatus = ApplyStatus.Accepted,
                            TravelId = 1,
                            PassengerId = customers[1].Id,
                            Passenger = customers[1],
                        }
                    }
                },
                new Travel
                {
                    Id = 2,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[1].Id,
                    StartAddress = addresses[1],
                    EndAddressId = addresses[0].Id,
                    EndAddress = addresses[0],
                    DepartureTime = DateTime.Parse("08/05/2020"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Completed,
                    TravelComment = travelComments[0],
                    TravelPassengers = new List<TravelPassenger>
                    {
                        new TravelPassenger
                        {
                            Id = 1,
                            ApplyStatus = ApplyStatus.Accepted,
                            TravelId = 1,
                            PassengerId = customers[0].Id,
                            Passenger = customers[0],
                        }
                    }
                },
                new Travel
                {
                    Id = 3,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[1].Id,
                    StartAddress = addresses[1],
                    EndAddressId = addresses[0].Id,
                    EndAddress = addresses[0],
                    DepartureTime = DateTime.Parse("08/05/2019"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Completed,
                    TravelComment = travelComments[0],
                    TravelPassengers = new List<TravelPassenger>
                    {
                        new TravelPassenger
                        {
                            Id = 1,
                            ApplyStatus = ApplyStatus.Accepted,
                            TravelId = 1,
                            PassengerId = customers[1].Id,
                            Passenger = customers[1],
                        }
                    }
                }
            };
            var feedbacks = new List<Feedback>
            {
                new Feedback
                {
                    Id = 1,
                    AuthorId = customers[1].Id,
                    Author = customers[1],
                    TargetId = customers[0].Id,
                    Target = customers[0],
                    TravelId = 3
                },
                new Feedback
                {
                    Id = 2,
                    AuthorId = customers[0].Id,
                    Author = customers[0],
                    TargetId = customers[3].Id,
                    Target = customers[3],
                    TravelId = 3
                }
            };
            var travelPassengers = new List<TravelPassenger>
            {                
                new TravelPassenger
                {
                    Id = 1,
                    PassengerId = customers[1].Id,
                    Passenger = customers[1],
                    ApplyStatus = ApplyStatus.Accepted,
                    TravelId = 3
                },
                new TravelPassenger
                {
                    Id = 2,
                    PassengerId = customers[2].Id,
                    Passenger = customers[2],
                    ApplyStatus = ApplyStatus.Accepted,
                    TravelId = 3
                },
                new TravelPassenger
                {
                    Id = 3,
                    PassengerId = customers[3].Id,
                    Passenger = customers[3],
                    ApplyStatus = ApplyStatus.Accepted,
                    TravelId = 3
                }
            };

            var rolesMock = roles.AsQueryable().BuildMockDbSet();
            var contriesMock = countries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var brandsMock = brands.AsQueryable().BuildMockDbSet();
            var carsMock = cars.AsQueryable().BuildMockDbSet();
            var modelsMock = models.AsQueryable().BuildMockDbSet();
            var customersMock = customers.AsQueryable().BuildMockDbSet();
            var travelsCommentsMock = travelComments.AsQueryable().BuildMockDbSet();
            var travelsMock = travels.AsQueryable().BuildMockDbSet();
            var feedbacksMock = feedbacks.AsQueryable().BuildMockDbSet();
            var travelPassengersMock = travelPassengers.AsQueryable().BuildMockDbSet();

            var mockDb = new Mock<TangraTravelContext>();
            mockDb.Setup(context => context.Roles).Returns(rolesMock.Object);
            mockDb.Setup(context => context.Countries).Returns(contriesMock.Object);
            mockDb.Setup(context => context.Cities).Returns(citiesMock.Object);
            mockDb.Setup(context => context.Addresses).Returns(addressesMock.Object);
            mockDb.Setup(context => context.Brands).Returns(brandsMock.Object);
            mockDb.Setup(context => context.Models).Returns(modelsMock.Object);
            mockDb.Setup(context => context.Cars).Returns(carsMock.Object);
            mockDb.Setup(context => context.Customers).Returns(customersMock.Object);
            mockDb.Setup(context => context.TravelComments).Returns(travelsCommentsMock.Object);
            mockDb.Setup(context => context.Travels).Returns(travelsMock.Object);
            mockDb.Setup(context => context.Feedbacks).Returns(feedbacksMock.Object);
            mockDb.Setup(context => context.TravelPassengers).Returns(travelPassengersMock.Object);

            this.fakeDb = mockDb;

            var mockCustomerService = new Mock<ICustomerService>();
            this.customerService = mockCustomerService;
        }

        [TestMethod]
        public async Task Get_CompletedTravelsOnWhichFeedbackIsNotGivenYet()
        {
            var userId = 1;
            int page = 1;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.RetriveCompletedTravelsAsync(userId, page);

            Assert.IsInstanceOfType(result, typeof(List<TravelFeedbackDto>));
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(1, result[0].TravelId);
            Assert.AreEqual("Burgas_Street Burgas", result[0].StartPoint);
            Assert.AreEqual("Sofia_Street Sofia", result[0].EndPoint);
            Assert.AreEqual("8/11/2020", result[0].ArrivalDate);
            Assert.AreEqual("Driver", result[0].Role);
            Assert.AreEqual(1, result[0].PassengerCounts);
            Assert.AreEqual(2, result[1].TravelId);
            Assert.AreEqual("Sofia_Street Sofia", result[1].StartPoint);
            Assert.AreEqual("Burgas_Street Burgas", result[1].EndPoint);
            Assert.AreEqual("8/5/2020", result[1].ArrivalDate);
            Assert.AreEqual("Passenger", result[1].Role);
            Assert.AreEqual(1, result[1].PassengerCounts);
        }

        [TestMethod]
        public async Task Get_DriverInFinishedTravel()
        {
            int travelId = 1;
            int userId = 1;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.RetriveDriverInFinishedTravelAsync(travelId, userId);

            Assert.IsInstanceOfType(result, typeof(TravelCustomer));
            Assert.AreEqual(1, result.TargetId);
            Assert.AreEqual("driverFirstName driverLastName", result.FullName);
            Assert.AreEqual("driverUsername", result.Username);
            Assert.AreEqual("Name: driverFirstName driverLastName, Username: driverUsername", result.TargetInformation);
        }

        [TestMethod]
        public async Task Should_Return_Null_WhenPassengerAlreadyGivenFeedbackToDriver()
        {
            int travelId = 3;
            int userId = 2;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.RetriveDriverInFinishedTravelAsync(travelId, userId);

            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task Get_PassengersInFinishedTravel()
        {
            int travelId = 3;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.RetrivePassengerInFinishedTravelsAsync(travelId);

            Assert.IsInstanceOfType(result, typeof(List<TravelCustomer>));
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(2, result[0].TargetId);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[0].FullName);
            Assert.AreEqual("driverUsername2", result[0].Username);
            Assert.AreEqual("Name: driverFirstName2 driverLastName2, Username: driverUsername2", result[0].TargetInformation);
            Assert.AreEqual(3, result[1].TargetId);
            Assert.AreEqual("driverFirstName3 driverLastName3", result[1].FullName);
            Assert.AreEqual("driverUsername3", result[1].Username);
            Assert.AreEqual("Name: driverFirstName3 driverLastName3, Username: driverUsername3", result[1].TargetInformation);
        }
    }
}