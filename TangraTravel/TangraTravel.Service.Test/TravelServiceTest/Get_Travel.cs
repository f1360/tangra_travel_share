﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Service.Enum;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.Service.Models.AddressDtos;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Models.TravelCommentDtos;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.Service.Test.TravelServiceTest
{
    [TestClass]
    public class Get_Travel
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private Mock<ICustomerService> customerService;

        [TestInitialize]
        public void SetUp()
        {
            var countries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                },
                new City
                {
                    Id = 2,
                    Name = "Sofia",
                    CountryId = countries[0].Id,
                    Country = countries[0]
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = cities[0].Id,
                    City = cities[0],
                    StreetName = cities[0].Name + "_Street"
                },
                new Address
                {
                    Id = 2,
                    CityId = cities[1].Id,
                    City = cities[1],
                    StreetName = cities[1].Name + "_Street"
                }
            };
            var brands = new List<Brand>
            {
                new Brand
                {
                    Id = 1,
                    Name = "BMW"
                }
            };
            var models = new List<Model>
            {
                new Model
                {
                    Id = 1,
                    BrandModel = "X7 40i xDrive",
                    BrandId = brands[0].Id,
                    Brand = brands[0],
                }
            };
            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    RegistrationNumber = "registration1",
                    Seats = 4,
                    ModelId = models[0].Id,
                    Model = models[0]
                },
                new Car
                {
                    Id = 2,
                    RegistrationNumber = "registration2",
                    Seats = 4,
                    ModelId = models[0].Id,
                    Model = models[0]
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "driverFirstName",
                    LastName = "driverLastName",
                    Car = cars[0],
                    Username = "driverUsername",
                    PhoneNumber = "driverPhone"
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "driverFirstName2",
                    LastName = "driverLastName2",
                    Car = cars[1],
                    Username = "driverUsername2",
                    PhoneNumber = "driverPhone2"
                },
                new Customer
                {
                    Id = 3,
                    FirstName = "passengerFirstName",
                    LastName = "passengerLastName",
                    Username = "passengerUsername",
                    PhoneNumber = "passengerPhone"
                }
            };
            var travelComments = new List<TravelComment>
            {
                new TravelComment
                {
                    Id = 1,
                    Comment = "Not smoke",
                    TravelId = 1
                },
                new TravelComment
                {
                    Id = 2,
                    Comment = "Not smoke and lugage",
                    TravelId = 2
                },
                new TravelComment
                {
                    Id = 3,
                    Comment = "Not smoke and lugage",
                    TravelId = 3
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date,
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Travelling,
                    TravelComment = travelComments[0],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 2,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Parse("08/11/2020"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Completed,
                    TravelComment = travelComments[1],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 3,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Parse("08/11/2020"),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 4,
                    DriverId = customers[0].Id,
                    Driver = customers[0],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(50),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },

                new Travel
                {
                    Id = 5,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(50),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 6,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddMinutes(50),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>
                    {
                        new TravelPassenger
                        {
                            Id = 1,
                            ApplyStatus = ApplyStatus.Accepted,
                            TravelId = 6,
                            PassengerId = customers[2].Id,
                            Passenger = customers[2],
                        }
                    }
                },
                new Travel
                {
                    Id = 7,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddMonths(3),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 8,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(21),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 9,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(16),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 10,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddDays(3),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 11,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[0].Id,
                    StartAddress = addresses[0],
                    EndAddressId = addresses[1].Id,
                    EndAddress = addresses[1],
                    DepartureTime = DateTime.Now.Date.AddMonths(1),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                },
                new Travel
                {
                    Id = 12,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    StartAddressId = addresses[1].Id,
                    StartAddress = addresses[1],
                    EndAddressId = addresses[0].Id,
                    EndAddress = addresses[0],
                    DepartureTime = DateTime.Now.Date.AddDays(19),
                    FreeSeats = 3,
                    TravelStatus = TravelStatus.Open,
                    TravelComment = travelComments[2],
                    TravelPassengers = new List<TravelPassenger>()
                }
            };

            var contriesMock = countries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var brandsMock = brands.AsQueryable().BuildMockDbSet();
            var carsMock = cars.AsQueryable().BuildMockDbSet();
            var modelsMock = models.AsQueryable().BuildMockDbSet();
            var customersMock = customers.AsQueryable().BuildMockDbSet();
            var travelCommentsMock = travelComments.AsQueryable().BuildMockDbSet();
            var travelMock = travels.AsQueryable().BuildMockDbSet();

            var mockDb = new Mock<TangraTravelContext>();
            mockDb.Setup(context => context.Countries).Returns(contriesMock.Object);
            mockDb.Setup(context => context.Cities).Returns(citiesMock.Object);
            mockDb.Setup(context => context.Addresses).Returns(addressesMock.Object);
            mockDb.Setup(context => context.Brands).Returns(brandsMock.Object);
            mockDb.Setup(context => context.Models).Returns(modelsMock.Object);
            mockDb.Setup(context => context.Cars).Returns(carsMock.Object);
            mockDb.Setup(context => context.Customers).Returns(customersMock.Object);
            mockDb.Setup(context => context.TravelComments).Returns(travelCommentsMock.Object);
            mockDb.Setup(context => context.Travels).Returns(travelMock.Object);

            this.fakeDb = mockDb;

            var mockCustomerService = new Mock<ICustomerService>();
            this.customerService = mockCustomerService;
        }

        [TestMethod]
        public async Task Get_SelectedTravel()
        {
            int travelId = 1;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.GetTravelAsync(travelId);

            Assert.IsInstanceOfType(result, typeof(TravelPresentDto));
            Assert.IsInstanceOfType(result.Driver, typeof(DriverDto));
            Assert.IsInstanceOfType(result.Driver.CarDetails, typeof(CarTravelDto));
            Assert.IsInstanceOfType(result.StartAddress, typeof(CustomerAddressDto));
            Assert.IsInstanceOfType(result.EndAddress, typeof(CustomerAddressDto));
            Assert.IsInstanceOfType(result.Comment, typeof(TravelCommentPresentDto));

            Assert.AreEqual(1, result.TravelId);

            Assert.AreEqual("driverFirstName driverLastName", result.Driver.DriverName);
            Assert.AreEqual("BMW", result.Driver.CarDetails.Brand);
            Assert.AreEqual("X7 40i xDrive", result.Driver.CarDetails.Model);
            Assert.AreEqual("registration1", result.Driver.CarDetails.RegistrationNumber);
            Assert.AreEqual(4, result.Driver.CarDetails.Seats);

            Assert.AreEqual(1, result.StartAddress.AddressId);
            Assert.AreEqual("Bulgaria", result.StartAddress.Country);
            Assert.AreEqual("Burgas", result.StartAddress.City);
            Assert.AreEqual("Burgas_Street", result.StartAddress.StreetAddress);

            Assert.AreEqual(2, result.EndAddress.AddressId);
            Assert.AreEqual("Bulgaria", result.EndAddress.Country);
            Assert.AreEqual("Sofia", result.EndAddress.City);
            Assert.AreEqual("Sofia_Street", result.EndAddress.StreetAddress);

            Assert.AreEqual(3, result.FreeSeats);
            Assert.AreEqual(TravelStatus.Travelling.ToString(), result.Status);
            Assert.AreEqual("Not smoke", result.Comment.Comment);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TravelNotExist()
        {
            string expected = "There is no such travel in the system!";

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await Assert.ThrowsExceptionAsync<TravelDoesNotExistException>(() => service.GetTravelAsync(150));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Get_AllTravelsInDatabase()
        {
            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.GetAllTravelsAsync();

            Assert.IsInstanceOfType(result, typeof(ICollection<TravelPresentDto>));
            Assert.AreEqual(12, result.Count());
        }

        [TestMethod]
        [DataRow(1, 5, 5)]
        [DataRow(2, 2, 2)]
        [DataRow(1, 15, 8)]
        public async Task Get_DriverTravelsBiggerThanCurrentDate(int page, int perPage, int expected)
        {
            int driverId = 1;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.GetAllAvaliableTravels(driverId, page, perPage);

            Assert.AreEqual(expected, result.Count());
            Assert.IsInstanceOfType(result, typeof(ICollection<TravelPresentDto>));
        }

        [TestMethod]
        [DataRow(1, 2)]
        [DataRow(2, 1)]
        public async Task Get_MaxPageForAvailableTravelsByFiveRecordsOnPage(int customerId, int expected)
        {
            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            int actual = await service.MaxPageForAvailableTravels(customerId);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(1, 1)]
        [DataRow(2, 2)]
        public async Task Get_MaxPageForDriverTravels(int customerId, int expected)
        {
            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            int actual = await service.MaxPageForDriverTravels(customerId);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(3, 1)]
        public async Task Get_MaxPageForPassengerTravels(int customerId, int expected)
        {
            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            int actual = await service.MaxPageForPassengerTravels(customerId);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(1, 4)]
        [DataRow(2, 8)]
        public async Task Get_TravelsByDriverId(int driverId, int expected)
        {

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.FilterByDriverAsync(driverId);

            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual(expected, result.Count());
        }

        [TestMethod]
        [DataRow(1, 11)]
        [DataRow(2, 1)]
        public async Task Get_TravelsByStartAddressId(int addressId, int expected)
        {
            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.FilterByStartAddressAsync(addressId);

            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual(expected, result.Count());
        }

        [TestMethod]
        [DataRow(1, 1)]
        [DataRow(2, 11)]
        public async Task Get_TravelsByEndAddressId(int addressId, int expected)
        {
            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.FilterByEndAddressAsync(addressId);

            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual(expected, result.Count());
        }

        [TestMethod]
        public async Task Get_TravelByDepartureTime()
        {
            string departureTime = DateTime.Now.AddDays(50).ToString("d");

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.FilterByDepartureTimeAsync(departureTime)).ToArray();

            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("driverFirstName driverLastName", result[0].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[1].Driver.DriverName);
        }

        [TestMethod]
        [DataRow("15/2021")]
        [DataRow("13/35/2021")]
        public async Task Should_ThrowException_When_DepartureDateIsInvalid(string input)
        {
            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await Assert.ThrowsExceptionAsync<InvalidDepartureTimeException>(() => service.FilterByDepartureTimeAsync(input));
        }               

        [TestMethod]
        public async Task Get_TravelsAccordingToSortedByDriver()
        {
            var input = new TravelSortByDto { Criteria = SortBy.Driver };

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.SortByAsync(input)).ToArray();

            Assert.AreEqual(12, result.Count());
            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual("driverFirstName driverLastName", result[0].Driver.DriverName);
            Assert.AreEqual("driverFirstName driverLastName", result[1].Driver.DriverName);
            Assert.AreEqual("driverFirstName driverLastName", result[2].Driver.DriverName);
            Assert.AreEqual("driverFirstName driverLastName", result[3].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[4].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[5].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[6].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[7].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[8].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[9].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[10].Driver.DriverName);
            Assert.AreEqual("driverFirstName2 driverLastName2", result[11].Driver.DriverName);
        }

        [TestMethod]
        public async Task Get_TravelsAccordingToSortedByTravelDepartureTime()
        {
            var input = new TravelSortByDto { Criteria = SortBy.DepartureTime };

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.SortByAsync(input)).ToArray();

            Assert.AreEqual(12, result.Count());
            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
        }

        [TestMethod]
        public async Task Get_TravelsAccordingToSortedByTravelStatus()
        {
            var input = new TravelSortByDto { Criteria = SortBy.TravelStatus };

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.SortByAsync(input)).ToArray();

            Assert.AreEqual(12, result.Count());
            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual("Open", result[0].Status);
            Assert.AreEqual("Open", result[1].Status);
            Assert.AreEqual("Open", result[2].Status);
            Assert.AreEqual("Open", result[3].Status);
            Assert.AreEqual("Open", result[4].Status);
            Assert.AreEqual("Open", result[5].Status);
            Assert.AreEqual("Open", result[6].Status);
            Assert.AreEqual("Open", result[7].Status);
            Assert.AreEqual("Open", result[8].Status);
            Assert.AreEqual("Open", result[9].Status);
            Assert.AreEqual("Travelling", result[10].Status);
            Assert.AreEqual("Completed", result[11].Status);
        }

        [TestMethod]
        public async Task Get_TravelsAccordingToSortedByEndAddress()
        {
            var input = new TravelSortByDto { Criteria = SortBy.EndAddress };

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.SortByAsync(input)).ToArray();

            Assert.AreEqual(12, result.Count());
            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual(1, result[0].EndAddress.AddressId);
            Assert.AreEqual(2, result[1].EndAddress.AddressId);
            Assert.AreEqual(2, result[2].EndAddress.AddressId);
            Assert.AreEqual(2, result[3].EndAddress.AddressId);
            Assert.AreEqual(2, result[4].EndAddress.AddressId);
            Assert.AreEqual(2, result[5].EndAddress.AddressId);
            Assert.AreEqual(2, result[6].EndAddress.AddressId);
            Assert.AreEqual(2, result[7].EndAddress.AddressId);
            Assert.AreEqual(2, result[8].EndAddress.AddressId);
            Assert.AreEqual(2, result[9].EndAddress.AddressId);
            Assert.AreEqual(2, result[10].EndAddress.AddressId);
            Assert.AreEqual(2, result[11].EndAddress.AddressId);
        }

        [TestMethod]
        public async Task Get_TravelsAccordingToSortedByStartAddress()
        {
            var input = new TravelSortByDto { Criteria = SortBy.StartAddress };

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.SortByAsync(input)).ToArray();

            Assert.AreEqual(12, result.Count());
            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual(1, result[0].StartAddress.AddressId);
            Assert.AreEqual(1, result[1].StartAddress.AddressId);
            Assert.AreEqual(1, result[2].StartAddress.AddressId);
            Assert.AreEqual(1, result[3].StartAddress.AddressId);
            Assert.AreEqual(1, result[4].StartAddress.AddressId);
            Assert.AreEqual(1, result[5].StartAddress.AddressId);
            Assert.AreEqual(1, result[6].StartAddress.AddressId);
            Assert.AreEqual(1, result[7].StartAddress.AddressId);
            Assert.AreEqual(1, result[8].StartAddress.AddressId);
            Assert.AreEqual(1, result[9].StartAddress.AddressId);
            Assert.AreEqual(1, result[10].StartAddress.AddressId);
            Assert.AreEqual(2, result[11].StartAddress.AddressId);
        }

        [TestMethod]
        public async Task Get_SoonTravels()
        {
            int customerId = 1;
            int take = 3;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.GetSoonTravelsAsync(customerId, take)).ToArray();
            var travel = result[0];
            var passenger = travel.TravelPassengers.ToArray()[0];

            Assert.AreEqual(3, result.Count());
            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelStoreDto>));

            Assert.IsInstanceOfType(travel.TravelPassengers, typeof(ICollection<TravelPassengerStoreDto>));
            Assert.AreEqual(6, travel.TravelId);
            Assert.AreEqual("Burgas, Burgas_Street", travel.StartPoint);
            Assert.AreEqual("Sofia, Sofia_Street", travel.EndPoint);
            Assert.AreEqual(2, travel.DriverId);
            Assert.AreEqual("Username: driverUsername2, Phone: driverPhone2", travel.Driver);
            Assert.AreEqual("Open", travel.StatusTravel);

            Assert.AreEqual(3, passenger.PassengerId);
            Assert.AreEqual("Username: passengerUsername, Phone: passengerPhone", passenger.Passenger);
        }

        [TestMethod]
        public async Task Get_DriverTravelsByPagination()
        {
            int driverId = 2;
            int page = 1;
            int perPage = 5;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = await service.GetAllDriverTravels(driverId, page, perPage);

            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPresentDto>));
            Assert.AreEqual(perPage, result.Count());
            Assert.IsFalse(result.Any(x => x.Driver.DriverName != "driverFirstName2 driverLastName2"));
        }

        [TestMethod]
        public async Task Get_PassengerTravelsByPagination()
        {
            int passengerId = 3;
            int page = 1;
            int perPage = 5;

            var service = new TravelService(this.fakeDb.Object, this.func, this.customerService.Object);
            var result = (await service.GetAllPassengerTravels(passengerId, page, perPage)).ToArray();

            Assert.IsInstanceOfType(result, typeof(IEnumerable<TravelPassengerDetailsDto>));
            Assert.AreEqual(1, result.Count());
            Assert.IsFalse(result.Any(x => x.PassengerStatus != "Accepted"));
        }
    }
}