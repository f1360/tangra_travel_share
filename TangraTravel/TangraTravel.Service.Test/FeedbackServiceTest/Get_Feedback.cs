﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.FeedbackDtos;

namespace TangraTravel.Service.Test.FeedbackServiceTest
{
    [TestClass]
    public class Get_Feedback
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();

        [TestInitialize]
        public void SetUp()
        {
            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Access = Access.Admin,
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Driver,
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Passenger,
                },
            };
            var feedbackComments = new List<FeedbackComment>();
            var feedbacks = new List<Feedback>();
            int index = 1;
            int authorId = 0;
            int targetId = 0;
            for (int i = 1; i <= 20; i++)
            {
                FeedbackComment comment = null;
                if (i % 2 != 0)
                {
                    comment = new FeedbackComment
                    {
                        Id = index++,
                        Comment = "fake comment" + i,
                        FeedbackId = i
                    };
                    feedbackComments.Add(comment);
                }
                if (i % 2 != 0)
                {
                    authorId = 1;
                    targetId = 2;
                }
                else
                {
                    authorId = 2;
                    targetId = 1;
                }
                var feedback = new Feedback
                {
                    Id = i,
                    AuthorId = authorId,
                    Author = new Customer
                    {
                        Id = i,
                        FirstName = "authorFirstName" + authorId,
                        LastName = "authorLastName" + authorId,
                        Username = "authorUsername" + authorId
                    },
                    TargetId = targetId,
                    Target = new Customer
                    {
                        Id = targetId,
                        FirstName = "targetFirstName" + targetId,
                        LastName = "targetLastName" + targetId,
                        Username = "targetUsername" + targetId
                    },
                    Rating = i,
                    FeedbackComment = comment
                };
                feedbacks.Add(feedback);
            }
            for (int i = 0; i < feedbacks.Count - 1; i++)
            {
                if (i % 2 == 0)
                {
                    feedbacks[i].Rating = 4;
                }
                else
                {
                    feedbacks[i].Rating = 3;
                }
            }

            var feedbackCommentsMock = feedbackComments.AsQueryable().BuildMockDbSet();
            var feedbacksMock = feedbacks.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.FeedbackComments).Returns(feedbackCommentsMock.Object);
            dbMock.Setup(context => context.Feedbacks).Returns(feedbacksMock.Object);

            this.fakeDb = dbMock;
        }

        [TestMethod]
        public async Task Get_SelectedFeedback()
        {
            var expectedFeedback = new FeedbackReturnDto
            {
                FeedbackId = 5,
                Author = "authorFirstName1 authorLastName1",
                AuthorUsername = "authorUsername1",
                Target = "targetFirstName2 targetLastName2",
                TargetUsername = "targetUsername2",
                Comment = "fake comment5"
            };

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await service.GetFeedbackAsync(expectedFeedback.FeedbackId);

            Assert.AreEqual(expectedFeedback.FeedbackId, result.FeedbackId);
            Assert.AreEqual(expectedFeedback.Author, result.Author);
            Assert.AreEqual(expectedFeedback.AuthorUsername, result.AuthorUsername);
            Assert.AreEqual(expectedFeedback.Target, result.Target);
            Assert.AreEqual(expectedFeedback.TargetUsername, result.TargetUsername);
            Assert.IsTrue(result.Rating >= 0 && result.Rating <= 5);
            Assert.AreEqual(expectedFeedback.Comment, result.Comment);
        }

        [TestMethod]
        [DataRow(1, null)]
        [DataRow(2, 3)]
        [DataRow(1, 7)]
        public async Task Get_GivenFeedbackByPagination(int page, int? byPage)
        {
            int expected = 5;
            if (byPage.HasValue)
            {
                expected = byPage.Value;
            }
            int customerId = 1;

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await service.GivenFeedback(customerId, page, byPage);

            Assert.AreEqual(expected, result.Count());
            Assert.IsInstanceOfType(result.ToArray()[0], typeof(FeedbackReturnDto));
        }

        [TestMethod]
        [DataRow(1, null)]
        [DataRow(2, 3)]
        [DataRow(1, 7)]
        public async Task Get_ReceivedFeedbackByPagination(int page, int? byPage)
        {
            int expected = 5;
            if (byPage.HasValue)
            {
                expected = byPage.Value;
            }
            int customerId = 1;

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await service.ReceivedFeedback(customerId, page, byPage);

            Assert.AreEqual(expected, result.Count());
            Assert.IsInstanceOfType(result.ToArray()[0], typeof(FeedbackReturnDto));
        }

        [TestMethod]
        public async Task Get_SortedGivenFeedbackInDescedingOrder()
        {
            int customerId = 1;

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = (await service.GivenSortedFeedbackAsync(customerId, take: 5, best: true)).ToArray();
            var avg = result.Average(r => r.Rating);

            Assert.IsTrue(result[0].Rating > 3);
            Assert.IsTrue(avg > 3);
            Assert.IsInstanceOfType(result[0], typeof(FeedbackReturnDto));
        }

        [TestMethod]
        public async Task Get_SortedGivenFeedback()
        {
            int customerId = 1;

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = (await service.GivenSortedFeedbackAsync(customerId, take: 5, best: false)).ToArray();
            var avg = result.Average(r => r.Rating);

            Assert.IsTrue(result[0].Rating <= 3);
            Assert.IsTrue(avg <= 3);
            Assert.IsInstanceOfType(result[0], typeof(FeedbackReturnDto));
        }

        [TestMethod]
        [DataRow(1, 5, true, 2)]
        [DataRow(1, 5, false, 2)]
        public async Task Get_MaxPage(int customerId, int byPage, bool isGiven, int maxPage)
        {
            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await service.MaxPageAsync(customerId, byPage, isGiven);

            Assert.AreEqual(maxPage, result);
        }
    }
}