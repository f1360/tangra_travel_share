﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Models.FeedbackDtos;
using TangraTravel.Service.Exeception.Feedback;

namespace TangraTravel.Service.Test.FeedbackServiceTest
{
    [TestClass]
    public class Create_PassengerFeedback
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private Feedback fakeFeedback = new Feedback();
        private FeedbackComment fakeFeedbackComment = new FeedbackComment();

        [TestInitialize]
        public void SetUp()
        {
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "AuthorFirstName",
                    LastName = "AuthorLastName",
                    Username = "AuthorUsername"
                }, // Passenger
                new Customer
                {
                    Id = 2,
                    FirstName = "TargetFirstName2",
                    LastName = "TargetLastName2",
                    Username = "TargetUsername2",
                    Rating = 5,
                }, // Driver
                new Customer
                {
                    Id = 3,
                    FirstName = "TargetFirstName3",
                    LastName = "TargetLastName3",
                    Username = "TargetUsername3",
                    Rating = 5
                }, // Other Customer
                new Customer
                {
                    Id = 4,
                    FirstName = "TargetFirstName4",
                    LastName = "TargetLastName4",
                    Username = "TargetUsername4",
                }, // Already Written Feedback to him
            };
            var feedbacks = new List<Feedback>
            {
                new Feedback
                {
                    Id = 0,
                    AuthorId = customers[0].Id,
                    Author = customers[0],
                    TargetId = customers[2].Id,
                    Target = customers[2],
                    TravelId = 1,
                    Rating = 5
                },
                new Feedback
                {
                    Id = 1,
                    AuthorId = customers[0].Id,
                    TargetId = customers[3].Id,
                    TravelId = 3,
                    Rating = 5
                }
            }; // Driver wrote an email to Passenger Id = 3
            var comments = new List<FeedbackComment>
            {
                new FeedbackComment
                {
                    Id = 1,
                    FeedbackId = feedbacks[1].Id,
                    Feedback = feedbacks[1],
                    Comment = "You are the best"
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DriverId = customers[1].Id,
                    Driver = customers[1],
                    TravelStatus = TravelStatus.Completed,
                    TravelPassengers = new List<TravelPassenger>
                    {
                        new TravelPassenger
                        {
                            Id = 1,
                            TravelId = 1,
                            //Travel = travels[0],
                            PassengerId = customers[0].Id,
                            Passenger = customers[0],
                            ApplyStatus = ApplyStatus.Accepted
                        },
                        new TravelPassenger
                        {
                            Id = 2,
                            TravelId = 1,
                            //Travel = travels[0],
                            PassengerId = customers[2].Id,
                            Passenger = customers[2],
                            ApplyStatus = ApplyStatus.Rejected
                        }
                    }
                },
                new Travel{Id = 2, TravelStatus = TravelStatus.Open},
                new Travel
                {
                    Id = 3,
                    DriverId = customers[3].Id,
                    Driver = customers[3],
                    TravelStatus = TravelStatus.Completed,
                    TravelPassengers = new List<TravelPassenger>
                    {
                        new TravelPassenger
                        {
                            Id = 1,
                            TravelId = 3,
                            //Travel = travels[0],
                            PassengerId = customers[0].Id,
                            Passenger = customers[0],
                            ApplyStatus = ApplyStatus.Accepted
                        }
                    }
                },
            };
            var travelPassengers = new List<TravelPassenger>
            {
                new TravelPassenger
                {
                    Id = 1,
                    TravelId = 1,
                    Travel = travels[0],
                    PassengerId = customers[0].Id,
                    Passenger = customers[0],
                    ApplyStatus = ApplyStatus.Accepted
                },
                new TravelPassenger
                {
                    Id = 2,
                    TravelId = 1,
                    //Travel = travels[0],
                    PassengerId = customers[2].Id,
                    Passenger = customers[2],
                    ApplyStatus = ApplyStatus.Rejected
                },
                new TravelPassenger
                {
                    Id = 3,
                    TravelId = travels[2].Id,
                    Travel = travels[2],
                    PassengerId = customers[0].Id,
                    Passenger = customers[0],
                    ApplyStatus = ApplyStatus.Accepted
                }
            };


            var customersMock = customers.AsQueryable().BuildMockDbSet();
            var feedbacksMock = feedbacks.AsQueryable().BuildMockDbSet();
            var travelsMock = travels.AsQueryable().BuildMockDbSet();
            var travelPassengersMock = travelPassengers.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Customers).Returns(customersMock.Object);
            dbMock.Setup(context => context.Feedbacks).Returns(feedbacksMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelsMock.Object);
            dbMock.Setup(context => context.TravelPassengers).Returns(travelPassengersMock.Object);

            this.fakeDb = dbMock;
        }

        [TestMethod]
        public async Task PassengerWriteFeedbackToDriver()
        {
            int passengerId = 1;
            var input = new FeedbackCreateDto
            {
                TargetId = 2,
                TravelId = 1,
                Rating = 5,
                Comment = "The best"
            };
            this.fakeDb.Setup(x => x.Feedbacks.AddAsync(It.IsAny<Feedback>(), It.IsAny<CancellationToken>()))
                       .Callback((Feedback feedback, CancellationToken token) =>
                       {
                           this.fakeFeedback.Rating = feedback.Rating;
                           this.fakeFeedback.TravelId = feedback.TravelId;
                           this.fakeFeedback.AuthorId = feedback.AuthorId;
                           this.fakeFeedback.TargetId = feedback.TargetId;
                       });
            this.fakeDb.Setup(x => x.FeedbackComments.AddAsync(It.IsAny<FeedbackComment>(), It.IsAny<CancellationToken>()))
                      .Callback((FeedbackComment comment, CancellationToken token) =>
                      {
                          this.fakeFeedbackComment.Comment = comment.Comment;
                          this.fakeFeedbackComment.Feedback = comment.Feedback;
                      });

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await service.PassengerWriteFeedbackAsync(passengerId, input);

            this.fakeDb.Verify(db => db.Feedbacks.AddAsync(It.IsAny<Feedback>(), It.IsAny<CancellationToken>()), Times.Once);
            this.fakeDb.Verify(db => db.FeedbackComments.AddAsync(It.IsAny<FeedbackComment>(), It.IsAny<CancellationToken>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);

            Assert.AreEqual(5, this.fakeFeedback.Rating);
            Assert.AreEqual(1, this.fakeFeedback.TravelId);
            Assert.AreEqual(1, this.fakeFeedback.AuthorId);
            Assert.AreEqual(2, this.fakeFeedback.TargetId);

            Assert.AreEqual(5, this.fakeFeedbackComment.Feedback.Rating);
            Assert.AreEqual(1, this.fakeFeedbackComment.Feedback.TravelId);
            Assert.AreEqual(1, this.fakeFeedbackComment.Feedback.AuthorId);
            Assert.AreEqual(2, this.fakeFeedbackComment.Feedback.TargetId);
            Assert.AreEqual(input.Comment, this.fakeFeedbackComment.Comment);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_AuthorNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.PassengerWriteFeedbackAsync(100, new FeedbackCreateDto()));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TargetNotExist()
        {
            string expected = "There is no user in the system!";
            int passengerId = 1;
            var input = new FeedbackCreateDto { TargetId = 500 };

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.PassengerWriteFeedbackAsync(passengerId, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TravelNotExist()
        {
            string expected = "There is no such travel in the system!";
            int passengerId = 1;
            var input = new FeedbackCreateDto
            {
                TargetId = 2,
                TravelId = 100,
                Rating = 5,
                Comment = "The best"
            };

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await Assert.ThrowsExceptionAsync<TravelDoesNotExistException>(() => service.PassengerWriteFeedbackAsync(passengerId, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TravelStatusIsNotCompleted()
        {
            string expected = "The Travel must be completed!";
            int passengerId = 1;
            var input = new FeedbackCreateDto
            {
                TargetId = 2,
                TravelId = 2,
                Rating = 5,
                Comment = "The best"
            };

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await Assert.ThrowsExceptionAsync<TravelDoesNotCompleteException>(() => service.PassengerWriteFeedbackAsync(passengerId, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        [DataRow(2)]
        [DataRow(4)]
        public async Task Should_ThrowException_When_PassengerMissInSelectedTravel(int passengerId)
        {
            string expected = "You did not attend to this travel!";
            var input = new FeedbackCreateDto
            {
                TargetId = 2,
                TravelId = 1,
                Rating = 5,
                Comment = "The best"
            };

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.PassengerWriteFeedbackAsync(passengerId, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_DriverIsWrong()
        {
            string expected = "You want to leave a feedback for someone who did not take part in the trip!";
            int passengerId = 1;
            var input = new FeedbackCreateDto
            {
                TargetId = 3,
                TravelId = 1,
                Rating = 5,
                Comment = "The best"
            };

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.PassengerWriteFeedbackAsync(passengerId, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_PassengerAlreadyWroteFeedbackToDriver()
        {
            string expected = "You already wrote a feedback to TargetUsername4";
            int passengerId = 1;
            var input = new FeedbackCreateDto
            {
                TargetId = 4,
                TravelId = 3,
                Rating = 5,
                Comment = "The best"
            };

            var service = new FeedbackService(this.fakeDb.Object, this.func);
            var result = await Assert.ThrowsExceptionAsync<FeedbackException>(() => service.PassengerWriteFeedbackAsync(passengerId, input));

            Assert.AreEqual(expected, result.Message);
        }
    }
}