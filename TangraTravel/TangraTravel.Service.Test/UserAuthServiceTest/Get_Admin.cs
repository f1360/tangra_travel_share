﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Service;
using TangraTravel.Service.Exeception.User;

namespace TangraTravel.Service.Test.UserAuthServiceTest
{
    [TestClass]
    public class Get_Admin
    {
        private Mock<TangraTravelContext> fakeDb;

        [TestInitialize]
        public void SetUp()
        {
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    Username = "username",
                    Email = "fake1@fake.com",
                    Password = "1234",
                    Role = new Role
                    {
                        Id = 2,
                        Access = Data.Enum.Access.Driver
                    }
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    Username = "username1",
                    Email = "fake2@fake.com",
                    Password = "1234",
                    Role = new Role
                    {
                        Id = 3,
                        Access = Data.Enum.Access.Passenger
                    }
                }
            };
            var admins = new List<Admin>
            {
                new Admin
                {
                    Id = 1,
                    FirstName = "firstname",
                    LastName = "lastname",
                    Email = "fake1@tangratravel.com",
                    Password = "1234",
                    Role = new Role
                    {
                        Id = 1,
                        Access = Data.Enum.Access.Admin
                    }
                },
                new Admin
                {
                    Id = 2,
                    FirstName = "firstname1",
                    LastName = "lastname1",
                    Email = "fake2@tangratravel.com",
                    Password = "1234",
                    Role = new Role
                    {
                        Id = 1,
                        Access = Data.Enum.Access.Admin
                    }
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var adminMock = admins.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.Admins).Returns(adminMock.Object);

            this.fakeDb = dbMock;
        }

        [TestMethod]
        public async Task Should_Return_AdminUserByEmailOnly()
        {
            string email = "fake1@tangratravel.com";
            string password = "1234";
            string expectedFirstName = "firstname";
            string expectedLastName = "lastname";

            var service = new UserAuthService(this.fakeDb.Object);
            var user = await service.LogInAsync(email);

            Assert.AreEqual(email, user.Email);
            Assert.AreEqual(password, user.Password);
            Assert.AreEqual(expectedFirstName, user.FirstName);
            Assert.AreEqual(expectedLastName, user.LastName);
            Assert.AreEqual("Admin", user.Role.Access.ToString());
        }

        [TestMethod]
        public async Task Should_Return_AdminUserByEmailAndPassword()
        {
            string email = "fake1@tangratravel.com";
            string password = "1234";
            string expectedFirstName = "firstname";
            string expectedLastName = "lastname";

            var service = new UserAuthService(this.fakeDb.Object);
            var user = await service.LogInAsync(email, password);

            Assert.AreEqual(email, user.Email);
            Assert.AreEqual(password, user.Password);
            Assert.AreEqual(expectedFirstName, user.FirstName);
            Assert.AreEqual(expectedLastName, user.LastName);
            Assert.AreEqual("Admin", user.Role.Access.ToString());
        }

        [TestMethod]
        public async Task Should_ThrowException_When_Email_NotExist()
        {
            string expected = "There is no user with such email!";
            string email = "wrong@tangratravel.com";

            var service = new UserAuthService(this.fakeDb.Object);
            var result = await Assert.ThrowsExceptionAsync<EmailIncorrectException>(() => service.LogInAsync(email));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_PasswordIsIncorrect()
        {
            string expected = "Password is incorrect!";
            string email = "fake1@tangratravel.com";

            var service = new UserAuthService(this.fakeDb.Object);
            
            var result = await Assert.ThrowsExceptionAsync<PasswordIncorrectException>(() => service.LogInAsync(email, "fake"));

            Assert.AreEqual(expected, result.Message);
        }
    }
}