﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Exeception.TravelPassenger;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.Service.Test.CustomerServiceTest
{
    [TestClass]
    public class Update_Customer
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private IAvatarService avatarService;
        private Mock<IEmailService> emailVerification;

        [TestInitialize]
        public void SetUp()
        {
            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Access = Access.Admin,
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Driver,
                },
                new Role
                {
                    Id = 3,
                    Access = Access.Passenger,
                },
            };
            var coutries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                },
                new Country
                {
                    Id = 2,
                    Name = "Germany"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    CountryId = 1,
                    Country = coutries[0],
                    Name = "Sofia"
                },
                new City
                {
                    Id = 2,
                    CountryId = 2,
                    Country = coutries[1],
                    Name = "Berlin"
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = 1,
                    City = cities[0],
                    StreetName = "Sofia_Address"
                },
                new Address
                {
                    Id = 2,
                    CityId = 2,
                    City = cities[1],
                    StreetName = "Berlin_Address"
                }
            };
            var customers = new List<Customer>();
            for (int i = 1; i <= 11; i++)
            {
                var driver = new Customer
                {
                    Id = i,
                    FirstName = "firstname" + i,
                    LastName = "lastname" + i,
                    Email = $"fake{i}@email.com",
                    Gender = Gender.Man,
                    Username = "username" + i,
                    Rating = i,
                    PhoneNumber = "fakephone" + i,
                    RoleId = 2,
                    Role = new Role
                    {
                        Id = 2,
                        Access = Access.Driver,
                    },
                    IsBlocked = false,
                    AddressId = 1,
                    Address = addresses[0],
                    Car = new Car
                    {
                        Id = i
                    }
                };

                customers.Add(driver);
            }
            for (int i = 1; i <= 11; i++)
            {
                var passenger = new Customer
                {
                    Id = (11 + i),
                    FirstName = "firstname" + (11 + i),
                    LastName = "lastname" + (11 + i),
                    Email = $"fake{(11 + i)}@email.com",
                    Username = "username" + (11 + i),
                    Rating = 11 + i,
                    PhoneNumber = "fakephone" + (11 + i),
                    RoleId = 3,
                    Role = new Role
                    {
                        Id = 3,
                        Access = Access.Passenger,
                    },
                    IsBlocked = false,
                    AddressId = 2,
                    Address = addresses[1]
                };

                customers.Add(passenger);
            }
            var travelPassengers = new List<TravelPassenger>
            {
                new TravelPassenger
                {
                    Id = 1,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 13,
                    Passenger = customers[12],
                    TravelId = 1,
                },
                new TravelPassenger
                {
                    Id = 2,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 14,
                    Passenger = customers[13],
                    TravelId = 1
                },
                new TravelPassenger
                {
                    Id = 3,
                    ApplyStatus = ApplyStatus.Rejected,
                    PassengerId = 9,
                    Passenger = customers[8],
                    TravelId = 1
                },
                new TravelPassenger
                {
                    Id = 4,
                    ApplyStatus = ApplyStatus.Applied,
                    PassengerId = 5,
                    Passenger = customers[4],
                    TravelId = 1
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DepartureTime = DateTime.Now.AddDays(15),
                    DriverId = 1,
                    Driver = customers[0],
                    EndAddressId = 1,
                    EndAddress = addresses[0],
                    StartAddressId = 2,
                    StartAddress = addresses[1],
                    FreeSeats = 2,
                    TravelComment = new TravelComment
                        {
                            Id = 1,
                            Comment = "Can not smoke!",
                            TravelId = 1
                        },
                    TravelStatus = TravelStatus.Open,
                    TravelPassengers = travelPassengers,
                },
                new Travel
                {
                    Id = 2,
                    DepartureTime = DateTime.Parse("05/08/2020"),
                    DriverId = 5,
                    Driver = customers[4],
                    EndAddressId = 1,
                    EndAddress = addresses[0],
                    StartAddressId = 2,
                    StartAddress = addresses[1],
                    FreeSeats = 4,
                    TravelComment = new TravelComment
                        {
                            Id = 1,
                            Comment = "Can not smoke!",
                            TravelId = 1
                        },
                    TravelStatus = TravelStatus.Completed,
                    TravelPassengers = new List<TravelPassenger>(),
                },
            };

            var rolesMock = roles.AsQueryable().BuildMockDbSet();
            var countriesMock = coutries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var travelPassengersMock = travelPassengers.AsQueryable().BuildMockDbSet();
            var travelsMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Roles).Returns(rolesMock.Object);
            dbMock.Setup(context => context.Countries).Returns(countriesMock.Object);
            dbMock.Setup(context => context.Cities).Returns(citiesMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressesMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.TravelPassengers).Returns(travelPassengersMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelsMock.Object);

            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetAvatarAsync(It.IsAny<int>(), It.IsAny<bool>()))
                             .ReturnsAsync(new AvatarDto { AvatarId = 1, ImageDataURL = "updatedfakeUrl", Alt = "updatedfakeAlt" });

            fakeAvatarService.Setup(x => x.UpdateAvatarAsync(It.IsAny<Customer>(), It.IsAny<IFormFile>()));
            this.avatarService = fakeAvatarService.Object;

            var fakeEmailVerigication = new Mock<IEmailService>();
            this.emailVerification = fakeEmailVerigication;
        }

        [TestMethod]
        public async Task Get_Updated_Customer()
        {
            int customerId = 1;
            var avatarMock = new Mock<IFormFile>();
            var input = new CustomerUpdateDto
            {
                Username = "newFake",
                FirstName = "newFakeFirstName",
                LastName = "newFakeLastName",
                NewEmail = "newFake@fake.com",
                Password = "newFakePass",
                Gender = Gender.NaN,
                AddressId = 2,
                PhoneNumber = "newFakePhone",
                Avatar = avatarMock.Object
            };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var customer = await service.UpdateAsync(customerId, input);
            var password = this.fakeDb.Object.Customers
                                  .Select(x => new { Id = x.Id, Pass = x.Password })
                                  .FirstOrDefault(x => x.Id == customerId);

            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.AreEqual(input.FirstName, customer.FirstName);
            Assert.AreEqual(input.LastName, customer.LastName);
            Assert.AreEqual(input.NewEmail, customer.Email);
            Assert.AreEqual(input.Password, password.Pass);
            Assert.AreEqual(input.Gender, customer.Gender);
            Assert.AreEqual(input.Username, customer.Username);
            Assert.AreEqual(input.PhoneNumber, customer.PhoneNumber);
            Assert.AreEqual(input.AddressId, customer.Address.AddressId);
            Assert.AreEqual("updatedfakeUrl", customer.Avatar.ImageDataURL);
            Assert.AreEqual("updatedfakeAlt", customer.Avatar.Alt);
        }

        [TestMethod]
        public async Task Get_Updated_Customer_When_NewEmailIsAsOriginalEmail()
        {
            int customerId = 1;
            var avatarMock = new Mock<IFormFile>();
            var input = new CustomerUpdateDto
            {
                NewEmail = $"fake1@email.com",
            };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var customer = await service.UpdateAsync(customerId, input);

            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.AreEqual(input.NewEmail, customer.Email);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.UpdateAsync(500, new CustomerUpdateDto()));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_NewEmailDomainIsAsCorporate()
        {
            string expected = "Email is with corporative domain!";
            int customerid = 1;
            var input = new CustomerUpdateDto
            {
                NewEmail = $"fake15@tangratravel.com",
                PhoneNumber = "fakephone5",
                Username = "username5"
            };
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.UpdateAsync(customerid, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_NewEmailExist()
        {
            string expected = "Email exist in the system!";
            int customerid = 1;
            var input = new CustomerUpdateDto
            {
                NewEmail = $"fake15@email.com",
                PhoneNumber = "fakephone5",
                Username = "username5"
            };
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.UpdateAsync(customerid, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_NewUsernameExist()
        {
            string expected = "Username exist in the system!";
            int customerid = 1;
            var input = new CustomerUpdateDto
            {
                NewEmail = $"fake150@email.com",
                PhoneNumber = "fakephone500",
                Username = "username5"
            };
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.UpdateAsync(customerid, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_NewPhoneExist()
        {
            string expected = "Phone exist in the system!";
            int customerid = 1;
            var input = new CustomerUpdateDto
            {
                NewEmail = $"fake150@email.com",
                PhoneNumber = "fakephone5",
                Username = "username500"
            };
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.UpdateAsync(customerid, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Get_TravelPassenger_After_ApplyingTravel()
        {
            int passengerId = 15;
            int travelId = 1;
            var passengers = new List<TravelPassenger>();
            this.fakeDb.Setup(x => x.TravelPassengers.AddAsync(It.IsAny<TravelPassenger>(), It.IsAny<CancellationToken>()))
                .Callback((TravelPassenger passenger, CancellationToken token) => { passengers.Add(passenger); });

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.ApplyForTravelAsync(passengerId, travelId, Access.Passenger);

            this.fakeDb.Verify(db => db.TravelPassengers.AddAsync(It.IsAny<TravelPassenger>(), It.IsAny<CancellationToken>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.AreEqual(passengerId, passengers[0].PassengerId);
            Assert.AreEqual(travelId, passengers[0].TravelId);
            Assert.AreEqual(ApplyStatus.Applied, passengers[0].ApplyStatus);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerRoleIsAdmin_In_ApplyForTravel()
        {
            string expected = "The user can not be with Admin role!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<PermissionException>(() => service.ApplyForTravelAsync(50, 1, Access.Admin));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_Set_ApplicationStatus_Of_TravelPassenger()
        {
            int travelPassengerId = 4;
            var input = new TravelPassengerApplyStatusDto { Criteria = ApplyStatus.Rejected };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.ProcessApplicationAsync(travelPassengerId, input);

            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TravelPassengerNotExist()
        {
            string expected = "There is no such travelPassenger record in the system!";
            var input = new TravelPassengerApplyStatusDto { Criteria = ApplyStatus.Rejected };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<TravelPassengerDoesNotExistException>(() => service.ProcessApplicationAsync(50, input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ChangeRole()
        {
            int userId = 15;
            Access role = Access.Driver;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.ChangeRoleAsync(userId, role);
            var passenger = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == userId);

            Assert.AreEqual(2, passenger.RoleId);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_SelectedRoleIsAdmin()
        {
            string expected = "The user can not be with Admin role!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.ChangeRoleAsync(1, Access.Admin));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_UserNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.ChangeRoleAsync(50, Access.Driver));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_SetCarToNull_When_DriverBecomesPassenger()
        {
            int userId = 1;
            Access role = Access.Passenger;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.ChangeRoleAsync(userId, role);
            var passenger = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == userId);

            Assert.AreEqual(3, passenger.RoleId);
            Assert.IsNull(passenger.Car);
        }

        [TestMethod]
        public async Task Should_Block_Customer()
        {
            int customerId = 1;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.BlockAsync(customerId);

            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == customerId);

            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.IsTrue(customer.IsBlocked);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TryToBlockCustomerWhoNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.BlockAsync(500));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_Unblock_Customer()
        {
            int customerId = 15;
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == customerId);
            customer.IsBlocked = true;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.UnblockAsync(customerId);

            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.IsFalse(customer.IsBlocked);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_TryToUnblockCustomerWhoNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.UnblockAsync(500));

            Assert.AreEqual(expected, result.Message);
        }
    }
}