﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;
using TangraTravel.Service.Models.CustomerDtos;

namespace TangraTravel.Service.Test.CustomerServiceTest
{
    [TestClass]
    public class Create_Customer
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private IAvatarService avatarService;
        private Customer fakeCustomer;
        private Mock<IEmailService> emailVerification;

        [TestInitialize]
        public void SetUp()
        {
            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Access = Access.Admin,
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Driver,
                },
                new Role
                {
                    Id = 3,
                    Access = Access.Passenger,
                },
            };
            var coutries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                },
                new Country
                {
                    Id = 2,
                    Name = "Germany"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    CountryId = 1,
                    Country = coutries[0],
                    Name = "Sofia"
                },
                new City
                {
                    Id = 2,
                    CountryId = 2,
                    Country = coutries[1],
                    Name = "Berlin"
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = 1,
                    City = cities[0],
                    StreetName = "Sofia_Address"
                },
                new Address
                {
                    Id = 2,
                    CityId = 2,
                    City = cities[1],
                    StreetName = "Berlin_Address"
                }
            };
            var customers = new List<Customer>();
            customers.Add(new Customer
            {
                Id = 0,
                Address = addresses[0],
            });
            this.fakeCustomer = customers[0];
            for (int i = 1; i <= 5; i++)
            {
                var driver = new Customer
                {
                    Id = i,
                    FirstName = "firstname" + i,
                    LastName = "lastname" + i,
                    Email = $"fake{i}@email.com",
                    Username = "username" + i,
                    Rating = i,
                    PhoneNumber = "fakephone" + i,
                    RoleId = 2,
                    Role = new Role
                    {
                        Id = 2,
                        Access = Access.Passenger,
                    },
                    IsBlocked = false,
                    AddressId = 1,
                    Address = addresses[0],
                    Car = new Car
                    {
                        Id = i
                    }
                };

                customers.Add(driver);
            }

            var rolesMock = roles.AsQueryable().BuildMockDbSet();
            var countriesMock = coutries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var customerMock = customers.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Roles).Returns(rolesMock.Object);
            dbMock.Setup(context => context.Countries).Returns(countriesMock.Object);
            dbMock.Setup(context => context.Cities).Returns(citiesMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressesMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetAvatarAsync(It.IsAny<int>(), It.IsAny<bool>()))
                             .ReturnsAsync(new AvatarDto { AvatarId = 1, ImageDataURL = "createdfakeUrl", Alt = "createdfakeAlt" });

            fakeAvatarService.Setup(x => x.CreateAvatarAsync(It.IsAny<Customer>(), It.IsAny<IFormFile>()));
            this.avatarService = fakeAvatarService.Object;

            var fakeEmailVerigication = new Mock<IEmailService>();
            this.emailVerification = fakeEmailVerigication;
        }

        [TestMethod]
        public async Task Get_CreatedCustomer()
        {
            string actualPass = string.Empty;
            int actualAddressId = 0;
            this.fakeDb.Setup(x => x.Customers.AddAsync(It.IsAny<Customer>(), It.IsAny<CancellationToken>()))
                       .Callback((Customer customer, CancellationToken token) =>
                       {
                           this.fakeCustomer.FirstName = customer.FirstName;
                           this.fakeCustomer.LastName = customer.LastName;
                           this.fakeCustomer.Username = customer.Username;
                           this.fakeCustomer.PhoneNumber = customer.PhoneNumber;
                           this.fakeCustomer.Gender = customer.Gender;
                           this.fakeCustomer.Email = customer.Email;
                           this.fakeCustomer.Password = customer.Password;
                           actualPass = customer.Password;
                           actualAddressId = customer.AddressId;
                           this.fakeCustomer.AddressId = customer.AddressId;
                           this.fakeCustomer.Role = customer.Role;
                           this.fakeCustomer.RoleId = customer.Role.Id;
                       });

            var fakeAvatar = new Mock<IFormFile>();
            var input = new CustomerCreateDto
            {
                Username = "newFakeUsername",
                FirstName = "fakeFirstName",
                LastName = "fakeLastName",
                Email = "fakeNewEmail@abv.bg",
                Password = "fakePass",
                AddressId = 1,
                PhoneNumber = "newFakePhone",
                Sex = Gender.Woman,
                Avatar = fakeAvatar.Object
            };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await service.CreateAsync(input);

            this.fakeDb.Verify(db => db.Customers.AddAsync(It.IsAny<Customer>(), It.IsAny<CancellationToken>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.AreEqual(input.FirstName, result.FirstName);
            Assert.AreEqual(input.LastName, result.LastName);
            Assert.AreEqual(input.Username, result.Username);
            Assert.AreEqual(input.PhoneNumber, result.PhoneNumber);
            Assert.AreEqual(input.Sex, result.Gender);
            Assert.AreEqual("Passenger", result.SystemStatus);
            Assert.AreEqual("Active", result.StatusProfile);
            Assert.AreEqual(input.Email, result.Email);
            Assert.AreEqual(input.Password, actualPass);
            Assert.AreEqual(input.AddressId, actualAddressId);
            Assert.AreEqual("createdfakeUrl", result.Avatar.ImageDataURL);
            Assert.AreEqual("createdfakeAlt", result.Avatar.Alt);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_EmailDomainAsCompany()
        {
            string expected = "Email is with corporative domain!";

            var input = new CustomerCreateDto
            {
                Email = "fake3@tangratravel.com",
                PhoneNumber = "fakephone3",
                Username = "username3"

            };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(()=>  service.CreateAsync(input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_EmailAlreadyExist()
        {
            string expected = "Email exist in the system!";

            var input = new CustomerCreateDto
            {
                Email = "fake3@email.com",
                PhoneNumber = "fakephone3",
                Username = "username3"

            };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.CreateAsync(input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_UsernameAlreadyExist()
        {
            string expected = "Username exist in the system!";

            var input = new CustomerCreateDto
            {
                Email = "fake33@email.com",
                PhoneNumber = "fakephone33",
                Username = "username3"

            };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.CreateAsync(input));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_PhoneNumberAlreadyExist()
        {
            string expected = "Phone exist in the system!";

            var input = new CustomerCreateDto
            {
                Email = "fake33@email.com",
                PhoneNumber = "fakephone3",
                Username = "username30"

            };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<InputExistException>(() => service.CreateAsync(input));

            Assert.AreEqual(expected, result.Message);
        }
    }
}