﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Test.CustomerServiceTest
{
    [TestClass]
    public class Delete_Customer
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private IAvatarService avatarService;
        private Mock<IEmailService> emailVerification;

        [TestInitialize]
        public void SetUp()
        {
            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    RegistrationNumber = $"registration1",
                    CustomerId = 1
                },
                new Car
                {
                    Id = 2,
                    RegistrationNumber = $"registration2",
                    CustomerId = 2
                },
                new Car
                {
                    Id = 3,
                    RegistrationNumber = $"registration3",
                    CustomerId = 3
                }

            };
            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Access = Access.Admin,
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Driver,
                },
                new Role
                {
                    Id = 3,
                    Access = Access.Passenger,
                },
            };
            var coutries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                },
                new Country
                {
                    Id = 2,
                    Name = "Germany"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    CountryId = 1,
                    Country = coutries[0],
                    Name = "Sofia"
                },
                new City
                {
                    Id = 2,
                    CountryId = 2,
                    Country = coutries[1],
                    Name = "Berlin"
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = 1,
                    City = cities[0],
                    StreetName = "Sofia_Address"
                },
                new Address
                {
                    Id = 2,
                    CityId = 2,
                    City = cities[1],
                    StreetName = "Berlin_Address"
                }
            };
            var customers = new List<Customer>();
            for (int i = 1; i <= 3; i++)
            {
                var driver = new Customer
                {
                    Id = i,
                    FirstName = "firstname" + i,
                    LastName = "lastname" + i,
                    Email = $"fake{i}@email.com",
                    Gender = Gender.Man,
                    Username = "username" + i,
                    Rating = i,
                    PhoneNumber = "fakephone" + i,
                    RoleId = 2,
                    Role = new Role
                    {
                        Id = 2,
                        Access = Access.Driver,
                    },
                    IsBlocked = false,
                    AddressId = 1,
                    Address = addresses[0],
                    Car = new Car
                    {
                        Id = i,
                        RegistrationNumber = $"registration{i}"
                    }
                };

                customers.Add(driver);
            }
            for (int i = 1; i <= 7; i++)
            {
                var passenger = new Customer
                {
                    Id = (3 + i),
                    FirstName = "firstname" + (3 + i),
                    LastName = "lastname" + (3 + i),
                    Email = $"fake{(3 + i)}@email.com",
                    Username = "username" + (3 + i),
                    Rating = 3 + i,
                    PhoneNumber = "fakephone" + (3 + i),
                    RoleId = 3,
                    Role = new Role
                    {
                        Id = 3,
                        Access = Access.Passenger,
                    },
                    IsBlocked = false,
                    AddressId = 2,
                    Address = addresses[1]
                };

                customers.Add(passenger);
            }
            var travelPassengers = new List<TravelPassenger>
            {
                new TravelPassenger
                {
                    Id = 1,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 4,
                    Passenger = customers[3],
                    TravelId = 1,
                    Travel = new Travel
                    {
                        Id = 1,
                        TravelStatus = TravelStatus.Open,
                        FreeSeats = 2
                    }
                },
                new TravelPassenger
                {
                    Id = 2,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 2,
                    Passenger = customers[1],
                    TravelId = 1,
                    Travel = new Travel
                    {
                        Id = 1,
                        TravelStatus = TravelStatus.Open,
                        FreeSeats = 2
                    }
                }, // Driver 2
                new TravelPassenger
                {
                    Id = 3,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 5,
                    Passenger = customers[4],
                    TravelId = 2,
                    Travel = new Travel
                    {
                        Id = 2,
                        TravelStatus = TravelStatus.Open,
                        FreeSeats = 3
                    }
                },
                new TravelPassenger
                {
                    Id = 4,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 6,
                    Passenger = customers[5],
                    TravelId = 3,
                    Travel = new Travel
                    {
                        Id = 3,
                        TravelStatus = TravelStatus.Travelling,
                        FreeSeats = 4
                    }
                },

            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DepartureTime = DateTime.Now.AddDays(15),
                    DriverId = 1,
                    Driver = customers[0],
                    EndAddressId = 1,
                    EndAddress = addresses[0],
                    StartAddressId = 2,
                    StartAddress = addresses[1],
                    FreeSeats = 2,
                    TravelComment = new TravelComment
                        {
                            Id = 1,
                            Comment = "Can not smoke!",
                            TravelId = 1
                        },
                    TravelStatus = TravelStatus.Open,
                    TravelPassengers = travelPassengers.Where(x=> x.Id == 1 || x.Id == 2).ToList(),
                },
                new Travel
                {
                    Id = 2,
                    DepartureTime = DateTime.Parse("05/08/2020"),
                    DriverId = 2,
                    Driver = customers[1],
                    EndAddressId = 1,
                    EndAddress = addresses[0],
                    StartAddressId = 2,
                    StartAddress = addresses[1],
                    FreeSeats = 3,
                    TravelComment = new TravelComment
                        {
                            Id = 1,
                            Comment = "Can not smoke!",
                            TravelId = 1
                        },
                    TravelStatus = TravelStatus.Open,
                    TravelPassengers = travelPassengers.Where(x=> x.Id == 3).ToList(),
                },
                new Travel
                {
                    Id = 3,
                    DepartureTime = DateTime.Parse("05/08/2025"),
                    DriverId = 3,
                    Driver = customers[2],
                    EndAddressId = 1,
                    EndAddress = addresses[0],
                    StartAddressId = 2,
                    StartAddress = addresses[1],
                    FreeSeats = 4,
                    TravelComment = new TravelComment
                        {
                            Id = 1,
                            Comment = "Can not smoke!",
                            TravelId = 1
                        },
                    TravelStatus = TravelStatus.Travelling,
                    TravelPassengers = travelPassengers.Where(x=> x.Id == 4).ToList(),
                },
            };

            var carsMock = cars.AsQueryable().BuildMockDbSet();
            var rolesMock = roles.AsQueryable().BuildMockDbSet();
            var countriesMock = coutries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var travelPassengersMock = travelPassengers.AsQueryable().BuildMockDbSet();
            var travelsMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Cars).Returns(carsMock.Object);
            dbMock.Setup(context => context.Roles).Returns(rolesMock.Object);
            dbMock.Setup(context => context.Countries).Returns(countriesMock.Object);
            dbMock.Setup(context => context.Cities).Returns(citiesMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressesMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.TravelPassengers).Returns(travelPassengersMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelsMock.Object);

            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetAvatarAsync(It.IsAny<int>(), It.IsAny<bool>()))
                             .ReturnsAsync(new AvatarDto { AvatarId = 1, ImageDataURL = "updatedfakeUrl", Alt = "updatedfakeAlt" });

            this.avatarService = fakeAvatarService.Object;

            var fakeEmailVerigication = new Mock<IEmailService>();
            this.emailVerification = fakeEmailVerigication;
        }

        [TestMethod]
        public async Task Remove_Customer_WhoIsNotInTravel()
        {
            int customerId = 7;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.DeleteCustomerAsync(customerId);
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == customerId);

            this.fakeDb.Verify(db => db.Customers.Remove(It.IsAny<Customer>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.IsNull(customer.Email);
            Assert.IsNull(customer.PhoneNumber);
            Assert.IsNull(customer.Username);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.DeleteCustomerAsync(500));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Remove_Passenger()
        {
            int customerId = 4;
            int expectedTravelFreeSpace = this.fakeDb.Object.Travels.FirstOrDefault(x => x.Id == 1).FreeSeats + 1;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.DeleteCustomerAsync(customerId);
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == customerId);
            int actualTravelFeeSpace = this.fakeDb.Object.TravelPassengers.FirstOrDefault(x => x.PassengerId == customerId).Travel.FreeSeats;

            this.fakeDb.Verify(db => db.Travels.Update(It.IsAny<Travel>()), Times.Once);
            this.fakeDb.Verify(db => db.TravelPassengers.RemoveRange(It.IsAny<TravelPassenger>()), Times.Once);
            this.fakeDb.Verify(db => db.Customers.Remove(It.IsAny<Customer>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.IsNull(customer.Email);
            Assert.IsNull(customer.PhoneNumber);
            Assert.IsNull(customer.Username);
            Assert.AreEqual(expectedTravelFreeSpace, actualTravelFeeSpace);
        }

        [TestMethod]
        public async Task Remove_Driver()
        {
            int customerId = 2;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.DeleteCustomerAsync(customerId);
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == customerId);

            this.fakeDb.Verify(db => db.Cars.Remove(It.IsAny<Car>()), Times.Once);
            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.IsNull(customer.Email);
            Assert.IsNull(customer.PhoneNumber);
            Assert.IsNull(customer.Username);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_DriverHasTravelWhichIsOnTravelling()
        {
            string expected = "You must finish the travel first!";
            int customerId = 3;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<DriverException>(() => service.DeleteCustomerAsync(customerId));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_PassengerHasTravelWhichIsOnTravelling()
        {
            string expected = "Please finish or cancel your travel/s first!";
            int customerId = 6;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<PassengerException>(() => service.DeleteCustomerAsync(customerId));

            Assert.AreEqual(expected, result.Message);
        }
    }
}