﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Mapper.Contracts;

namespace TangraTravel.Service.Test.CustomerServiceTest
{
    [TestClass]
    public class Confirm_Customer
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private IAvatarService avatarService;
        private Mock<IEmailService> emailVerification;

        [TestInitialize]
        public void SetUp()
        {
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 5005,
                    ConfirmationString = "confirm"
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            this.avatarService = fakeAvatarService.Object;

            var fakeEmailVerigication = new Mock<IEmailService>();
            this.emailVerification = fakeEmailVerigication;
        }

        [TestMethod]
        public async Task ConfirmUser()
        {
            string verification = "confirm";
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == 5005);

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            await service.ConfirmUser(verification);

            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.IsTrue(customer.IsVerified);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.ConfirmUser("wrong"));

            Assert.AreEqual(expected, result.Message);
        }
    }
}