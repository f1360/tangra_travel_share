﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Service.Enum;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.AvatarDtos;
using TangraTravel.Service.Models.CustomerDtos;

namespace TangraTravel.Service.Test.CustomerServiceTest
{
    [TestClass]
    public class Get_Customer
    {
        private Mock<TangraTravelContext> fakeDb;
        private ICustomExpression func = new CustomExpression();
        private IAvatarService avatarService;
        private Mock<IEmailService> emailVerification;

        [TestInitialize]
        public void SetUp()
        {
            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Access = Access.Admin,
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Driver,
                },
                new Role
                {
                    Id = 2,
                    Access = Access.Passenger,
                },
            };
            var coutries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria",
                },
                new Country
                {
                    Id = 2,
                    Name = "Germany"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    CountryId = 1,
                    Country = coutries[0],
                    Name = "Sofia"
                },
                new City
                {
                    Id = 2,
                    CountryId = 2,
                    Country = coutries[1],
                    Name = "Berlin"
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    CityId = 1,
                    City = cities[0],
                    StreetName = "Sofia_Address"
                },
                new Address
                {
                    Id = 2,
                    CityId = 2,
                    City = cities[1],
                    StreetName = "Berlin_Address"
                }
            };
            var customers = new List<Customer>();
            for (int i = 1; i <= 11; i++)
            {
                var driver = new Customer
                {
                    Id = i,
                    FirstName = "firstname" + i,
                    LastName = "lastname" + i,
                    Email = $"fake{i}@email.com",
                    Gender = Gender.Man,
                    Username = "username" + i,
                    Rating = i,
                    PhoneNumber = "fakephone" + i,
                    RoleId = 2,
                    Role = new Role
                    {
                        Id = 2,
                        Access = Access.Driver,
                    },
                    IsBlocked = false,
                    IsVerified = true,
                    AddressId = 1,
                    Address = addresses[0]
                };

                customers.Add(driver);
            }
            for (int i = 1; i <= 11; i++)
            {
                var passenger = new Customer
                {
                    Id = (11 + i),
                    FirstName = "firstname" + (11 + i),
                    LastName = "lastname" + (11 + i),
                    Email = $"fake{(11 + i)}@email.com",
                    Username = "username" + (11 + i),
                    Rating = 11 + i,
                    PhoneNumber = "fakephone" + (11 + i),
                    RoleId = 3,
                    Role = new Role
                    {
                        Id = 3,
                        Access = Access.Passenger,
                    },
                    IsBlocked = false,
                    IsVerified = true,
                    AddressId = 2,
                    Address = addresses[1]
                };

                customers.Add(passenger);
            }
            var travelPassengers = new List<TravelPassenger>
            {
                new TravelPassenger
                {
                    Id = 1,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 13,
                    Passenger = customers[12],
                    TravelId = 1,
                },
                new TravelPassenger
                {
                    Id = 2,
                    ApplyStatus = ApplyStatus.Accepted,
                    PassengerId = 14,
                    Passenger = customers[13],
                    TravelId = 1
                },
                new TravelPassenger
                {
                    Id = 3,
                    ApplyStatus = ApplyStatus.Rejected,
                    PassengerId = 9,
                    Passenger = customers[8],
                    TravelId = 1
                },
                new TravelPassenger
                {
                    Id = 4,
                    ApplyStatus = ApplyStatus.Applied,
                    PassengerId = 5,
                    Passenger = customers[4],
                    TravelId = 1
                }
            };
            var travels = new List<Travel>
            {
                new Travel
                {
                    Id = 1,
                    DepartureTime = DateTime.Now.AddDays(15),
                    DriverId = 1,
                    Driver = customers[0],
                    EndAddressId = 1,
                    EndAddress = addresses[0],
                    StartAddressId = 2,
                    StartAddress = addresses[1],
                    FreeSeats = 2,
                    TravelComment = new TravelComment
                        {
                            Id = 1,
                            Comment = "Can not smoke!",
                            TravelId = 1
                        },
                    TravelStatus = TravelStatus.Open,
                    TravelPassengers = travelPassengers,
                },
                new Travel
                {
                    Id = 2,
                    DepartureTime = DateTime.Parse("05/08/2020"),
                    DriverId = 5,
                    Driver = customers[4],
                    EndAddressId = 1,
                    EndAddress = addresses[0],
                    StartAddressId = 2,
                    StartAddress = addresses[1],
                    FreeSeats = 4,
                    TravelComment = new TravelComment
                        {
                            Id = 1,
                            Comment = "Can not smoke!",
                            TravelId = 1
                        },
                    TravelStatus = TravelStatus.Completed,
                    TravelPassengers = new List<TravelPassenger>(),
                },
            };

            var rolesMock = roles.AsQueryable().BuildMockDbSet();
            var countriesMock = coutries.AsQueryable().BuildMockDbSet();
            var citiesMock = cities.AsQueryable().BuildMockDbSet();
            var addressesMock = addresses.AsQueryable().BuildMockDbSet();
            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var travelPassengersMock = travelPassengers.AsQueryable().BuildMockDbSet();
            var travelsMock = travels.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Roles).Returns(rolesMock.Object);
            dbMock.Setup(context => context.Countries).Returns(countriesMock.Object);
            dbMock.Setup(context => context.Cities).Returns(citiesMock.Object);
            dbMock.Setup(context => context.Addresses).Returns(addressesMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            dbMock.Setup(context => context.TravelPassengers).Returns(travelPassengersMock.Object);
            dbMock.Setup(context => context.Travels).Returns(travelsMock.Object);

            this.fakeDb = dbMock;

            var fakeAvatarService = new Mock<IAvatarService>();
            fakeAvatarService.Setup(x => x.GetAvatarAsync(It.IsAny<int>(), It.IsAny<bool>()))
                             .ReturnsAsync(new AvatarDto { AvatarId = 1, ImageDataURL = "fakeUrl", Alt = "fakeAlt" });
            this.avatarService = fakeAvatarService.Object;

            var fakeEmailVerigication = new Mock<IEmailService>();
            this.emailVerification = fakeEmailVerigication;
        }

        [TestMethod]
        public async Task Get_Selected_Customer()
        {
            int customerId = 1;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await service.GetCustomerAsync(customerId);

            Assert.AreEqual(1, result.CustomerId);
            Assert.AreEqual("firstname1", result.FirstName);
            Assert.AreEqual("lastname1", result.LastName);
            Assert.AreEqual("firstname1 lastname1", result.FullName);
            Assert.AreEqual(Gender.Man, result.Gender);
            Assert.AreEqual("fake1@email.com", result.Email);
            Assert.AreEqual("username1", result.Username);
            Assert.AreEqual("fakephone1", result.PhoneNumber);
            Assert.AreEqual(1, result.Rating);
            Assert.AreEqual(1, result.Address.AddressId);
            Assert.AreEqual("Sofia_Address", result.Address.StreetAddress);
            Assert.AreEqual(1, result.Address.CityId);
            Assert.AreEqual("Sofia", result.Address.City);
            Assert.AreEqual(1, result.Address.CountryId);
            Assert.AreEqual("Bulgaria", result.Address.Country);
            Assert.AreEqual(1, result.Avatar.AvatarId);
            Assert.AreEqual("fakeUrl", result.Avatar.ImageDataURL);
            Assert.AreEqual("fakeAlt", result.Avatar.Alt);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerNotExist()
        {
            int customerId = 150;
            string expected = "There is no user in the system!";

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.GetCustomerAsync(customerId));

            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod]
        public async Task Get_10Drivers()
        {
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var drivers = (await service.GetTopTenDriversAsync()).ToArray();
            var noDriver = drivers.Any(d => d.SystemStatus != "Driver");

            Assert.AreEqual(10, drivers.Count());
            Assert.IsFalse(noDriver);
            Assert.AreEqual(11, drivers[0].Rating);
            Assert.IsNotNull(drivers[0].Avatar);
            Assert.AreEqual(2, drivers[9].Rating);
            Assert.IsNotNull(drivers[9].Avatar);
        }

        [TestMethod]
        public async Task Get_10Passengers()
        {
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var passengers = (await service.GetTopTenPassengersAsync()).ToArray();

            Assert.AreEqual(10, passengers.Count());
            Assert.AreEqual(22, passengers[0].Rating);
            Assert.IsNotNull(passengers[0].Avatar);
            Assert.AreEqual(13, passengers[9].Rating);
            Assert.IsNotNull(passengers[9].Avatar);
        }

        [TestMethod]
        [DataRow(1, 10)]
        [DataRow(2, 7)]
        [DataRow(3, null)]
        [DataRow(50, 22)]
        [DataRow(-1, null)]
        public async Task Get_CustomersByPage(int page, int? byPage)
        {
            int? expected = byPage;
            if (byPage == null)
            {
                expected = 5;
            }
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var customers = await service.GetCustomersByPageAsync(page, byPage);
            var isAvatar = customers.Any(x => x.Avatar == null);

            Assert.AreEqual(expected, customers.Count());
            Assert.IsFalse(isAvatar);
        }

        [TestMethod]
        [DataRow(SearchBy.Email, "fake5@email.com")]
        [DataRow(SearchBy.Username, "username7")]
        [DataRow(SearchBy.Phone, "fakephone13")]
        public async Task Get_CustomerBy_Search(SearchBy criteria, string value)
        {
            var input = new CustomerSearchByDto { Criteria = criteria, Value = value };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var customer = await service.SearchAsync(input);

            Assert.IsNotNull(customer);
            Assert.IsNotNull(customer.Avatar);
            if (criteria == SearchBy.Email)
            {
                Assert.AreEqual(value, customer.Email);
            }
            else if (criteria == SearchBy.Username)
            {
                Assert.AreEqual(value, customer.Username);
            }
            else
            {
                Assert.AreEqual(value, customer.PhoneNumber);
            }
        }

        [TestMethod]
        public async Task Should_Return_Null_When_ThereIsNoCustomerInSearchMethod()
        {
            var input = new CustomerSearchByDto { Criteria = SearchBy.Email, Value = "noMatch" };

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var customer = await service.SearchAsync(input);

            Assert.IsNull(customer);
        }

        [TestMethod]
        [DataRow(1, 22)]
        [DataRow(5, 5)]
        [DataRow(15, 2)]
        public async Task Get_MaxPage(int byPage, int expected)
        {
            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var result = await service.MaxPage(byPage);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public async Task Get_CollectionOfTravelPassengers()
        {
            int travelId = 1;

            var service = new CustomerService(this.fakeDb.Object, this.avatarService, this.func, this.emailVerification.Object);
            var travelPassengers = (await service.ListApplicationsAsync(travelId)).ToList();

            Assert.AreEqual(4, travelPassengers.Count());
            Assert.AreEqual(ApplyStatus.Accepted, travelPassengers[0].ApplyStatus);
            Assert.AreEqual("firstname13 lastname13", travelPassengers[0].PassengerName);
            Assert.AreEqual(1, travelPassengers[0].TravelId);
            Assert.AreEqual(1, travelPassengers[0].TravelPassengerId);
            Assert.AreEqual(ApplyStatus.Rejected, travelPassengers[2].ApplyStatus);
        }        
    }
}