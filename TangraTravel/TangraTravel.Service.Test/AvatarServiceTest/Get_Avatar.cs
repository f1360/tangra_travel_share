﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Service;
using TangraTravel.Service.Models.Cloud;
using TangraTravel.Service.Models.Provider;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Test.AvatarServiceTest
{
    [TestClass]
    public class Get_Avatar
    {
        private Mock<TangraTravelContext> fakeDb;
        private Mock<ICloud> cloud;
        private Mock<IWebHostEnvironment> host;
        private Mock<IProvider> provider;
        private readonly string directory = Directory.GetCurrentDirectory();

        [TestInitialize]
        public void SetUp()
        {
            var cloudMock = new Mock<ICloud>();
            cloudMock.Setup(c => c.Uri).Returns("fakeUri");
            cloudMock.Setup(c => c.PublicId).Returns("fakePublicId");
            this.cloud = cloudMock;

            var avatars = new List<Avatar>
            {
                new Avatar
                {
                    Id = 1,
                    PictureUri = "admin-uri.jpg",
                    Alternative = "admin"
                },
                new Avatar
                {
                    Id = 2,
                    PictureUri = "customer-uri.jpg",
                    Alternative = "customer"
                }
            };
            var admins = new List<Admin>
            {
                new Admin
                {
                    Id = 1,
                    Email = "admin@tangratravel.com",
                    AvatarId = 1,
                    Avatar = avatars[0]
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Email = "customer@abv.bg",
                    AvatarId = 2,
                    Avatar = avatars[1]
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "fakeFirstName",
                    LastName = "fakeLastName",
                    Email = "default@abv.bg"
                }
            };

            var avatarMock = avatars.AsQueryable().BuildMockDbSet();
            var adminMock = admins.AsQueryable().BuildMockDbSet();
            var customerMock = customers.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Avatars).Returns(avatarMock.Object);
            dbMock.Setup(context => context.Admins).Returns(adminMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            this.fakeDb = dbMock;

            var fakeHost = new Mock<IWebHostEnvironment>();
            fakeHost.Setup(x => x.WebRootPath).Returns(this.directory);
            this.host = fakeHost;

            var fakeProvider = new Mock<IProvider>();
            fakeProvider.Setup(x => x.UserAvatarName(It.IsAny<string>(),It.IsAny<string>(), It.IsAny<Gender>())).Returns("fakeCustomerAvatar");
            fakeProvider.Setup(x => x.CarAvatarName(It.IsAny<string>(),It.IsAny<string>(), It.IsAny<string>())).Returns("fakeCarAvatar");
            this.provider = fakeProvider;
        }

        [TestMethod]
        [DataRow(true, "admin-uri.jpg", "admin", 1)]
        [DataRow(false, "customer-uri.jpg", "customer", 2)]
        public async Task Get_Avatar_AccordingToRole(bool isAdmin, string imageDataUrl, string alt, int avatarId)
        {
            int userId = 1;

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            var avatart = await service.GetAvatarAsync(userId, isAdmin);
            Assert.AreEqual(avatarId, avatart.AvatarId);
            Assert.AreEqual(imageDataUrl, avatart.ImageDataURL);
            Assert.AreEqual(alt, avatart.Alt);
        }

        [TestMethod]
        public async Task Get_Default_Avatar()
        {
            int userId = 2;
            string expectedImageDataUrl = "/avatar/fakeCustomerAvatar";

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            var avatar = await service.GetAvatarAsync(userId, isAdmin: false);

            Assert.IsNull(avatar.AvatarId);
            Assert.AreEqual(expectedImageDataUrl, avatar.ImageDataURL);
            Assert.AreEqual("fakeFirstName fakeLastName", avatar.Alt);
            Assert.IsInstanceOfType(avatar, typeof(AvatarDto));
        }

        [TestMethod]
        [DataRow("BMW")]
        [DataRow("Mercedes-Benz")]
        [DataRow("AUDI")]
        public void Get_Car_Avatar(string brand)
        {
            string expectedUrl = @"/brands/fakeCarAvatar";
            string expectedAlt = brand;

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            var avatar = service.GetCarAvatar(brand);

            Assert.AreEqual(expectedUrl, avatar.ImageUrl);
            Assert.AreEqual(expectedAlt, avatar.Alt);
            Assert.IsInstanceOfType(avatar, typeof(CarAvatarDto));
        }
    }
}