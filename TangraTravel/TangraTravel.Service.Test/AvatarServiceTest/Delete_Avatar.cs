﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Service;
using TangraTravel.Service.Models.Cloud;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Models.Provider;

namespace TangraTravel.Service.Test.AvatarServiceTest
{
    [TestClass]
    public class Delete_Avatar
    {
        private Mock<TangraTravelContext> fakeDb;
        private Mock<ICloud> cloud;
        private Mock<IWebHostEnvironment> host;
        private Mock<IProvider> provider;

        [TestInitialize]
        public void SetUp()
        {
            var cloudMock = new Mock<ICloud>();
            this.cloud = cloudMock;

            var avatars = new List<Avatar>
            {
                new Avatar
                {
                    Id = 1,
                    PictureUri = "customer-uri.jpg",
                    Alternative = "customer",
                    PublicId = "fakePublicity"
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Email = "customer@abv.bg",
                    AvatarId = avatars[0].Id,
                    Avatar = avatars[0]
                },
                new Customer
                {
                    Id = 2,
                    FirstName = "fakeFirstName",
                    LastName = "fakeLastName",
                    Email = "default@abv.bg"
                }
            };

            var avatarMock = avatars.AsQueryable().BuildMockDbSet();
            var customerMock = customers.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(context => context.Avatars).Returns(avatarMock.Object);
            dbMock.Setup(context => context.Customers).Returns(customerMock.Object);
            this.fakeDb = dbMock;

            var fakeHost = new Mock<IWebHostEnvironment>();
            this.host = fakeHost;

            var fakeProvider = new Mock<IProvider>();
            this.provider = fakeProvider;
        }

        [TestMethod]
        public async Task Delete_Customer_Avatar()
        {
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == 1);

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            await service.DeleteAvatarAsync(customer);

            this.fakeDb.Verify(db => db.Avatars.Remove(It.IsAny<Avatar>()), Times.Once);
            this.cloud.Verify(c => c.DeleteAsync(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.DeleteAvatarAsync(null));

            Assert.AreEqual(expected, result.Message);
        }
    }
}