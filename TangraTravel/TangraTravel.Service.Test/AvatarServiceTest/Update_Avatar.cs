﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CloudinaryDotNet.Actions;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Service;
using TangraTravel.Service.Models.Cloud;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Models.Provider;

namespace TangraTravel.Service.Test.AvatarServiceTest
{
    [TestClass]
    public class Update_Avatar
    {
        private Mock<TangraTravelContext> fakeDb;
        private Mock<ICloud> cloud;
        private Mock<IWebHostEnvironment> host;
        private Mock<IProvider> provider;
        private Mock<IFormFile> formFile;

        [TestInitialize]
        public void SetUp()
        {
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Email = "fake1@abv.bg",
                    Username = "fakeUsername1",
                    FirstName = "fakeFirstName1",
                    LastName = "fakeLastName1",
                    AvatarId = 1,
                    Avatar = new Avatar
                    {
                        Id = 1,
                        //Customer = customers[0],
                        FileName = "initialFileName",
                        Alternative = "initialAlternative",
                        PictureUri = "initialPictureUri",
                        PublicId = "initialPublicId"
                    }
                },
                new Customer
                {
                    Id = 2,
                    Email = "fake2@abv.bg",
                    Username = "fakeUsername2",
                    FirstName = "fakeFirstName2",
                    LastName = "fakeLastName2"
                }
            };
            var avatars = new List<Avatar>
            {
                new Avatar
                {
                    Id = 1,
                    Customer = customers[0],
                    FileName = "initialFileName",
                    Alternative = "initialAlternative",
                    PictureUri = "initialPictureUri",
                    PublicId = "initialPublicId"
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();
            var avatarMock = avatars.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(x => x.Customers).Returns(customerMock.Object);
            dbMock.Setup(x => x.Avatars).Returns(avatarMock.Object);
            this.fakeDb = dbMock;

            var cloudMock = new Mock<ICloud>();
            cloudMock.Setup(x => x.Uri).Returns("fakeUri");
            cloudMock.Setup(x => x.PublicId).Returns("fakePublicId");
            this.cloud = cloudMock;

            var fakeHost = new Mock<IWebHostEnvironment>();
            this.host = fakeHost;

            var fakeProvider = new Mock<IProvider>();
            this.provider = fakeProvider;

            var formFileMock = new Mock<IFormFile>();
            this.formFile = formFileMock;
        }

        [TestMethod]
        public async Task Update_ExistedAvatar()
        {
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == 1);
            var avatar = this.fakeDb.Object.Avatars.FirstOrDefault(x => x.Customer.Id == customer.Id);

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            await service.UpdateAvatarAsync(customer, this.formFile.Object);

            this.cloud.Verify(x => x.UploadAsync(It.IsAny<ImageUploadParams>()), Times.Once);
            this.cloud.Verify(x => x.DeleteAsync(It.IsAny<string>()), Times.Once);
            Assert.AreEqual("fake1@abv.bg", avatar.FileName);
            Assert.AreEqual("fakeFirstName1 fakeLastName1", avatar.Alternative);
            Assert.AreEqual("fakeUri", avatar.PictureUri);
            Assert.AreEqual("fakePublicId", avatar.PublicId);
        }

        [TestMethod]
        public async Task Create_Avatar_DuringUpdation()
        {
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == 2);

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            await service.UpdateAvatarAsync(customer, this.formFile.Object);

            this.cloud.Verify(x => x.UploadAsync(It.IsAny<ImageUploadParams>()), Times.Once);
            this.cloud.Verify(x => x.DeleteAsync(It.IsAny<string>()), Times.Never);
            Assert.AreEqual("fake2@abv.bg", customer.Avatar.FileName);
            Assert.AreEqual("fakeFirstName2 fakeLastName2", customer.Avatar.Alternative);
            Assert.AreEqual("fakeUri", customer.Avatar.PictureUri);
            Assert.AreEqual("fakePublicId", customer.Avatar.PublicId);
        } 

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.UpdateAvatarAsync(null, this.formFile.Object));

            Assert.AreEqual(expected, result.Message);
        }
    }
}