﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using MockQueryable.Moq;

using CloudinaryDotNet.Actions;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Service;
using TangraTravel.Service.Models.Cloud;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Models.Provider;

namespace TangraTravel.Service.Test.AvatarServiceTest
{
    [TestClass]
    public class Create_Avatar
    {
        private Mock<TangraTravelContext> fakeDb;
        private Mock<ICloud> cloud;
        private Mock<IWebHostEnvironment> host;
        private Mock<IProvider> provider;
        private Mock<IFormFile> formFile;

        [TestInitialize]
        public void SetUp()
        {
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Email = "fake@abv.bg",
                    Username = "fakeUsername",
                    FirstName = "fakeFirstName",
                    LastName = "fakeLastName"
                }
            };

            var customerMock = customers.AsQueryable().BuildMockDbSet();

            var dbMock = new Mock<TangraTravelContext>();
            dbMock.Setup(x => x.Customers).Returns(customerMock.Object);
            this.fakeDb = dbMock;

            var cloudMock = new Mock<ICloud>();
            cloudMock.Setup(x => x.Uri).Returns("fakeUri");
            cloudMock.Setup(x => x.PublicId).Returns("fakePublicId");
            this.cloud = cloudMock;

            var fakeHost = new Mock<IWebHostEnvironment>();
            this.host = fakeHost;

            var fakeProvider = new Mock<IProvider>();
            this.provider = fakeProvider;

            var formFileMock = new Mock<IFormFile>();
            this.formFile = formFileMock;
        }

        [TestMethod]
        public async Task Create_CustomerAvatar()
        {
            var customer = this.fakeDb.Object.Customers.FirstOrDefault(x => x.Id == 1);

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            await service.CreateAvatarAsync(customer, this.formFile.Object);

            this.fakeDb.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Never);
            this.cloud.Verify(x => x.UploadAsync(It.IsAny<ImageUploadParams>()), Times.Once);
            Assert.IsNotNull(customer.Avatar);
            Assert.AreEqual("fake@abv.bg", customer.Avatar.FileName);
            Assert.AreEqual("fakeUri", customer.Avatar.PictureUri);
            Assert.AreEqual("fakePublicId", customer.Avatar.PublicId);
            Assert.AreEqual("fakeFirstName fakeLastName", customer.Avatar.Alternative);
        }

        [TestMethod]
        public async Task Should_ThrowException_When_CustomerNotExist()
        {
            string expected = "There is no user in the system!";

            var service = new AvatarService(this.fakeDb.Object, this.host.Object, this.cloud.Object, this.provider.Object);
            var result = await Assert.ThrowsExceptionAsync<UserNotExistException>(() => service.CreateAvatarAsync(null, this.formFile.Object));

            Assert.AreEqual(expected, result.Message);
        }
    }
}