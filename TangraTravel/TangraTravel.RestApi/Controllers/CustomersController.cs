﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using TangraTravel.Data.Enum;
using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.RestApi.Controllers.Abstract;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : AbstractController
    {
        private readonly ICustomerService service;

        public CustomersController(ICustomerService service, IUserAuthService authService)
            : base(authService)
        {
            this.service = service;
        }

        /// <summary>
        /// Admin can see all customers in the system
        /// </summary>
        /// <param name="email"></param>
        /// <param name="page"></param>
        /// <param name="perPage"></param>
        /// <returns>Collection from customers</returns>
        [HttpGet("")]
        public async Task<IActionResult> GetByPage([FromHeader] string email, int page, int? perPage)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            var customers = await this.service.GetCustomersByPageAsync(page, perPage);
            return this.Ok(customers);
        }

        /// <summary>
        /// Customer take personal information
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Personal information</returns>
        [HttpGet("personal-information")]
        
        public async Task<IActionResult> Get([FromHeader] string email)
        {
            User user = await this.LogInAsync(email, Access.Passenger, Access.Driver);

            var customer = await this.service.GetCustomerAsync(user.Id);
            return this.Ok(customer);
        }

        /// <summary>
        /// Admin take personal information
        /// </summary>
        /// <param name="email"></param>
        /// <param name="customerId"></param>
        /// <returns>Personal information</returns>
        [HttpGet("{customerId}")]
        public async Task<IActionResult> Get([FromHeader] string email, int customerId)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            var customer = await this.service.GetCustomerAsync(customerId);
            return this.Ok(customer);
        }

        /// <summary>
        /// Search by email, username or phone number
        /// </summary>
        /// <param name="email"></param>
        /// <param name="input"></param>
        /// <returns>Intended user or that such a user does not exist</returns>
        [HttpGet("search-by")]
        public async Task<IActionResult> Search([FromHeader] string email, [FromQuery] CustomerSearchByDto input)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            var customer = await this.service.SearchAsync(input);
            return this.Ok(customer);
        }

        /// <summary>
        /// Creating customer in the system
        /// </summary>
        /// <param name="input"></param>
        /// <returns>URI and created customer</returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromForm] CustomerCreateDto input)
        {
            var customer = await this.service.CreateAsync(input);
            return this.Created($"customers/{customer.CustomerId}", customer);
        }

        /// <summary>
        /// Customer can update his profile
        /// </summary>
        /// <param name="email"></param>
        /// <param name="input"></param>
        /// <returns>Update profile</returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpPut]
        public async Task<IActionResult> Update([FromHeader] string email, [FromForm] CustomerUpdateDto input)
        {
            User user = await this.LogInAsync(email, Access.Passenger, Access.Driver);

            var customer = await this.service.UpdateAsync(user.Id, input);
            return this.Ok(customer);
        }

        /// <summary>
        /// User can delete his email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromHeader] string email)
        {
            User user = await this.LogInAsync(email, Access.Passenger, Access.Driver);

            await this.service.DeleteCustomerAsync(user.Id);
            return this.Ok("User was deleted succesfully");
        }

        /// <summary>
        /// Admin can delete user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpDelete("user/{customerId}")]
        public async Task<IActionResult> Delete([FromHeader] string email, int customerId)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            await this.service.DeleteCustomerAsync(customerId);
            return this.Ok("User was deleted succesfully");
        }

        /// <summary>
        /// Admin blocks selected customer
        /// </summary>
        /// <param name="email"></param>
        /// <param name="customerId"></param>
        /// <returns>If it is blocked</returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpPut("block-user/{customerId}")]
        public async Task<IActionResult> Block([FromHeader] string email, int customerId)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            await this.service.BlockAsync(customerId);
            return this.Ok($"User with Id:{customerId} was blocked!");
        }

        /// <summary>
        /// Admin unblocks selected customer
        /// </summary>
        /// <param name="email"></param>
        /// <param name="customerId"></param>
        /// <returns>If it is unblocked</returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpPut("unblock-user/{customerId}")]
        public async Task<IActionResult> Unblock([FromHeader] string email, int customerId)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            await this.service.UnblockAsync(customerId);
            return this.Ok($"User with Id:{customerId} was unblocked!");
        }

        /// <summary>
        /// Customer applies to take part in a travel
        /// </summary>
        /// <param name="email"></param>
        /// <param name="travelId"></param>
        /// <returns>JSON record of the application</returns>
        /// <Author>Viet Chan</Author>
        [HttpPost("travelApplications/{travelId}")]
        public async Task<IActionResult> ApplyForTravel([FromHeader] string email, int travelId)
        {
            User user = await this.LogInAsync(email, Access.Passenger, Access.Driver);

            var travelApplicationDto = await this.service.ApplyForTravelAsync(user.Id, travelId, user.Role.Access);

            return this.Ok(travelApplicationDto);
        }

        /// <summary>
        /// Driver lists all applicants for a selected travel
        /// </summary>
        /// <param name="email"></param>
        /// <param name="travelId"></param>
        /// <returns>Collection of applications</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("travelApplications/{travelId}")]
        public async Task<IActionResult> ListApplications([FromHeader] string email, int travelId)
        {
            User user = await this.LogInAsync(email, Access.Driver);

            var travelApplicationDtos = await this.service.ListApplicationsAsync(travelId);

            return this.Ok(travelApplicationDtos);
        }

        /// <summary>
        /// Driver approves/rejects an applicant
        /// </summary>
        /// <param name="email"></param>
        /// <param name="travelPassengerId"></param>
        /// <param name="inputData"></param>
        /// <returns>OK status with message</returns>
        /// <Author>Viet Chan</Author>
        [HttpPut("travelApplications/{travelPassengerId}")]
        public async Task<IActionResult> ProcessApplication([FromHeader] string email, int travelPassengerId, [FromForm] TravelPassengerApplyStatusDto inputData)
        {
            User user = await this.LogInAsync(email, Access.Driver);

            await this.service.ProcessApplicationAsync(travelPassengerId, inputData);

            return this.Ok($"The application has been successfully processed!");
        }
    }
}