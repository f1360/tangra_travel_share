﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using TangraTravel.Data.Enum;
using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.RestApi.Controllers.Abstract;

namespace TangraTravel.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : AbstractController
    {
        private readonly ICarService carService;

        public CarsController(ICarService carService, IUserAuthService authService)
            : base(authService)
        {
            this.carService = carService;
        }

        /// <summary>
        /// Driver retrive information about his the car
        /// </summary>
        /// <param name="carId"></param>
        /// <param name="email"></param>
        /// <returns>Information for selected Car</returns>
        /// <Author>Nikolay Veliko</Author>
        [HttpGet("user/get-car")]
        public async Task<IActionResult> DriverGet([FromHeader] string email)
        {
            User user = await this.LogInAsync(email, Access.Driver);

            var car = await this.carService.GetByUerIdAsync(user.Id);
            return this.Ok(car);
        }

        /// <summary>
        /// Admid check selected car in the system
        /// </summary>
        /// <param name="carId"></param>
        /// <param name="email"></param>
        /// <returns>Information for selected Car</returns>
        /// <Author>Nikolay Veliko</Author>
        [HttpGet("check-car/{carId}")]
        public async Task<IActionResult> AdminCheck([FromHeader] string email, [FromRoute] int carId)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            var car = await this.carService.GetByCarIdAsync(carId);
            return this.Ok(car);
        }

        /// <summary>
        /// Get all car from data base
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Return collection from cars</returns>
        [HttpGet("")]
        public async Task<IActionResult> GetPerPage([FromHeader] string email, int page, int? perPage)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            var cars = await this.carService.GetByPageAsync(page, perPage);
            return this.Ok(cars);
        }

        /// <summary>
        /// Registration of the car in the system and switch to Driver
        /// </summary>
        /// <param name="email"></param>
        /// <param name="input"></param>
        /// <returns>Created car</returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpPost("user/register-car")]
        public async Task<IActionResult> Create([FromHeader] string email, [FromBody] CarCreateDto input)
        {
            User user = await this.LogInAsync(email, Access.Passenger);

            var car = await this.carService.CreateAsync(user.Id, input);
            return this.Created($"user/get-car/{car.CarId}", car);
        }

        /// <summary>
        /// Update registration number of the car
        /// </summary>
        /// <param name="email"></param>
        /// <param name="input"></param>
        /// <returns>Update car</returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpPut("user/update-car")]
        public async Task<IActionResult> Update([FromHeader] string email, [FromBody] CarUpdateDto input)
        {
            User user = await this.LogInAsync(email, Access.Driver);

            var car = await this.carService.UpdateAsync(user.Id, input);
            return this.Ok(car);
        }

        /// <summary>
        /// Delete selected car in the system and swtich Role to Passenger
        /// </summary>
        /// <param name="email"></param>
        /// <param name="carId"></param>
        /// <returns>Empty content</returns>
        /// <Author>Nikolay Velikov</Author>
        [HttpDelete("")]
        public async Task<IActionResult> Delete([FromHeader] string email)
        {
            User user = await this.LogInAsync(email, Access.Driver);

            await this.carService.DeleteAsync(user.Id);
            return this.Ok("Deleted");
        }
    }
}