﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using TangraTravel.Data.Enum;
using TangraTravel.Service.Enum;
using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Models.FeedbackDtos;
using TangraTravel.RestApi.Controllers.Abstract;

namespace TangraTravel.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeedbackController : AbstractController
    {
        private readonly IFeedbackService service;

        public FeedbackController(IFeedbackService service, IUserAuthService authorService)
            : base(authorService)
        {
            this.service = service;
        }

        /// <summary>
        /// Take all feedbacks
        /// </summary>
        /// <param name="email"></param>
        /// <param name="input"></param>
        [HttpGet("")]
        public async Task<IActionResult> Feedbacks([FromHeader] string email, FeedbackInputDto input)
        {
            User user = await this.LogInAsync(email, Access.Driver, Access.Passenger);

            if (input.TypeFeedback == TypeFeedback.Given)
            {
                return this.Ok(await service.GivenFeedback(user.Id, input.Page, input.Perpage));
            }
            else if (input.TypeFeedback == TypeFeedback.Received)
            {
                return this.Ok(await service.ReceivedFeedback(user.Id, input.Page, input.Perpage));
            }

            return this.NotFound();
        }

        /// <summary>
        /// Driver leave feedback to passenger.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="input"></param>
        [HttpPost("driver-leave-feedback")]
        public async Task<IActionResult> DriverCreateFeedback([FromHeader] string email, [FromBody] FeedbackCreateDto input)
        {
            User user = await this.LogInAsync(email, Access.Driver);

            var result = await this.service.DriverWriteFeedbackAsync(user.Id, input);
            return this.Created("", result);
        }

        /// <summary>
        /// Passenger leave feedback to driver.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="input"></param>
        [HttpPost("passenger-leave-feedback")]
        public async Task<IActionResult> PassengerCreateFeedback([FromHeader] string email, [FromBody] FeedbackCreateDto input)
        {
            User user = await this.LogInAsync(email, Access.Passenger);

            var result = await this.service.PassengerWriteFeedbackAsync(user.Id, input);
            return this.Created("", result);

        }
    }
}