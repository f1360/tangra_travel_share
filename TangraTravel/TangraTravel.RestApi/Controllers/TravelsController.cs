﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using TangraTravel.Data.Enum;
using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.RestApi.Controllers.Abstract;

namespace TangraTravel.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TravelsController : AbstractController
    {
        private readonly ITravelService travelService;

        public TravelsController(ITravelService travelService, IUserAuthService authService)
            : base(authService)
        {
            this.travelService = travelService;
        }

        /// <summary>
        /// Admin retrieves information for all travels in the system
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Collection of travels</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("")]
        public async Task<IActionResult> GetAll([FromHeader] string email)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Admin)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            var travels = await this.travelService.GetAllTravelsAsync();
            return this.Ok(travels);
        }

        /// <summary>
        /// Admin retrieves information about a selected travel
        /// </summary>
        /// <param name="email"></param>
        /// <param name="travelId"></param>
        /// <returns>Travel details</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("{travelId}")]
        public async Task<IActionResult> Get([FromHeader] string email, int travelId)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Admin)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travel = await this.travelService.GetTravelAsync(travelId);
                return this.Ok(travel);
            }
            catch (TravelDoesNotExistException tdnee)
            {
                return this.NotFound(tdnee.Message);
            }
        }

        /// <summary>
        /// Driver creates a new travel
        /// </summary>
        /// <param name="email"></param>
        /// <param name="createInput"></param>
        /// <returns>Created travel details</returns>
        /// <Author>Viet Chan</Author>
        [HttpPost]
        public async Task<IActionResult> Create([FromHeader] string email, [FromForm] TravelCreateDto createInput)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Driver)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travel = await this.travelService.CreateTravelAsync(createInput);
                return this.Ok(travel);
            }
            catch (TravelFailedCreationException tfce)
            {
                return this.BadRequest(tfce.Message);
            }
        }

        /// <summary>
        /// Driver updates a travel
        /// </summary>
        /// <param name="email"></param>
        /// <param name="updateInput"></param>
        /// <param name="travelId"></param>
        /// <returns>Updated travel details</returns>
        /// <Author>Viet Chan</Author>
        [HttpPut("{travelId}")]
        public async Task<IActionResult> Update([FromHeader] string email, [FromForm] TravelUpdateDto updateInput, int travelId)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Driver)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travel = await this.travelService.UpdateTravelAsync(travelId, updateInput);
                return this.Ok(travel);
            }
            catch (TravelDoesNotExistException tdnee)
            {
                return this.BadRequest(tdnee.Message);
            }
        }

        /// <summary>
        /// Driver deletes a travel
        /// </summary>
        /// <param name="email"></param>
        /// <param name="travelId"></param>
        /// <returns></returns>
        /// <Author>Viet Chan</Author>
        [HttpDelete("{travelId}")]
        public async Task<IActionResult> Delete([FromHeader] string email, int travelId)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Driver)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                await this.travelService.DeleteTravelAsync(travelId);
                return this.Ok("The travel has been successfully deleted");
            }
            catch (TravelFailedDeletionException tfde)
            {
                return this.BadRequest(tfde.Message);
            }
        }

        /// <summary>
        /// Admin filters travels by their driver
        /// </summary>
        /// <param name="email"></param>
        /// <param name="driverId"></param>
        /// <returns>Collection of filtered travels</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("filter/driver/{driverId}")]
        public async Task<IActionResult> FilterByDriver([FromHeader] string email, int driverId)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Admin)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travels = await this.travelService.FilterByDriverAsync(driverId);
                return this.Ok(travels);
            }
            catch (TravelDoesNotExistException tdnee)
            {
                return this.BadRequest(tdnee.Message);
            }
        }

        /// <summary>
        /// Admin filters travels by their start address
        /// </summary>
        /// <param name="email"></param>
        /// <param name="startAddressId"></param>
        /// <returns>Collection of filtered travels</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("filter/startAddress/{startAddressId}")]
        public async Task<IActionResult> FilterByStartAddress([FromHeader] string email, int startAddressId)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Admin)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travels = await this.travelService.FilterByStartAddressAsync(startAddressId);
                return this.Ok(travels);
            }
            catch (TravelDoesNotExistException tdnee)
            {
                return this.BadRequest(tdnee.Message);
            }
        }

        /// <summary>
        /// Admin filters travels by their end address
        /// </summary>
        /// <param name="email"></param>
        /// <param name="endAddressId"></param>
        /// <returns>Collection of filtered travels</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("filter/endAddress/{endAddressId}")]
        public async Task<IActionResult> FilterByEndAddress([FromHeader] string email, int endAddressId)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Admin)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travels = await this.travelService.FilterByEndAddressAsync(endAddressId);
                return this.Ok(travels);
            }
            catch (TravelDoesNotExistException tdnee)
            {
                return this.BadRequest(tdnee.Message);
            }
        }

        /// <summary>
        /// Admin filters travels by their departure time
        /// </summary>
        /// <param name="email"></param>
        /// <param name="departureTime"></param>
        /// <returns>Collection of filtered travels</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("filter/departureTime/{departureTime}")]
        public async Task<IActionResult> FilterByDepartureTime([FromHeader] string email, string departureTime)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Admin)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travels = await this.travelService.FilterByDepartureTimeAsync(departureTime);
                return this.Ok(travels);
            }
            catch (TravelDoesNotExistException tdnee)
            {
                return this.BadRequest(tdnee.Message);
            }
        }

        /// <summary>
        /// Admin filters travels by their status. Statuses are as follow: 0 - Open; 1 - Travelling; 2 - Completed.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="travelStatusId"></param>
        /// <returns>Collection of filtered travels</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("filter/status/{travelStatusId}")]
        public async Task<IActionResult> FilterByStatus([FromHeader] string email, int travelStatusId)
        {
            User user = null;
            try
            {
                user = await this.LogInAsync(email);
                if (user.Role.Access != Access.Admin)
                {
                    return this.Permission();
                }
            }
            catch (EmailIncorrectException eie)
            {
                return this.Permission(eie.Message);
            }

            try
            {
                var travels = await this.travelService.FilterByTravelStatusAsync(travelStatusId);
                return this.Ok(travels);
            }
            catch (TravelDoesNotExistException tdnee)
            {
                return this.BadRequest(tdnee.Message);
            }
        }

        /// <summary>
        /// Admin sorts travels by a selected criterion
        /// </summary>
        /// <param name="email"></param>
        /// <param name="inputData"></param>
        /// <returns>Collection of sorted travels</returns>
        /// <Author>Viet Chan</Author>
        [HttpGet("filter/sortBy")]
        public async Task<IActionResult> SortBy([FromHeader] string email, [FromQuery] TravelSortByDto inputData)
        {
            User user = await this.LogInAsync(email, Access.Admin);

            var travels = await this.travelService.SortByAsync(inputData);
            return this.Ok(travels);
        }
    }
}