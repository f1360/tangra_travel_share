﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using TangraTravel.Data.Enum;
using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Exeception.User;

namespace TangraTravel.RestApi.Controllers.Abstract
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class AbstractController : ControllerBase
    {
        private readonly IUserAuthService service;

        protected AbstractController(IUserAuthService service)
        {
            this.service = service;
        }

        protected async Task<User> LogInAsync(string email, params Access[] access)
        {
            User user = await this.service.LogInAsync(email);

            if (!access.Contains(user.Role.Access))
            {
                throw new PermissionException("You do not have a permission!");
            }

            return user;
        }

        protected async Task<User> LogInAsync(string email, string password = null)
        {
            return await this.service.LogInAsync(email, password);
        }

        protected IActionResult Permission(string message = null)
        {
            return this.Unauthorized(message == null ? "Your do not have permissions" : message);
        }
    }
}