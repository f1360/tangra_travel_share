using System;
using System.IO;
using System.Reflection;

using CloudinaryDotNet;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using TangraTravel.Data.DataBase;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.RestApi.Extention;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.Cloud;
using TangraTravel.Service.Models.Provider;

namespace TangraTravel.RestApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TangraTravelContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddControllers();
            services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null
            //Link: https://docs.microsoft.com/en-us/aspnet/core/web-api/advanced/formatting?view=aspnetcore-5.0
            );
            services.AddControllers().AddNewtonsoftJson(option =>
            {
                option.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

                option.SerializerSettings.Converters.Add(new StringEnumConverter
                {
                    CamelCaseText = false
                });

                //Link: https://www.codeproject.com/Articles/5300099/Description-of-the-//Enumeration-Members-in-Swashbuc
            });

            Cloudinary cloudinary = new Cloudinary(
                new Account(
                    this.Configuration.GetSection("Cloudinary").GetValue<string>("cloudName"),
                    this.Configuration.GetSection("Cloudinary").GetValue<string>("apyKey"),
                    this.Configuration.GetSection("Cloudinary").GetValue<string>("apiSecret")
                    ));            
            services.AddSingleton(cloudinary);
            services.AddSingleton<ICloud, Cloud>();
            services.AddScoped<IProvider, CustomProvider>();
            services.AddSingleton<ICustomExpression, CustomExpression>();
            services.AddScoped<IUserAuthService, UserAuthService>();
            services.AddScoped<ICarService, CarService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IAvatarService, AvatarService>();
            services.AddScoped<ITravelService, TravelService>();
            services.AddScoped<IFeedbackService, FeedbackService>();
            services.AddScoped<IEmailService, EmailService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TangraTravel.RestApi", Version = "v1" });
                var fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";

                var filePath = Path.Combine(AppContext.BaseDirectory, fileName);

                c.IncludeXmlComments(filePath);
            });
            services.AddSwaggerGenNewtonsoftSupport();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TangraTravel.RestApi v1"));
            }

            app.UseExceptionHandler(options =>
            {
                options.Run(context => context.ExceptionHandler());
            });

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}