﻿using System;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Diagnostics;

using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Exeception.Feedback;

namespace TangraTravel.RestApi.Extention
{
    public static class CustomeWebExtention
    {
        public static async Task ExceptionHandler(this HttpContext context)
        {
            var exceptionContext = context.Features.Get<IExceptionHandlerFeature>();
            var exception = exceptionContext.Error.GetType().Name;
            if (exception.Equals(nameof(EmailIncorrectException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
            else if (exception.Equals(nameof(PermissionException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
            else if (exception.Equals(nameof(UserNotExistException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else if (exception.Equals(nameof(InputExistException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            else if (exception.Equals(nameof(DriverException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            else if (exception.Equals(nameof(PassengerException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            else if (exception.Equals(nameof(CarException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else if (exception.Equals(nameof(TravelDoesNotExistException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else if (exception.Equals(nameof(TravelDoesNotCompleteException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            else if (exception.Equals(nameof(FeedbackException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            else if (exception.Equals(nameof(InvalidOperationException)))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            if (exceptionContext != null)
            {
                var error = exceptionContext.Error.Message;
                await context.Response.WriteAsync(error);
            }
        }
    }
}