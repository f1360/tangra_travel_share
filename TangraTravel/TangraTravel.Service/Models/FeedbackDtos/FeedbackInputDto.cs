﻿using TangraTravel.Service.Enum;

namespace TangraTravel.Service.Models.FeedbackDtos
{
    public class FeedbackInputDto
    {
        public TypeFeedback TypeFeedback { get; set; }

        public int Page { get; set; }

        public int? Perpage { get; set; }
    }
}