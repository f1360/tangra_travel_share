﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TangraTravel.Service.Models.FeedbackDtos
{
    public class FeedbackCreateDto
    {
        [Range(0, 5, ErrorMessage = "Rating value can be between 0 to 1")]
        public int Rating { get; set; }

        [MaxLength(255, ErrorMessage = "{0} value must be between and {1} symbols!")]
        public string Comment { get; set; }

        public int TargetId { get; set; }

        public int TravelId { get; set; }
    }
}