﻿using Newtonsoft.Json;

namespace TangraTravel.Service.Models.FeedbackDtos
{
    public class FeedbackReturnDto
    {
        [JsonIgnore]
        public int FeedbackId { get; set; }

        public string Author { get; set; }

        public string AuthorUsername { get; set; }

        public string Target { get; set; }

        public string TargetUsername { get; set; }

        public int Rating { get; set; }

        public string Comment { get; set; }
    }
}