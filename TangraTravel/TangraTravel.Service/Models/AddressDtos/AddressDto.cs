﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace TangraTravel.Service.Models.AddressDtos
{
    public class AddressDto
    {
        public SelectList Addresses { get; set; }

        public SelectList Cities { get; set; }

        public SelectList Countires { get; set; }
    }
}