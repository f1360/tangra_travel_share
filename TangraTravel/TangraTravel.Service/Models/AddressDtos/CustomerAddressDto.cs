﻿using Newtonsoft.Json;

namespace TangraTravel.Service.Models.AddressDtos
{
    public class CustomerAddressDto
    {
        [JsonIgnore]
        public int AddressId { get; set; }
        public string StreetAddress { get; set; }

        [JsonIgnore]
        public int CityId { get; set; }
        public string City { get; set; }

        [JsonIgnore]
        public int CountryId { get; set; }
        public string Country { get; set; }
    }
}