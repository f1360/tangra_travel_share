﻿using System.Collections.Generic;

namespace TangraTravel.Service.Models.Email
{
    public record Email
    {
        /// <summary>
        /// E-Mail recipient
        /// </summary>
        public List<string> To { get; set; }
        /// <summary>
        /// Subject
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// Body, could be HTML-Code
        /// </summary>
        public string Body { get; set; }
    }
}