﻿using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace TangraTravel.Service.Models.CarDtos
{
    public class CarUpdateDto
    {
        [Required, StringLength(10, MinimumLength = 4, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        [JsonProperty(PropertyName = "registration number")]
        public string RegistrationNumber { get; set; }
    }
}