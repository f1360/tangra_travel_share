﻿namespace TangraTravel.Service.Models.CarDtos
{
    public class CarTravelDto
    {
        public string Brand { get; set; }

        public string Model { get; set; }

        public string RegistrationNumber { get; set; }

        public int Seats { get; set; }
    }
}