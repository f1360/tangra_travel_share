﻿using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace TangraTravel.Service.Models.CarDtos
{
    public class CarCreateDto
    {
        public int ModelId { get; set; }

        [Required, StringLength(10, MinimumLength = 4, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        [JsonProperty(PropertyName = "registration number")]
        public string RegistrationNumber { get; set; }

        [Range(1, 8, ErrorMessage = "{0} must be in range between {1} to {2}!")]
        public int Seats { get; set; }
    }
}