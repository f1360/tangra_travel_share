﻿using System.ComponentModel;

using Newtonsoft.Json;

using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Models.CarDtos
{
    public class CarReturnDto
    {
        [JsonIgnore]
        public int CustomerId { get; set; }

        [JsonIgnore]
        public int CarId { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        [DisplayName("Registration Number")]
        [JsonProperty(PropertyName = "registration number")]
        public string RegistrationNumber { get; set; }

        public int Seats { get; set; }

        public string Owner { get; set; }

        public CarAvatarDto Avatar { get; set; }
    }
}