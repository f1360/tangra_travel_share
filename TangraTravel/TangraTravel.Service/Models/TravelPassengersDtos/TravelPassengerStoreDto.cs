﻿namespace TangraTravel.Service.Models.TravelPassengersDtos
{
    public class TravelPassengerStoreDto
    {
        public int PassengerId { get; set; }

        public string Passenger { get; set; }
    }
}