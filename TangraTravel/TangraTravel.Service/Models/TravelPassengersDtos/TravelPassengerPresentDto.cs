﻿using Newtonsoft.Json;

using TangraTravel.Data.Enum;

namespace TangraTravel.Service.Models.TravelPassengersDtos
{
    public class TravelPassengerPresentDto
    {
        [JsonIgnore]
        public int TravelPassengerId { get; set; }

        public string PassengerName { get; set; }

        public int TravelId { get; set; }

        public ApplyStatus ApplyStatus { get; set; }
    }
}