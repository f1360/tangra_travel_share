﻿using TangraTravel.Data.Enum;

namespace TangraTravel.Service.Models.TravelPassengersDtos
{
    public class TravelPassengerApplyStatusDto
    {
        public ApplyStatus Criteria { get; set; }
    }
}