﻿using System.Threading.Tasks;

using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace TangraTravel.Service.Models.Cloud
{
    public class Cloud : ICloud
    {
        private readonly Cloudinary cloud;
        private string uri;
        private string publicId;

        public Cloud(Cloudinary cloud)
        {
            this.cloud = cloud;
        }

        public string Uri => this.uri;

        public string PublicId => this.publicId;

        public async Task UploadAsync(ImageUploadParams image)
        {
            var upload = await this.cloud.UploadAsync(image);
            this.uri = upload?.SecureUri.AbsoluteUri;
            this.publicId = upload?.PublicId;
        }

        public async Task DeleteAsync(string publicId)
        {
            await this.cloud.DeleteResourcesAsync(publicId);
        }
    }
}