﻿using System.Threading.Tasks;

using CloudinaryDotNet.Actions;

namespace TangraTravel.Service.Models.Cloud
{
    public interface ICloud
    {
        string Uri { get; }

        string PublicId { get; }

        Task UploadAsync(ImageUploadParams image);

        Task DeleteAsync(string publicId);
    }
}