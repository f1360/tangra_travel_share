﻿using System.Collections.Generic;

namespace TangraTravel.Service.Models.CustomerDtos
{
    public class CustomerSelectionIdsDto : CustomerReturnDto
    {
        public ICollection<int> CarIds { get; set; }

        public IList<int> TravelIds { get; set; }

        public IList<int> FeedbacksGivenId { get; set; }

        public IList<int> FeedbacksReceivedId { get; set; }
    }
}