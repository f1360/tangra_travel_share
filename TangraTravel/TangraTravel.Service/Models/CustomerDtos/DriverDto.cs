﻿using TangraTravel.Service.Models.CarDtos;

namespace TangraTravel.Service.Models.CustomerDtos
{
    public class DriverDto
    {
        public string DriverName { get; set; }

        public CarTravelDto CarDetails { get; set; }
    }
}