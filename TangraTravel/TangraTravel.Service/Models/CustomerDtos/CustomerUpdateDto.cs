﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using Microsoft.AspNetCore.Http;

using TangraTravel.Data.Enum;

namespace TangraTravel.Service.Models.CustomerDtos
{
    public class CustomerUpdateDto
    {
        [StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Username { get; set; }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string FirstName { get; set; }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string LastName { get; set; }

        [DisplayName("New Email")]
        [MaxLength(100, ErrorMessage = "Email length must be {1}!"), EmailAddress(ErrorMessage = "Invalid value for {0}")]
        public string NewEmail { get; set; }

        [StringLength(8, ErrorMessage = "Password length must be {1} characters!")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Password is invalid!")]
        public string Password { get; set; }

        public Gender? Gender { get; set; }

        public int? AddressId { get; set; }

        [StringLength(10, ErrorMessage = "Phone number is invalid!")]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Phone number is invalid!")]
        public string PhoneNumber { get; set; }

        public IFormFile Avatar { get; set; }
    }
}