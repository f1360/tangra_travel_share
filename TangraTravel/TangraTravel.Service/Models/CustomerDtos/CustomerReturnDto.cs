﻿using System.ComponentModel;

using Newtonsoft.Json;

using TangraTravel.Data.Enum;
using TangraTravel.Service.Models.AvatarDtos;
using TangraTravel.Service.Models.AddressDtos;

namespace TangraTravel.Service.Models.CustomerDtos
{
    public class CustomerReturnDto
    {
        [JsonIgnore]
        public int CustomerId { get; set; }

        [DisplayName("Full Name")]
        [JsonProperty(PropertyName = "full name")]
        public string FullName => $"{this.FirstName} {this.LastName}";

        [JsonIgnore]
        public string FirstName { get; set; }

        [JsonIgnore]
        public string LastName { get; set; }

        public string Email { get; set; }

        public Gender Gender { get; set; }

        public string Username { get; set; }

        public string PhoneNumber { get; set; }

        [DisplayName("status in the system")]
        [JsonProperty(PropertyName = "status in the system")]
        public string SystemStatus { get; set; }

        [DisplayName("Status profile")]
        [JsonProperty(PropertyName = "status profile")]
        public string StatusProfile { get; set; }

        public double Rating { get; set; }

        public CustomerAddressDto Address { get; set; }
                
        public AvatarDto Avatar { get; set; }
    }
}