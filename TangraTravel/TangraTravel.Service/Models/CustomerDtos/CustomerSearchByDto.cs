﻿using System.ComponentModel.DataAnnotations;

using TangraTravel.Service.Enum;

namespace TangraTravel.Service.Models.CustomerDtos
{
    public class CustomerSearchByDto
    {
        public SearchBy Criteria { get; set; }

        [Required]
        public string Value { get; set; }
    }
}