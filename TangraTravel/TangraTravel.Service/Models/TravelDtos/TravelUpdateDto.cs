﻿namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelUpdateDto
    {
        public int? DriverId { get; set; }

        public int? StartAddressId { get; set; }

        public int? EndAddressId { get; set; }

        public string DepartureTime { get; set; }

        public int? TravelStatus { get; set; }

    }
}