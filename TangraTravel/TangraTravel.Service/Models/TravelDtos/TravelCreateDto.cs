﻿using System.ComponentModel.DataAnnotations;

namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelCreateDto
    {
        [Required]
        public int DriverId { get; set; }

        [Required]
        public int StartAddressId { get; set; }

        [Required]
        public int EndAddressId { get; set; }

        [Required]
        public string DepartureTime { get; set; }

        public int FreeSeats { get; set; }

        public string Comment { get; set; }
    }
}