﻿namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelCustomer
    {
        public int TargetId { get; set; }

        public string FullName { get; set; }

        public string Username { get; set; }

        public string TargetInformation => $"Name: {this.FullName}, Username: {this.Username}";
    }
}