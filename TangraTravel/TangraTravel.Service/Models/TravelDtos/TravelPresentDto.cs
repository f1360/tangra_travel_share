﻿using Newtonsoft.Json;

using TangraTravel.Service.Models.AddressDtos;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Models.TravelCommentDtos;

namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelPresentDto
    {
        [JsonIgnore]
        public int TravelId { get; set; }

        public DriverDto Driver { get; set; }

        public CustomerAddressDto StartAddress { get; set; }

        public CustomerAddressDto EndAddress { get; set; }

        public string DepartureTime { get; set; }

        public string Status { get; set; }

        public int FreeSeats { get; set; }

        public TravelCommentPresentDto Comment { get; set; }
    }
}