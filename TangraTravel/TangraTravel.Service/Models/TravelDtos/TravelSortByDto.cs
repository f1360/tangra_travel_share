﻿using TangraTravel.Service.Enum;

namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelSortByDto
    {
        public SortBy Criteria { get; set; }
    }
}