﻿namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelFeedbackDto
    {
        public int TravelId { get; set; }

        public string StartPoint { get; set; }

        public string EndPoint { get; set; }

        public string ArrivalDate { get; set; }

        public string Role { get; set; }

        public int PassengerCounts { get; set; }
    }
}