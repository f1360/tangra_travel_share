﻿namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelUserDto
    {
        public int TravelId { get; set; }

        public int UserId { get; set; }

        public string Role { get; set; }
    }
}