﻿using System.Collections.Generic;

using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelStoreDto
    {
        public int TravelId { get; set; }

        public string DepartureDate { get; set; }

        public string StartPoint { get; set; }

        public string EndPoint { get; set; }

        public int DriverId { get; set; }
        public string Driver { get; set; }

        public string StatusTravel { get; set; }

        public ICollection<TravelPassengerStoreDto> TravelPassengers { get; set; }
    }
}