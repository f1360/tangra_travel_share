﻿namespace TangraTravel.Service.Models.TravelDtos
{
    public class TravelPassengerDetailsDto : TravelPresentDto
    {
        public string PassengerStatus { get; set; }
    }
}