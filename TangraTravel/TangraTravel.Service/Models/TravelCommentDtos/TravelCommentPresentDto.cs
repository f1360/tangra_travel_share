﻿using Newtonsoft.Json;

namespace TangraTravel.Service.Models.TravelCommentDtos
{
    public class TravelCommentPresentDto
    {
        [JsonIgnore]
        public int CommentId { get; set; }

        public string Comment { get; set; }
    }
}