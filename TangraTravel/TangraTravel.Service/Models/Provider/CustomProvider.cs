﻿using System.IO;
using System.Linq;

using Microsoft.Extensions.FileProviders;

using TangraTravel.Data.Enum;

namespace TangraTravel.Service.Models.Provider
{
    public class CustomProvider : IProvider
    {
        public string UserAvatarName(string webRootPath, string folder, Gender gender)
        {
            var provider = new PhysicalFileProvider(webRootPath);
            var content = provider.GetDirectoryContents(Path.Combine(folder));
            var objFile = content.FirstOrDefault(x => x.Name.Contains(gender.ToString()));

            return objFile.Name;
        }

        public string CarAvatarName(string webRootPath, string folder, string brand)
        {
            var provider = new PhysicalFileProvider(webRootPath);
            var content = provider.GetDirectoryContents(Path.Combine(folder));
            var objFile = content.FirstOrDefault(x => x.Name.Contains(brand.ToLower()));

            return objFile.Name;
        }
    }
}