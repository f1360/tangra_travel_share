﻿using TangraTravel.Data.Enum;

namespace TangraTravel.Service.Models.Provider
{
    public interface IProvider
    {
        string UserAvatarName(string webRootPath, string folder, Gender gender);

        string CarAvatarName(string webRootPath, string folder, string brand);
    }
}