﻿using Newtonsoft.Json;

namespace TangraTravel.Service.Models.AvatarDtos
{
    public class AvatarDto
    {
        [JsonIgnore]
        public int? AvatarId { get; set; }

        public string ImageDataURL { get; set; }

        public string Alt { get; set; }
    }
}