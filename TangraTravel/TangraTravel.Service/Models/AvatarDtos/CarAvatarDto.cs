﻿namespace TangraTravel.Service.Models.AvatarDtos
{
    public class CarAvatarDto
    {
        public string ImageUrl { get; set; }

        public string Alt { get; set; }
    }
}