﻿using System;
using System.Linq;

namespace TangraTravel.Service.Extension
{
    public static class PageExtension
    {
        private const int defaultPerpage = 5;

        /// <summary>
        /// Calculate how entities must be included in one page and where is maxPage
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="page"></param>
        /// <param name="perpage"></param>
        /// <returns>IQueryable about type of given collection</returns>
        public static IQueryable<T> Paginate<T>(this IQueryable<T> collection, int page, int perpage = defaultPerpage)
        {
            int count = collection.Count();

            int maxPage = (int)Math.Ceiling(count * 1.0 / perpage);

            if (page > maxPage && maxPage != 0)
            {
                page = maxPage;
            }
            else if (page <= 0)
            {
                page = 1;
            }

            return collection.Skip((page - 1) * perpage).Take(perpage);
        }
    }
}