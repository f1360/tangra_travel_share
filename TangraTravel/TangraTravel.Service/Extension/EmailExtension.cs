﻿using System.Net.Mail;

using TangraTravel.Service.Models.Email;

namespace TangraTravel.Service.Extension
{
    internal static class EmailExtension
    {
        internal static MailMessage ToMailMessage(this Email email, EmailServiceOptions options)
        {
            var mail = new MailMessage
            {
                From = new MailAddress(options.Email, options.DisplayName),
                Body = email.Body,
                Subject = email.Subject
            };

            email.To?.ForEach(to => mail.To.Add(to));

            return mail;
        }
    }
}