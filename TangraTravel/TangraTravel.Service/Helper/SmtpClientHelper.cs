﻿using System.Net;
using System.Net.Mail;

using TangraTravel.Service.Models.Email;

namespace TangraTravel.Service.Helper
{
    internal static class SmtpClientHelper
    {
        internal static SmtpClient CreateSmtpClient(EmailServiceOptions options) => new(options.Host, options.Port)
        {
            Credentials = new NetworkCredential(options.Account, options.Password),
            EnableSsl = options.Security
        };
    }
}