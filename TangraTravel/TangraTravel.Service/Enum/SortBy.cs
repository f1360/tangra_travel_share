﻿namespace TangraTravel.Service.Enum
{
    public enum SortBy
    {
        Driver,
        StartAddress,
        EndAddress,
        DepartureTime,
        TravelStatus
    }
}