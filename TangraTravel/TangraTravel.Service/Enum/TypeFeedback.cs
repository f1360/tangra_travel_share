﻿namespace TangraTravel.Service.Enum
{
    public enum TypeFeedback
    {
        Given,
        Received
    }
}