﻿namespace TangraTravel.Service.Enum
{
    public enum SearchBy
    {
        Username,
        Email,
        Phone
    }
}