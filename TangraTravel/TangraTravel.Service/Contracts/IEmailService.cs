﻿using System;
using System.Threading;
using System.Threading.Tasks;

using TangraTravel.Service.Models.Email;

namespace TangraTravel.Service.Contracts
{
    public interface IEmailService : IDisposable
    {
        Task SendEmailAsync(Email email, CancellationToken cancellationToken = default);
    }
}