﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Contracts
{
    public interface IAvatarService
    {
        Task CreateAvatarAsync(User user, IFormFile file);

        Task UpdateAvatarAsync(User user, IFormFile file);

        Task<AvatarDto> GetAvatarAsync(int userId, bool isAdmin = false);

        Task DeleteAvatarAsync(User user);

        CarAvatarDto GetCarAvatar(string brand);
    }
}