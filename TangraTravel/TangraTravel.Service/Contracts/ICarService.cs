﻿using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc.Rendering;

using TangraTravel.Service.Models.CarDtos;

namespace TangraTravel.Service.Contracts
{
    public interface ICarService
    {
        Task<CarReturnDto> GetByUerIdAsync(int customerId);

        Task<CarReturnDto> GetByCarIdAsync(int carId);
                
        Task<ICollection<CarReturnDto>> GetByPageAsync(int page, int? perPage);

        Task<CarReturnDto> CreateAsync(int customerId, CarCreateDto input);

        Task<CarReturnDto> UpdateAsync(int customerId, CarUpdateDto input);

        Task DeleteAsync(int customerId);

        Task<SelectList> TakeBrandsAndModelsAsync();

        Task<int> MaxPage(int byPage);
    }
}