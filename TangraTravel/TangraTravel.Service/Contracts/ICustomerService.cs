﻿using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc.Rendering;

using TangraTravel.Data.Enum;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.Service.Contracts
{
    public interface ICustomerService
    {
        Task<CustomerReturnDto> GetCustomerAsync(int customerId);

        Task<ICollection<CustomerReturnDto>> GetCustomersByPageAsync(int page, int? perPage = null);

        Task<CustomerReturnDto> SearchAsync(CustomerSearchByDto input);

        Task<CustomerReturnDto> CreateAsync(CustomerCreateDto input);

        Task<CustomerReturnDto> UpdateAsync(int customerId, CustomerUpdateDto input);

        Task DeleteCustomerAsync(int customerId);

        Task ChangeRoleAsync(int userId, Access role);

        Task BlockAsync(int customerId);

        Task UnblockAsync(int customerId);

        Task<TravelPassengerPresentDto> ApplyForTravelAsync(int passengerId, int travelId, Access role);

        Task<ICollection<TravelPassengerPresentDto>> ListApplicationsAsync(int travelId);

        Task<TravelPassengerPresentDto> GetApplicationAsync(int travelPassId);

        Task ProcessApplicationAsync(int travelPassengerId, TravelPassengerApplyStatusDto inputData);

        Task CancelTravel(int travelId, int customerId);

        Task<int> MaxPage(int byPage);

        Task<ICollection<CustomerReturnDto>> GetTopTenDriversAsync();

        Task<ICollection<CustomerReturnDto>> GetTopTenPassengersAsync();

        Task<int> GetCarSeats(int userId);

        Task ConfirmUser(string confirmationString);

        Task<int> CountAllUsers();

        Task<SelectList> PrepareDriversInformationAsync();
    }
}