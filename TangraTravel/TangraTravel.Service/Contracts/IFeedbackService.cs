﻿using System.Threading.Tasks;
using System.Collections.Generic;

using TangraTravel.Service.Models.FeedbackDtos;

namespace TangraTravel.Service.Contracts
{
    public interface IFeedbackService
    {
        Task<FeedbackReturnDto> DriverWriteFeedbackAsync(int authorId, FeedbackCreateDto input);

        Task<FeedbackReturnDto> PassengerWriteFeedbackAsync(int authorId, FeedbackCreateDto input);

        Task<ICollection<FeedbackReturnDto>> GivenFeedback(int customerId, int page, int? byPage);

        Task<ICollection<FeedbackReturnDto>> ReceivedFeedback(int customerId, int page, int? byPage);

        Task<int> MaxPageAsync(int customerId, int byPage, bool IsGiven);

        Task<ICollection<FeedbackReturnDto>> GivenSortedFeedbackAsync(int userId, int take, bool best);

        Task<FeedbackReturnDto> GetFeedbackAsync(int feedbackId);
    }
}