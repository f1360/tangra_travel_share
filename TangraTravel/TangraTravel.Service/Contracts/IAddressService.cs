﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace TangraTravel.Service.Contracts
{
    public interface IAddressService
    {
        public Task<SelectList> PrepareAddressInformationAsync();
    }
}