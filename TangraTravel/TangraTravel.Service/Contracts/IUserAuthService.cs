﻿using System.Threading.Tasks;

using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.Service.Contracts
{
    public interface IUserAuthService
    {
        Task<User> LogInAsync(string email, string password = null);
    }
}