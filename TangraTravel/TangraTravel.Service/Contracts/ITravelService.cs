﻿using System.Threading.Tasks;
using System.Collections.Generic;

using TangraTravel.Service.Models.TravelDtos;

namespace TangraTravel.Service.Contracts
{
    public interface ITravelService
    {
        Task<TravelPresentDto> GetTravelAsync(int travelId);

        Task<ICollection<TravelPresentDto>> GetAllTravelsAsync();

        Task<IEnumerable<TravelPresentDto>> GetAllAdminTravelsAsync(int page, int perPage);

        Task<IEnumerable<TravelPresentDto>> GetAllTravelsFilteredByStartPoint(int page, int perPage, int addressId);

        Task<IEnumerable<TravelPresentDto>> GetAllTravelsFilteredByEndPoint(int page, int perPage, int addressId);

        Task<IEnumerable<TravelPresentDto>> GetAllTravelsFilteredByDriver(int page, int perPage, int driverId);

        Task<IEnumerable<TravelPresentDto>> GetAllTravelsSorted(int page, int perPage, string criterion);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravels(int customerId, int page, int perPage);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsFilteredByStartPoint(int customerId, int page, int perPage, int addressId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsFilteredByEndPoint(int customerId, int page, int perPage, int addressId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsFilteredByDriver(int customerId, int page, int perPage, int driverId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsSorted(int customerId, int page, int perPage, string criterion);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsFilteredByStartPoint(int customerId, int page, int perPage, int addressId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsFilteredByEndPoint(int customerId, int page, int perPage, int addressId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsFilteredByDriver(int customerId, int page, int perPage, int driverId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsSorted(int customerId, int page, int perPage, string criterion);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllDriverTravelsFilteredByStartPoint(int customerId, int page, int perPage, int addressId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllDriverTravelsFilteredByEndPoint(int customerId, int page, int perPage, int addressId);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllDriverTravelsSorted(int customerId, int page, int perPage, string criterion);

        Task<IEnumerable<TravelPresentDto>> GetAllDriverTravels(int customerId, int page, int perPage);

        Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravels(int customerId, int page, int perPage);

        Task<int> MaxPageForAllTravels();

        Task<int> MaxPageForAvailableTravels(int customerId);

        Task<int> MaxPageForDriverTravels(int customerId);

        Task<int> MaxPageForPassengerTravels(int customerId);

        Task<TravelPresentDto> CreateTravelAsync(TravelCreateDto inputDto);

        Task<TravelPresentDto> UpdateTravelAsync(int travelId, TravelUpdateDto inputDto);

        Task DeleteTravelAsync(int travelId);

        Task ChangeTravelStatusAsync(int travelId, string status);

        Task<IEnumerable<TravelPresentDto>> FilterByDriverAsync(int driverId);

        Task<IEnumerable<TravelPresentDto>> FilterByStartAddressAsync(int startAddressId);

        Task<IEnumerable<TravelPresentDto>> FilterByEndAddressAsync(int endAddressId);

        Task<IEnumerable<TravelPresentDto>> FilterByDepartureTimeAsync(string departureTime);

        Task<IEnumerable<TravelPresentDto>> FilterByTravelStatusAsync(int travelStatusId);

        Task<IEnumerable<TravelPresentDto>> SortByAsync(TravelSortByDto inputData);

        Task<TravelCustomer> RetriveDriverInFinishedTravelAsync(int travelId, int userId);

        Task<IList<TravelFeedbackDto>> RetriveCompletedTravelsAsync(int userId, int page);

        Task<IList<TravelCustomer>> RetrivePassengerInFinishedTravelsAsync(int travelId);

        Task<ICollection<TravelStoreDto>> GetSoonTravelsAsync(int userId, int take);

        Task<int> CountAllTravels();
    }
}