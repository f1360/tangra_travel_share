﻿using System.IO;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Models.Cloud;
using TangraTravel.Service.Models.Provider;
using TangraTravel.Service.Service.Abstract;
using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.Service.Service
{
    public class AvatarService : AbstractService, IAvatarService
    {
        private readonly IWebHostEnvironment webHost;
        private readonly ICloud cloud;
        private readonly IProvider provider;
        private const string customerAvatar = "avatar";
        private const string carAvatar = "brands";

        public AvatarService(TangraTravelContext db, IWebHostEnvironment webHost, ICloud cloud, IProvider provider)
            : base(db)
        {
            this.webHost = webHost;
            this.cloud = cloud;
            this.provider = provider;
        }

        public async Task CreateAvatarAsync(User user, IFormFile file)
        {
            this.UserExist(user);

            byte[] picture = null;
            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms);
                picture = ms.ToArray();
            }

            string fileName = $"{user.Email}";
            using (var ms = new MemoryStream(picture))
            {
                ImageUploadParams uploadParams = new ImageUploadParams
                {
                    Folder = "Tangra_Travel",
                    File = new FileDescription(fileName, ms)
                };

                await this.cloud.UploadAsync(uploadParams);
            }

            string uri = this.cloud.Uri;
            string publicId = this.cloud.PublicId;
            var avatar = new Avatar()
            {
                FileName = fileName,
                PictureUri = uri,
                PublicId = publicId,
                Alternative = $"{user.FirstName} {user.LastName}"
            };
            user.Avatar = avatar;
        }

        public async Task<AvatarDto> GetAvatarAsync(int userId, bool isAdmin = false)
        {
            User user = null;
            if (isAdmin)
            {
                user = await this.db.Admins.FirstOrDefaultAsync(a => a.Id == userId);
            }
            else
            {
                user = await this.db.Customers.FirstOrDefaultAsync(a => a.Id == userId);
            }
            this.UserExist(user);

            string imageDataURL = string.Empty;
            string alt = string.Empty;
            if (user.AvatarId.HasValue)
            {
                var avatar = await this.db.Avatars.FirstOrDefaultAsync(x => x.Id == user.AvatarId);
                imageDataURL = avatar.PictureUri;
                alt = avatar.Alternative;
            }
            else
            {
                imageDataURL = @"/avatar/" + this.provider.UserAvatarName(this.webHost.WebRootPath, folder: customerAvatar, user.Gender);
                alt = $"{user.FirstName} {user.LastName}";
            }

            var avatarRetrun = new AvatarDto
            {
                AvatarId = user.AvatarId != null ? user.AvatarId.Value : null,
                ImageDataURL = imageDataURL,
                Alt = alt
            };

            return avatarRetrun;
        }

        public async Task UpdateAvatarAsync(User user, IFormFile file)
        {
            this.UserExist(user);
            if (user.AvatarId.HasValue)
            {
                var avatar = await this.db.Avatars.FirstOrDefaultAsync(x => x.Id == user.AvatarId);

                byte[] picture = null;
                using (var ms = new MemoryStream())
                {
                    await file.CopyToAsync(ms);
                    picture = ms.ToArray();
                }

                string fileName = $"{user.Email}";
                using (var ms = new MemoryStream(picture))
                {
                    ImageUploadParams uploadParams = new ImageUploadParams
                    {
                        Folder = "Tangra_Travel",
                        File = new FileDescription(fileName, ms)
                    };
                    await this.cloud.UploadAsync(uploadParams);
                    await this.cloud.DeleteAsync(avatar.PublicId);
                }

                avatar.FileName = fileName;
                avatar.Alternative = $"{user.FirstName} {user.LastName}";
                avatar.PictureUri = this.cloud.Uri;
                avatar.PublicId = this.cloud.PublicId;
            }
            else
            {
                await this.CreateAvatarAsync(user, file);
            }
        }

        public async Task DeleteAvatarAsync(User user)
        {
            this.UserExist(user);
            if (user.AvatarId.HasValue)
            {
                var avatar = await this.db.Avatars.FirstOrDefaultAsync(a => a.Id == user.AvatarId);
                await this.cloud.DeleteAsync(avatar.PublicId);
                this.db.Avatars.Remove(avatar);
            }
        }

        public CarAvatarDto GetCarAvatar(string brand)
        {
            var avatar = new CarAvatarDto
            {
                ImageUrl = @"/brands/" + this.provider.CarAvatarName(this.webHost.WebRootPath, folder: carAvatar, brand),
                Alt = brand
            };

            return avatar;
        }
    }
}