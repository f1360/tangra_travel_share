﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Extension;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Service.Abstract;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Exeception.Feedback;
using TangraTravel.Service.Models.FeedbackDtos;

namespace TangraTravel.Service.Service
{
    public class FeedbackService : AbstractService, IFeedbackService
    {
        private readonly ICustomExpression func;

        public FeedbackService(TangraTravelContext db, ICustomExpression func)
            : base(db)
        {
            this.func = func;
        }

        public async Task<FeedbackReturnDto> GetFeedbackAsync(int feedbackId)
        {
            var result = await this.db.Feedbacks
                                   .Select(this.func.FeedbackToFeedbackReturnDto())
                                   .FirstOrDefaultAsync(x => x.FeedbackId == feedbackId);

            return result;
        }

        public async Task<ICollection<FeedbackReturnDto>> GivenFeedback(int customerId, int page, int? byPage)
        {
            var feedbacks = this.db.Feedbacks
                                   .Where(x => x.AuthorId == customerId)
                                   .Select(this.func.FeedbackToFeedbackReturnDto());
            if (byPage.HasValue)
            {
                feedbacks = feedbacks.Paginate(page, byPage.Value);
            }
            else
            {
                feedbacks = feedbacks.Paginate(page);
            }

            return await feedbacks.ToArrayAsync();
        }

        public async Task<ICollection<FeedbackReturnDto>> ReceivedFeedback(int customerId, int page, int? byPage)
        {
            var feedbacks = this.db.Feedbacks
                                   .Where(x => x.TargetId == customerId)
                                   .Select(this.func.FeedbackToFeedbackReturnDto());

            if (byPage.HasValue)
            {
                feedbacks = feedbacks.Paginate(page, byPage.Value);
            }
            else
            {
                feedbacks = feedbacks.Paginate(page);
            }

            return await feedbacks.ToArrayAsync();
        }

        public async Task<ICollection<FeedbackReturnDto>> GivenSortedFeedbackAsync(int userId, int take, bool best)
        {
            var feedback = this.db.Feedbacks
                               .Where(f => f.TargetId == userId);
            if (best)
            {
                feedback = feedback.Where(x => x.Rating > 3).OrderByDescending(f => f.Rating);
            }
            else
            {
                feedback = feedback.Where(x => x.Rating <= 3).OrderBy(f => f.Rating);
            }

            var result = await feedback.Select(this.func.FeedbackToFeedbackReturnDto()).Take(take).ToListAsync();

            return result;
        }

        public async Task<FeedbackReturnDto> DriverWriteFeedbackAsync(int authorId, FeedbackCreateDto input)
        {
            var author = await this.db.Customers
                                      .FirstOrDefaultAsync(c => c.Id == authorId);
            this.UserExist(author);


            int targetId = input.TargetId;
            var target = await this.db.Customers
                                      .Include(x => x.FeedbacksReceived)
                                      .FirstOrDefaultAsync(c => c.Id == targetId);
            this.UserExist(target);

            var travel = await this.db.Travels
                                      .Include(t => t.TravelPassengers)
                                      .FirstOrDefaultAsync(t => t.Id == input.TravelId);
            this.TravelExists(travel);
            if (travel.TravelStatus != TravelStatus.Completed)
            {
                throw new TravelDoesNotCompleteException("The Travel must be completed!");
            }

            if (travel.DriverId != authorId)
            {
                throw new UserNotExistException("You did not attend to this travel!");
            }

            if (!travel.TravelPassengers.Any(tp => tp.PassengerId == targetId))
            {
                throw new UserNotExistException("You want to leave a feedback for someone who did not take part in the trip!");
            }

            if (travel.TravelPassengers.Any(tp => tp.PassengerId == targetId && tp.ApplyStatus != ApplyStatus.Accepted))
             {
                throw new UserNotExistException("You want to leave a feedback for someone who did not take part in the trip!");
            }

            if (await this.db.Feedbacks.AnyAsync(f => f.TargetId == targetId && f.TravelId == input.TravelId))
            {
                throw new FeedbackException($"You already wrote a feedback to {target.Username}");
            }

            var feedback = new Feedback
            {
                Rating = input.Rating,
                TravelId = input.TravelId,
                AuthorId = authorId,
                TargetId = targetId
            };
            await this.db.Feedbacks.AddAsync(feedback);

            if (input.Comment != null)
            {
                var comment = new FeedbackComment
                {
                    Feedback = feedback,
                    Comment = input.Comment
                };
                await this.db.FeedbackComments.AddAsync(comment);
            }

            int countsFeedbackReceived = target.FeedbacksReceived.Count();
            int rating = target.FeedbacksReceived.Sum(t => t.Rating);
            double averageRating = (rating * 1.0) / countsFeedbackReceived;
            target.Rating = averageRating;

            await this.db.SaveChangesAsync();

            var commentReturn = await this.db.Feedbacks
                                             .Select(this.func.FeedbackToFeedbackReturnDto())
                                             .FirstOrDefaultAsync(f => f.FeedbackId == feedback.Id);

            return commentReturn;
        }

        public async Task<FeedbackReturnDto> PassengerWriteFeedbackAsync(int authorId, FeedbackCreateDto input)
        {
            var author = await this.db.Customers
                                      .FirstOrDefaultAsync(c => c.Id == authorId);
            this.UserExist(author);


            int targetId = input.TargetId;
            var target = await this.db.Customers
                                      .Include(x => x.FeedbacksReceived)
                                      .FirstOrDefaultAsync(c => c.Id == targetId);
            this.UserExist(target);

            var travel = await this.db.Travels
                                      .Include(t => t.TravelPassengers)
                                      .FirstOrDefaultAsync(t => t.Id == input.TravelId);
            this.TravelExists(travel);
            if (travel.TravelStatus != TravelStatus.Completed)
            {
                throw new TravelDoesNotCompleteException("The Travel must be completed!");
            }

            if (!travel.TravelPassengers.Any(tp => tp.PassengerId == authorId && tp.ApplyStatus == ApplyStatus.Accepted))
            {
                throw new UserNotExistException("You did not attend to this travel!");
            }

            if (travel.DriverId != targetId)
            {
                throw new UserNotExistException("You want to leave a feedback for someone who did not take part in the trip!");
            }


            if (await this.db.Feedbacks.AnyAsync(f => f.TargetId == targetId && f.TravelId == input.TravelId && f.AuthorId == author.Id))
            {
                throw new FeedbackException($"You already wrote a feedback to {target.Username}");
            }

            var feedback = new Feedback
            {
                Rating = input.Rating,
                TravelId = input.TravelId,
                AuthorId = authorId,
                TargetId = targetId
            };
            await this.db.Feedbacks.AddAsync(feedback);

            if (input.Comment != null)
            {
                var comment = new FeedbackComment
                {
                    Feedback = feedback,
                    Comment = input.Comment
                };
                await this.db.FeedbackComments.AddAsync(comment);
            }

            int countsFeedbackReceived = target.FeedbacksReceived.Count();
            int rating = target.FeedbacksReceived.Sum(t => t.Rating);
            double averageRating = (rating * 1.0) / countsFeedbackReceived;
            target.Rating = averageRating;

            await this.db.SaveChangesAsync();

            var commentReturn = await this.db.Feedbacks
                                             .Select(this.func.FeedbackToFeedbackReturnDto())
                                             .FirstOrDefaultAsync(f => f.FeedbackId == feedback.Id);

            return commentReturn;
        }

        public async Task<int> MaxPageAsync(int customerId, int byPage, bool IsGiven)
        {
            IQueryable<Feedback> feedback = null;
            if (IsGiven)
            {
                feedback = this.db.Feedbacks.Where(x => x.AuthorId == customerId);
            }
            else
            {
                feedback = this.db.Feedbacks.Where(x => x.TargetId == customerId);
            }
            int count = await feedback.CountAsync();

            int maxPage = (int)Math.Ceiling(count * 1.0 / byPage);

            return maxPage;
        }
    }
}