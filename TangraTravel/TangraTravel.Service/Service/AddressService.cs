﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

using TangraTravel.Data.DataBase;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Models.AddressDtos;

namespace TangraTravel.Service.Service
{
    public class AddressService : IAddressService
    {
        private readonly TangraTravelContext db;

        public AddressService(TangraTravelContext db)
        {
            this.db = db;
        }

        public async Task<SelectList> PrepareAddressInformationAsync()
        {
            var addresses = await this.db.Addresses
                                 .Select(x => new
                                 {
                                     Id = x.Id,
                                     Information = $"{x.StreetName}, {x.City.Name}, {x.City.Country.Name}"
                                 }).ToArrayAsync();

            var selectedList = new SelectList(addresses, "Id", "Information");

            return selectedList;
        }

        public async Task<AddressDto> GetAddressInformation()
        {
            var addresses = await this.db.Addresses
                                          .Select(x => new { StreetName = x.StreetName, Id = x.Id })
                                          .ToArrayAsync();

            var cities = await db.Cities
                                  .Select(x => new { CityName = x.Name, Id = x.Id })
                                  .ToArrayAsync();

            var countries = await this.db.Countries
                                          .Select(x => new { CountryName = x.Name, Id = x.Id })
                                          .ToArrayAsync();

            var selectedListAddresses = new SelectList(addresses, "Id", "StreetName");
            var selectedListCities = new SelectList(cities, "Id", "CityName");
            var selectedListCountries = new SelectList(countries, "Id", "CountryName");

            var result = new AddressDto
            {
                Addresses = selectedListAddresses,
                Cities = selectedListCities,
                Countires = selectedListCountries
            };

            return result;
        }
    }
}