﻿using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using TangraTravel.Data.DataBase;
using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Service.Abstract;

namespace TangraTravel.Service.Service
{
    public class UserAuthService : AbstractService, IUserAuthService
    {
        private const string emailPattern = "@tangratravel.com";

        public UserAuthService(TangraTravelContext db)
            : base(db)
        {
        }

        public async Task<User> LogInAsync(string email, string password = null)
        {
            email = this.RemoveSpaces(email).ToLower();
            User user = null;
            if (password == null)
            {
                user = await this.LogInAsync(email);
            }
            else if (email.EndsWith(emailPattern))
            {
                user = await this.db.Admins
                                    .Include(r => r.Role)
                                    .FirstOrDefaultAsync(a => a.Email == email);
            }
            else
            {
                user = await this.db.Customers
                                    .Include(r => r.Role)
                                    .FirstOrDefaultAsync(a => a.Email == email);
            }

            this.ValidationForLogIn(user, password);
            return user;
        }

        private async Task<User> LogInAsync(string email)
        {
            User user = null;
            if (email.EndsWith(emailPattern))
            {
                user = await this.db.Admins
                                    .Include(r => r.Role)
                                    .FirstOrDefaultAsync(a => a.Email == email);
            }
            else
            {
                user = await this.db.Customers
                                    .Include(r => r.Role)
                                    .FirstOrDefaultAsync(a => a.Email == email);
            }

            return user;
        }
    }
}