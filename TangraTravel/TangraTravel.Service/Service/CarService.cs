﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Extension;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Service.Abstract;

namespace TangraTravel.Service.Service
{
    public class CarService : AbstractService, ICarService
    {
        private ICustomExpression func;
        private readonly ICustomerService customerService;
        private readonly IAvatarService avatarService;

        public CarService(TangraTravelContext db, ICustomExpression func, ICustomerService customerService, IAvatarService avatarService)
            : base(db)
        {
            this.func = func;
            this.customerService = customerService;
            this.avatarService = avatarService;
        }

        public async Task<CarReturnDto> CreateAsync(int customerId, CarCreateDto input)
        {
            input.RegistrationNumber = input.RegistrationNumber.ToUpper();
            await this.RegistrationNumberExistAsync(input.RegistrationNumber);

            if (!(await this.db.Models.AnyAsync(x => x.Id == input.ModelId)))
            {
                throw new CarException("Selected model does not exist in database!");
            }

            var car = new Car
            {
                ModelId = input.ModelId,
                RegistrationNumber = input.RegistrationNumber,
                Seats = input.Seats,
                CustomerId = customerId
            };

            await this.customerService.ChangeRoleAsync(customerId, Access.Driver);
            await this.db.Cars.AddAsync(car);
            await this.db.SaveChangesAsync();

            var returnCar = await this.db.Cars
                                         .Select(this.func.CarToCarReturnModel())
                                         .FirstOrDefaultAsync(c => c.CarId == car.Id);

            return returnCar;
        }

        public async Task<CarReturnDto> GetByUerIdAsync(int customerId)
        {
            var car = await this.db.Cars
                                   .Select(this.func.CarToCarReturnModel())
                                   .FirstOrDefaultAsync(c => c.CustomerId == customerId);

            this.CarIsNull<CarReturnDto>(car);
            car.Avatar = this.avatarService.GetCarAvatar(car.Brand);

            return car;
        }

        public async Task<CarReturnDto> GetByCarIdAsync(int carId)
        {
            var car = await this.db.Cars
                                   .Select(this.func.CarToCarReturnModel())
                                   .FirstOrDefaultAsync(c => c.CarId == carId);

            this.CarIsNull<CarReturnDto>(car);
            car.Avatar = this.avatarService.GetCarAvatar(car.Brand);

            return car;
        }

        public async Task<ICollection<CarReturnDto>> GetByPageAsync(int page, int? perPage)
        {
            var cars = this.db.Cars.Select(this.func.CarToCarReturnModel());

            if (perPage.HasValue)
            {
                cars = cars.Paginate(page, perPage.Value);
            }
            else
            {
                cars = cars.Paginate(page);
            }

            var list = await cars.ToArrayAsync();
            foreach (var car in list)
            {
                car.Avatar = this.avatarService.GetCarAvatar(car.Brand);
            }

            return list;
        }

        public async Task<CarReturnDto> UpdateAsync(int customerId, CarUpdateDto input)
        {
            var car = await this.db.Cars.FirstOrDefaultAsync(c => c.CustomerId == customerId);
            this.CarIsNull<Car>(car);

            input.RegistrationNumber = input.RegistrationNumber.ToUpper();
            await this.RegistrationNumberExistAsync(input.RegistrationNumber);

            car.RegistrationNumber = input.RegistrationNumber;
            await this.db.SaveChangesAsync();

            var returnCar = await this.db.Cars
                                         .Select(this.func.CarToCarReturnModel())
                                         .FirstOrDefaultAsync(c => c.CarId == car.Id);
            return returnCar;
        }

        public async Task DeleteAsync(int customerId)
        {
            var car = await this.db.Cars.FirstOrDefaultAsync(c => c.CustomerId == customerId);
            this.CarIsNull<Car>(car);

            var driverTravels = await this.db.Travels
                                       .Select(t => new
                                       {
                                           TravelId = t.Id,
                                           DriverId = t.DriverId,
                                           TravelStatus = t.TravelStatus
                                       })
                                       .Where(t => t.DriverId == car.CustomerId && t.TravelStatus != TravelStatus.Completed)
                                       .CountAsync();
            if (driverTravels != 0)
            {
                throw new CarException("You cannot delete a car, because you still have active travels!");
            }

            await this.customerService.ChangeRoleAsync(customerId, Access.Passenger);
            car.RegistrationNumber = null;
            this.db.Cars.Remove(car);
            await this.db.SaveChangesAsync();
        }

        public async Task<SelectList> TakeBrandsAndModelsAsync()
        {
            var cars = await this.db.Models
                                 .Select(m => new
                                 {
                                     ModelId = m.Id,
                                     Model = m.BrandModel,
                                     BrandId = m.BrandId,
                                     BrandName = m.Brand.Name,
                                     Text = $"{m.Brand.Name} {m.BrandModel}"
                                 })
                                 .OrderBy(b => b.BrandName)
                                 .ThenBy(m => m.Model)
                                 .ToArrayAsync();

            var result = new SelectList(cars, "ModelId","Text");
            return result;
        }

        public async Task<int> MaxPage(int byPage)
        {
            int count = await this.db.Cars.CountAsync();

            int maxPage = (int)Math.Ceiling(count * 1.0 / byPage);

            return maxPage;
        }

        private async Task RegistrationNumberExistAsync(string registration)
        {
            if (await this.db.Cars.AnyAsync(c => c.RegistrationNumber == registration))
            {
                throw new CarException($"Car with registration: {registration} already exist in database");
            }
        }
    }
}