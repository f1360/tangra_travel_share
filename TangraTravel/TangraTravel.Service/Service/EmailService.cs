﻿using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Options;

using TangraTravel.Service.Extension;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Models.Email;
using static TangraTravel.Service.Helper.SmtpClientHelper;

namespace TangraTravel.Service.Service
{
    public sealed class EmailService : IEmailService
    {
        private readonly EmailServiceOptions options;
        private readonly SmtpClient smtpClient;

        public EmailService(IOptions<EmailServiceOptions> serviceOptions)
        {
            options = serviceOptions.Value;
            smtpClient = CreateSmtpClient(options);
        }

        public async Task SendEmailAsync(Email email, CancellationToken cancellationToken = default)
        {
            smtpClient.EnableSsl = true;

            await smtpClient
                .SendMailAsync(email.ToMailMessage(options), cancellationToken)
                .ConfigureAwait(false);
        }

        public void Dispose()
        {
            if (smtpClient is not null)
            {
                smtpClient.Dispose();
            }
        }
    }
}
