﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Service.Enum;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Extension;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Service.Abstract;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.Service.Exeception.Travel;

namespace TangraTravel.Service.Service
{
    public class TravelService : AbstractService, ITravelService
    {
        private ICustomExpression func;
        private readonly ICustomerService customerService;

        public TravelService(TangraTravelContext db, ICustomExpression func, ICustomerService customerService)
            : base(db)
        {
            this.func = func;
            this.customerService = customerService;
        }

        public async Task<TravelPresentDto> GetTravelAsync(int travelId)
        {
            var travel = await this.db.Travels
                                   .Select(this.func.TravelToTravelPresentDto())
                                   .FirstOrDefaultAsync(t => t.TravelId == travelId);

            this.TravelExists<TravelPresentDto>(travel);

            return travel;
        }

        public async Task<ICollection<TravelPresentDto>> GetAllTravelsAsync()
        {
            var travels = await this.db.Travels
                                    .Select(this.func.TravelToTravelPresentDto())
                                    .ToArrayAsync();

            return travels;
        }

        public async Task<IEnumerable<TravelPresentDto>> GetAllAdminTravelsAsync(int page, int perPage)
        {
            var travels = this.db.Travels
                .Select(this.func.TravelToTravelPresentDto());

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPresentDto>> GetAllTravelsFilteredByStartPoint(int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.StartAddressId == addressId)
                .Select(this.func.TravelToTravelPresentDto());

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPresentDto>> GetAllTravelsFilteredByEndPoint(int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.EndAddressId == addressId)
                .Select(this.func.TravelToTravelPresentDto());

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPresentDto>> GetAllTravelsFilteredByDriver(int page, int perPage, int driverId)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId == driverId)
                .Select(this.func.TravelToTravelPresentDto());

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPresentDto>> GetAllTravelsSorted(int page, int perPage, string criterion)
        {
            IQueryable<Travel> travels = this.db.Travels;

            if (criterion == "freeSeats")
            {
                travels = travels.OrderByDescending(t => t.FreeSeats);
            }
            else if (criterion == "depTime")
            {
                travels = travels.OrderByDescending(t => t.DepartureTime);
            }

            var sortedTravels = travels.Select(this.func.TravelToTravelPresentDto());

            sortedTravels = sortedTravels.Paginate(page, perPage);

            var results = await sortedTravels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravels(int customerId, int page, int perPage)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId != customerId && t.TravelStatus == TravelStatus.Open);

            travels = travels.Paginate(page, perPage);

            var results = await travels.Select(this.func.TravelToTravelPassengerDetailsDto(customerId)).ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsFilteredByStartPoint(int customerId, int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId != customerId && t.TravelStatus == TravelStatus.Open && t.StartAddressId == addressId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsFilteredByEndPoint(int customerId, int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId != customerId && t.TravelStatus == TravelStatus.Open && t.EndAddressId == addressId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsFilteredByDriver(int customerId, int page, int perPage, int driverId)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId != customerId && t.TravelStatus == TravelStatus.Open && t.DriverId == driverId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllAvaliableTravelsSorted(int customerId, int page, int perPage, string criterion)
        {
            IQueryable<Travel> travels = this.db.Travels
                .Where(t => t.DriverId != customerId && t.TravelStatus == TravelStatus.Open);

            if (criterion == "freeSeats")
            {
                travels = travels.OrderByDescending(t => t.FreeSeats);
            }
            else if (criterion == "depTime")
            {
                travels = travels.OrderByDescending(t => t.DepartureTime);
            }

            var sortedTravels = travels.Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            sortedTravels = sortedTravels.Paginate(page, perPage);

            var results = await sortedTravels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsFilteredByStartPoint(int customerId, int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.TravelPassengers.Any(tp => tp.PassengerId == customerId) && t.StartAddressId == addressId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsFilteredByEndPoint(int customerId, int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.TravelPassengers.Any(tp => tp.PassengerId == customerId) && t.EndAddressId == addressId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsFilteredByDriver(int customerId, int page, int perPage, int driverId)
        {
            var travels = this.db.Travels
                .Where(t => t.TravelPassengers.Any(tp => tp.PassengerId == customerId) && t.DriverId == driverId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravelsSorted(int customerId, int page, int perPage, string criterion)
        {
            IQueryable<Travel> travels = this.db.Travels
                .Where(t => t.TravelPassengers.Any(tp => tp.PassengerId == customerId));

            if (criterion == "freeSeats")
            {
                travels = travels.OrderByDescending(t => t.FreeSeats);
            }
            else if (criterion == "depTime")
            {
                travels = travels.OrderByDescending(t => t.DepartureTime);
            }

            var sortedTravels = travels.Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            sortedTravels = sortedTravels.Paginate(page, perPage);

            var results = await sortedTravels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllDriverTravelsFilteredByStartPoint(int customerId, int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId == customerId && t.StartAddressId == addressId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllDriverTravelsFilteredByEndPoint(int customerId, int page, int perPage, int addressId)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId == customerId && t.EndAddressId == addressId)
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllDriverTravelsSorted(int customerId, int page, int perPage, string criterion)
        {
            IQueryable<Travel> travels = this.db.Travels
                .Where(t => t.DriverId == customerId);

            if (criterion == "freeSeats")
            {
                travels = travels.OrderByDescending(t => t.FreeSeats);
            }
            else if (criterion == "depTime")
            {
                travels = travels.OrderByDescending(t => t.DepartureTime);
            }

            var sortedTravels = travels.Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            sortedTravels = sortedTravels.Paginate(page, perPage);

            var results = await sortedTravels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPresentDto>> GetAllDriverTravels(int customerId, int page, int perPage)
        {
            var travels = this.db.Travels
                .Where(t => t.DriverId == customerId)
                .Select(this.func.TravelToTravelPresentDto());

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<IEnumerable<TravelPassengerDetailsDto>> GetAllPassengerTravels(int customerId, int page, int perPage)
        {
            var travels = this.db.Travels
                .Where(t => t.TravelPassengers.Any(tp => tp.PassengerId == customerId))
                .Select(this.func.TravelToTravelPassengerDetailsDto(customerId));

            travels = travels.Paginate(page, perPage);

            var results = await travels.ToArrayAsync();

            return results;
        }

        public async Task<int> MaxPageForAllTravels()
        {
            int count = await this.db.Travels.CountAsync();

            int maxPage = (int)Math.Ceiling(count * 1.0 / 5);

            return maxPage;
        }

        public async Task<int> MaxPageForAvailableTravels(int customerId)
        {
            int count = await this.db.Travels.Where(t => t.DriverId != customerId && t.TravelStatus == TravelStatus.Open).CountAsync();

            int maxPage = (int)Math.Ceiling(count * 1.0 / 5);

            return maxPage;
        }

        public async Task<int> MaxPageForDriverTravels(int customerId)
        {
            int count = await this.db.Travels.Where(t => t.DriverId == customerId).CountAsync();

            int maxPage = (int)Math.Ceiling(count * 1.0 / 5);

            return maxPage;
        }

        public async Task<int> MaxPageForPassengerTravels(int customerId)
        {
            int count = await this.db.Travels
                .Include(t => t.TravelPassengers)
                .Where(t => t.TravelPassengers.Any(tp => tp.PassengerId == customerId)).CountAsync();

            int maxPage = (int)Math.Ceiling(count * 1.0 / 5);

            return maxPage;
        }

        public async Task<TravelPresentDto> CreateTravelAsync(TravelCreateDto inputDto)
        {
            int freeSeats = await this.customerService.GetCarSeats(inputDto.DriverId);

            var travel = new Travel
            {
                DriverId = inputDto.DriverId,
                StartAddressId = inputDto.StartAddressId,
                EndAddressId = inputDto.EndAddressId,
                DepartureTime = DateTime.Parse(inputDto.DepartureTime),
                TravelStatus = TravelStatus.Open,
                FreeSeats = freeSeats
            };

            await this.db.Travels.AddAsync(travel);
            await this.db.SaveChangesAsync();

            if (inputDto.Comment != null)
            {
                var travelComment = new TravelComment
                {
                    Comment = inputDto.Comment,
                    TravelId = travel.Id
                };

                await this.db.TravelComments.AddAsync(travelComment);
                await this.db.SaveChangesAsync();
            }

            var travelDto = await this.db.Travels
                                         .Select(this.func.TravelToTravelPresentDto())
                                         .FirstOrDefaultAsync(t => t.TravelId == travel.Id);

            return travelDto;
        }

        public async Task<TravelPresentDto> UpdateTravelAsync(int travelId, TravelUpdateDto inputDto)
        {
            var travel = await this.db.Travels.FirstOrDefaultAsync(t => t.Id == travelId);
            this.TravelExists<Travel>(travel);

            travel.DriverId = (int)(inputDto.DriverId != null ? inputDto.DriverId : travel.DriverId);
            travel.StartAddressId = (int)(inputDto.StartAddressId != null ? inputDto.StartAddressId : travel.StartAddressId);
            travel.EndAddressId = (int)(inputDto.EndAddressId != null ? inputDto.EndAddressId : travel.EndAddressId);
            travel.DepartureTime = inputDto.DepartureTime != null ? DateTime.Parse(inputDto.DepartureTime) : travel.DepartureTime;
            travel.TravelStatus = inputDto.TravelStatus != null ? (TravelStatus)inputDto.TravelStatus : travel.TravelStatus;

            await this.db.SaveChangesAsync();

            var travelDto = await this.db.Travels
                                         .Select(this.func.TravelToTravelPresentDto())
                                         .FirstOrDefaultAsync(t => t.TravelId == travel.Id);
            return travelDto;
        }

        public async Task DeleteTravelAsync(int travelId)
        {
            var travel = await this.db.Travels.FirstOrDefaultAsync(t => t.Id == travelId);
            this.TravelExists<Travel>(travel);

            this.db.Travels.Remove(travel);

            await this.db.SaveChangesAsync();
        }

        public async Task ChangeTravelStatusAsync(int travelId, string status)
        {
            var travel = await this.db.Travels.FirstOrDefaultAsync(t => t.Id == travelId);
            this.TravelExists<Travel>(travel);

            travel.TravelStatus = (TravelStatus)TravelStatus.Parse(typeof(TravelStatus), status, true);

            await this.db.SaveChangesAsync();
        }

        public async Task<IEnumerable<TravelPresentDto>> FilterByDriverAsync(int driverId)
        {
            IQueryable<Travel> travels = this.db.Travels
                .Where(t => t.DriverId == driverId);

            var filteredByDriver = await travels.Select(this.func.TravelToTravelPresentDto())
                .ToListAsync();

            return filteredByDriver;
        }

        public async Task<IEnumerable<TravelPresentDto>> FilterByStartAddressAsync(int startAddressId)
        {
            IQueryable<Travel> travels = this.db.Travels
                .Where(t => t.StartAddressId == startAddressId);

            var filteredByStartAddress = await travels.Select(this.func.TravelToTravelPresentDto())
                .ToListAsync();

            return filteredByStartAddress;
        }

        public async Task<IEnumerable<TravelPresentDto>> FilterByEndAddressAsync(int endAddressId)
        {
            IQueryable<Travel> travels = this.db.Travels
                .Where(t => t.EndAddressId == endAddressId);

            var filteredByEndAddress = await travels.Select(this.func.TravelToTravelPresentDto())
                .ToListAsync();

            return filteredByEndAddress;
        }

        public async Task<IEnumerable<TravelPresentDto>> FilterByDepartureTimeAsync(string departureTime)
        {
            DateTime validDepartureTime;

            bool successConvertDate = DateTime.TryParse(departureTime, out validDepartureTime);

            if (successConvertDate)
            {
                IQueryable<Travel> travels = this.db.Travels
                .Where(t => t.DepartureTime == validDepartureTime);

                var filteredByDepartureTime = await travels.Select(this.func.TravelToTravelPresentDto())
                    .ToListAsync();

                return filteredByDepartureTime;
            }
            else
            {
                throw new InvalidDepartureTimeException("Invalid departure time input!");
            }
        }

        public async Task<IEnumerable<TravelPresentDto>> FilterByTravelStatusAsync(int travelStatusId)
        {
            IQueryable<Travel> travels = this.db.Travels
                .Where(t => (int)t.TravelStatus == travelStatusId);

            var filteredByTravelStatus = await travels.Select(this.func.TravelToTravelPresentDto())
                .ToListAsync();

            return filteredByTravelStatus;
        }

        //TODO: Filter by passenger

        public async Task<IEnumerable<TravelPresentDto>> SortByAsync(TravelSortByDto inputData)
        {
            IQueryable<Travel> travels = this.db.Travels;

            switch (inputData.Criteria)
            {
                case SortBy.Driver:
                    travels = travels.OrderBy(t => t.DriverId);

                    break;
                case SortBy.StartAddress:
                    travels = travels.OrderBy(t => t.StartAddressId);

                    break;
                case SortBy.EndAddress:
                    travels = travels.OrderBy(t => t.EndAddressId);

                    break;
                case SortBy.DepartureTime:
                    travels = travels.OrderBy(t => t.DepartureTime);

                    break;
                case SortBy.TravelStatus:
                    travels = travels.OrderBy(t => t.TravelStatus);

                    break;
            }

            var sortedTravels = await travels.Select(this.func.TravelToTravelPresentDto())
                .ToListAsync();

            return sortedTravels;
        }

        public async Task<IList<TravelFeedbackDto>> RetriveCompletedTravelsAsync(int userId, int page)
        {
            var travels = await this.db.Travels
                                       .Where(
                                          t => (t.DriverId == userId || t.TravelPassengers.Any(p => p.PassengerId == userId && p.ApplyStatus == ApplyStatus.Accepted)) && t.TravelStatus == TravelStatus.Completed)
                                       .Select(t => new TravelFeedbackDto
                                       {
                                           TravelId = t.Id,
                                           Role = t.DriverId == userId ? "Driver" : "Passenger",
                                           ArrivalDate = t.DepartureTime.Date.ToString("d"),
                                           StartPoint = $"{t.StartAddress.StreetName} {t.StartAddress.City.Name}",
                                           EndPoint = $"{t.EndAddress.StreetName} {t.EndAddress.City.Name}",
                                           PassengerCounts = t.TravelPassengers.Where(x => x.ApplyStatus == ApplyStatus.Accepted).Count()
                                       })
                                       .Paginate(page)
                                       .ToArrayAsync();

            var customer = await this.db.Customers
                                     .Include(x => x.FeedbacksGiven)
                                     .FirstOrDefaultAsync(c => c.Id == userId);

            var travelPassengers = await this.db.Travels
                                             .Where(x => x.TravelStatus == TravelStatus.Completed)
                                             .Select(x => new
                                             {
                                                 TravelId = x.Id,
                                                 PassengerCounts = x.TravelPassengers.Where(t => t.ApplyStatus == ApplyStatus.Accepted).Count()
                                             }).ToArrayAsync();

            var givenFeedbacks = customer.FeedbacksGiven.ToHashSet();
            var list = new List<TravelFeedbackDto>();
            foreach (var travel in travels)
            {
                int travelId = travel.TravelId;
                int givenCount = givenFeedbacks.Where(f => f.TravelId == travelId).Count();
                if (travel.Role == "Passenger" && givenCount == 0)
                {
                    list.Add(travel);

                }
                else if (travel.Role == "Driver")
                {
                    int count = travelPassengers.FirstOrDefault(x => x.TravelId == travelId).PassengerCounts;
                    if (givenCount < count)
                    {
                        list.Add(travel);
                    }
                }
            }

            return list;
        }

        public async Task<TravelCustomer> RetriveDriverInFinishedTravelAsync(int travelId, int userId)
        {
            var driver = await this.db.Travels
                                   .Where(t => t.Id == travelId && t.TravelStatus == TravelStatus.Completed)
                                   .Select(t => new TravelCustomer
                                   {
                                       TargetId = t.DriverId,
                                       FullName = $"{t.Driver.FirstName} {t.Driver.LastName}",
                                       Username = t.Driver.Username
                                   }).FirstOrDefaultAsync();

            var isFeedback = await this.db.Feedbacks.AnyAsync(x => x.TargetId == driver.TargetId && x.AuthorId == userId);
            if (isFeedback)
            {
                return null;
            }

            return driver;
        }

        public async Task<IList<TravelCustomer>> RetrivePassengerInFinishedTravelsAsync(int travelId)
        {
            var passengers = await this.db.TravelPassengers
                                          .Where(t => t.TravelId == travelId && t.ApplyStatus == ApplyStatus.Accepted)
                                          .Select(t => new TravelCustomer
                                          {
                                              TargetId = t.PassengerId,
                                              FullName = $"{t.Passenger.FirstName} {t.Passenger.LastName}",
                                              Username = t.Passenger.Username
                                          }
                                          ).ToListAsync();

            var feedbacks = await this.db.Feedbacks
                                         .Select(c => new { TargetId = c.TargetId, TravelId = c.TravelId })
                                         .ToArrayAsync();

            var result = new List<TravelCustomer>();
            foreach (var passenger in passengers)
            {
                if (feedbacks.FirstOrDefault(x => x.TargetId == passenger.TargetId && x.TravelId == travelId) != null)
                {
                    continue;
                }
                result.Add(passenger);
            }

            return result;
        }

        public async Task<ICollection<TravelStoreDto>> GetSoonTravelsAsync(int userId, int take)
        {
            var currentDate = DateTime.UtcNow.Date;
            var travels = await this.db.Travels
                                    .Where(t => t.TravelStatus != TravelStatus.Completed && t.DepartureTime.Date <= currentDate)
                                    .OrderByDescending(t => t.DepartureTime)
                                    .Select(this.func.TravelToTravelStoreDto())
                                    .Take(take)
                                    .ToListAsync();
            return travels;
        }

        public async Task<int> CountAllTravels()
        {
            int travelsCount = await this.db.Travels
                                        .CountAsync();

            return travelsCount;
        }
    }
}