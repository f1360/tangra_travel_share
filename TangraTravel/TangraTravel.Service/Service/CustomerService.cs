﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Service.Enum;
using TangraTravel.Data.DataBase;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Extension;
using TangraTravel.Service.Models.Email;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Service.Abstract;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.Service.Service
{
    public class CustomerService : AbstractService, ICustomerService
    {
        private readonly IAvatarService avatarService;
        private readonly ICustomExpression func;
        private readonly IEmailService emailService;
        private static Random random = new Random();

        public CustomerService(TangraTravelContext db, IAvatarService avatarService, ICustomExpression func, IEmailService emailService)
            : base(db)
        {
            this.avatarService = avatarService;
            this.func = func;
            this.emailService = emailService;
        }

        public async Task<CustomerReturnDto> GetCustomerAsync(int customerId)
        {
            var customer = await this.db.Customers
                                        .Select(this.func.CustomerToCustomerReturnModel())
                                        .FirstOrDefaultAsync(c => c.CustomerId == customerId);

            this.UserExist<CustomerReturnDto>(customer);
            customer.Avatar = await this.avatarService.GetAvatarAsync(customer.CustomerId);
            return customer;
        }

        public async Task<ICollection<CustomerReturnDto>> GetTopTenDriversAsync()
        {
            IQueryable<Customer> drivers = this.db.Customers
                .Where(c => c.Role.Access == Access.Driver)
                .OrderByDescending(c => c.Rating)
                .Take(10);

            var topTenDrivers = await drivers.Select(this.func.CustomerToCustomerReturnModel())
                .ToArrayAsync();

            foreach (var customer in topTenDrivers)
            {
                customer.Avatar = await this.avatarService.GetAvatarAsync(customer.CustomerId);
            }

            return topTenDrivers;
        }

        public async Task<ICollection<CustomerReturnDto>> GetTopTenPassengersAsync()
        {
            IQueryable<Customer> passengers = this.db.Customers
                .Where(c => c.Role.Access == Access.Driver || c.Role.Access == Access.Passenger)
                .OrderByDescending(c => c.Rating)
                .Take(10);

            var topTenPassengers = await passengers.Select(this.func.CustomerToCustomerReturnModel())
                .ToArrayAsync();

            foreach (var customer in topTenPassengers)
            {
                customer.Avatar = await this.avatarService.GetAvatarAsync(customer.CustomerId);
            }

            return topTenPassengers;
        }

        public async Task<ICollection<CustomerReturnDto>> GetCustomersByPageAsync(int page, int? perPage = null)
        {
            var customers = this.db.Customers.Select(this.func.CustomerToCustomerReturnModel());
            if (perPage.HasValue)
            {
                customers = customers.Paginate(page, perPage.Value);
            }
            else
            {
                customers = customers.Paginate(page);
            }
            var results = await customers.ToArrayAsync();

            foreach (var customer in results)
            {
                customer.Avatar = await this.avatarService.GetAvatarAsync(customer.CustomerId);
            }
            return results;
        }

        public async Task<CustomerReturnDto> SearchAsync(CustomerSearchByDto input)
        {
            IQueryable<Customer> customer = this.db.Customers;
            switch (input.Criteria)
            {
                case SearchBy.Email: customer = customer.Where(c => c.Email == this.RemoveSpaces(input.Value).ToLower()); break;
                case SearchBy.Phone: customer = customer.Where(c => c.PhoneNumber == input.Value); break;
                case SearchBy.Username: customer = customer.Where(c => c.Username == input.Value); break;
            }

            var user = await customer.Select(this.func.CustomerToCustomerReturnModel()).FirstOrDefaultAsync();
            if (user != null)
            {
                user.Avatar = await this.avatarService.GetAvatarAsync(user.CustomerId);
            }

            return user;
        }

        public async Task<CustomerReturnDto> CreateAsync(CustomerCreateDto input)
        {

            string email = this.RemoveSpaces(input.Email);
            await this.ValidationUniquePropertiesAsync(isAdmin: false, email, input.PhoneNumber, input.Username);
            string randomString = RandomString(8);

            var customer = new Customer
            {
                FirstName = input.FirstName,
                LastName = input.LastName,
                Username = input.Username,
                Email = email,
                Password = input.Password,
                AddressId = input.AddressId,
                PhoneNumber = input.PhoneNumber,
                ConfirmationString = randomString,
                Role = await this.db.Roles.FirstOrDefaultAsync(r => r.Access == Access.Passenger),
            };
            if (input.Sex != null)
            {
                customer.Gender = input.Sex.Value;
            }
            await this.db.Customers.AddAsync(customer);
            if (input.Avatar != null)
            {
                await this.avatarService.CreateAvatarAsync(customer, input.Avatar);
            }
            await this.db.SaveChangesAsync();

            var customerModel = await this.db.Customers
                                             .Select(this.func.CustomerToCustomerReturnModel())
                                             .FirstOrDefaultAsync(c => c.CustomerId == customer.Id);

            customerModel.Avatar = await this.avatarService.GetAvatarAsync(customerModel.CustomerId);

            string confirmationLink = $"https://localhost:5001/Auth/LoginConfirm?access={randomString}";

            var mail = new Email
            {
                To = new List<string> { $"{email}" },
                Subject = "Confirm your registration",
                Body = $"Here is your confirmation link: {confirmationLink}",
            };

            await emailService.SendEmailAsync(mail)
                .ConfigureAwait(false);


            return customerModel;
        }

        public async Task<CustomerReturnDto> UpdateAsync(int customerId, CustomerUpdateDto input)
        {
            Customer customer = await this.db.Customers.FirstOrDefaultAsync(x => x.Id == customerId);
            this.UserExist(customer);

            if (input.NewEmail != null)
            {
                input.NewEmail = this.RemoveSpaces(input.NewEmail);
            }
            bool newEmail = input.NewEmail != null && input.NewEmail != customer.Email;
            bool newPhone = input.PhoneNumber != null && input.PhoneNumber != customer.PhoneNumber;
            bool newUsername = input.Username != null && input.Username != customer.Username;
            await this.ValidationUniquePropertiesAsync
                       (
                       false,
                       newEmail ? input.NewEmail : null,
                       newPhone ? input.PhoneNumber : null,
                       newUsername ? input.Username : null
                       );

            if (input.FirstName != null)
            {
                customer.FirstName = input.FirstName;
            }
            if (input.LastName != null)
            {
                customer.LastName = input.LastName;
            }
            if (input.Username != null)
            {
                customer.Username = input.Username;
            }
            if (input.NewEmail != null)
            {
                customer.Email = input.NewEmail;
            }
            if (input.Password != null)
            {
                customer.Password = input.Password;
            }
            if (input.AddressId != null)
            {
                customer.AddressId = input.AddressId.Value;
            }
            if (input.PhoneNumber != null)
            {
                customer.PhoneNumber = input.PhoneNumber;
            }
            if (input.Avatar != null)
            {
                await this.avatarService.UpdateAvatarAsync(customer, input.Avatar);
            }
            if (input.Gender != null)
            {
                customer.Gender = input.Gender.Value;
            }

            await this.db.SaveChangesAsync();
            var customerModel = await this.db.Customers
                                              .Select(this.func.CustomerToCustomerReturnModel())
                                              .FirstOrDefaultAsync(c => c.CustomerId == customer.Id);

            customerModel.Avatar = await this.avatarService.GetAvatarAsync(customerModel.CustomerId);
            return customerModel;
        }

        public async Task DeleteCustomerAsync(int customerId)
        {
            var customer = await this.db.Customers
                                        .Include(c => c.Role)
                                        .FirstOrDefaultAsync(c => c.Id == customerId);

            this.UserExist(customer);
            if (customer.Role.Access == Access.Driver)
            {
                var car = await this.db.Cars.FirstOrDefaultAsync(c => c.CustomerId == customerId);
                car.RegistrationNumber = null;
                this.db.Cars.Remove(car);

                var travels = await this.db.Travels
                                           .Include(t => t.TravelComment)
                                           .Include(t => t.TravelPassengers)
                                           .Where(t => t.DriverId == customerId && t.TravelStatus != TravelStatus.Completed)
                                           .ToArrayAsync();
                if (travels.Any(t => t.TravelStatus == TravelStatus.Travelling))
                {
                    throw new DriverException("You must finish the travel first!");
                }

                foreach (var travel in travels)
                {
                    if (travel.TravelPassengers.Any(t => t.PassengerId == customerId))
                    {
                        var travelPassenger = travel.TravelPassengers.FirstOrDefault(p => p.PassengerId == customerId);
                        this.db.TravelPassengers.Remove(travelPassenger);
                        travel.FreeSeats++;
                    }
                    else
                    {
                        var travelPassengers = travel.TravelPassengers;
                        this.db.RemoveRange(travelPassengers);
                        this.db.Remove(travel);
                    }
                }
            }
            else if (customer.Role.Access == Access.Passenger)
            {
                var travelPassengers = await this.db.TravelPassengers
                                                    .Include(t => t.Travel)
                                                    .Where(d => d.PassengerId == customerId)
                                                    .ToArrayAsync();

                if (travelPassengers.Any(t => t.Travel.TravelStatus == TravelStatus.Travelling))
                {
                    throw new PassengerException("Please finish or cancel your travel/s first!");
                }

                foreach (var travelPassenger in travelPassengers)
                {
                    var travel = travelPassenger.Travel;
                    travel.FreeSeats++;
                    this.db.Travels.Update(travel);
                }

                this.db.TravelPassengers.RemoveRange(travelPassengers);
            }

            await this.avatarService.DeleteAvatarAsync(customer);
            customer.Email = null;
            customer.PhoneNumber = null;
            customer.Username = null;
            this.db.Customers.Remove(customer);

            await this.db.SaveChangesAsync();
        }

        public async Task BlockAsync(int customerId)
        {
            var customer = await this.db.Customers
                                        .FirstOrDefaultAsync(c => c.Id == customerId);
            this.UserExist(customer);

            customer.IsBlocked = true;
            await this.db.SaveChangesAsync();
        }

        public async Task UnblockAsync(int customerId)
        {
            var customer = await this.db.Customers
                                        .FirstOrDefaultAsync(c => c.Id == customerId);
            this.UserExist(customer);

            customer.IsBlocked = false;
            await this.db.SaveChangesAsync();
        }

        public async Task ChangeRoleAsync(int userId, Access role)
        {
            if (role == Access.Admin)
            {
                throw new InputExistException("The user can not be with Admin role!");
            }
            var user = await this.db.Customers.FirstOrDefaultAsync(c => c.Id == userId);
            this.UserExist(user);

            user.RoleId = (await this.db.Roles.FirstOrDefaultAsync(r => r.Access == role)).Id;
            if (role == Access.Passenger)
            {
                user.Car = null;
            }
        }

        public async Task<TravelPassengerPresentDto> ApplyForTravelAsync(int passengerId, int travelId, Access role)
        {
            if (role == Access.Admin)
            {
                throw new PermissionException("The user can not be with Admin role!");
            }

            var travelPassenger = new TravelPassenger
            {
                PassengerId = passengerId,
                TravelId = travelId,
                ApplyStatus = ApplyStatus.Applied
            };

            await this.db.TravelPassengers.AddAsync(travelPassenger);
            await this.db.SaveChangesAsync();

            var travelPassengerDto = await this.db.TravelPassengers
                                         .Select(this.func.TravelPassengerToTravelPassengerPresentDto())
                                         .FirstOrDefaultAsync(t => t.TravelPassengerId == travelPassenger.Id);

            return travelPassengerDto;
        }

        public async Task<ICollection<TravelPassengerPresentDto>> ListApplicationsAsync(int travelId)
        {
            var travelPassengerDtos = await this.db.TravelPassengers
                                         .Where(tp => tp.TravelId == travelId)
                                         .Select(this.func.TravelPassengerToTravelPassengerPresentDto())
                                         .ToArrayAsync();

            return travelPassengerDtos;
        }

        public async Task ProcessApplicationAsync(int travelPassengerId, TravelPassengerApplyStatusDto inputData)
        {
            var travelPassenger = await this.db.TravelPassengers
                                        .FirstOrDefaultAsync(tp => tp.Id == travelPassengerId);
            this.TravelPassengerExist(travelPassenger);

            travelPassenger.ApplyStatus = inputData.Criteria;
            await this.db.SaveChangesAsync();
        }

        public async Task<int> MaxPage(int byPage)
        {
            int count = await this.db.Customers.CountAsync();

            int maxPage = (int)Math.Ceiling(count * 1.0 / byPage);

            return maxPage;
        }

        public async Task<TravelPassengerPresentDto> GetApplicationAsync(int travelPassId)
        {
            var travelPassengerDto = await this.db.TravelPassengers
                                         .Select(this.func.TravelPassengerToTravelPassengerPresentDto())
                                         .FirstOrDefaultAsync(tp => tp.TravelPassengerId == travelPassId);

            return travelPassengerDto;
        }

        public async Task CancelTravel(int travelId, int customerId)
        {
            var travelPassenger = await this.db.TravelPassengers
                                        .FirstOrDefaultAsync(tp => tp.TravelId == travelId && tp.PassengerId == customerId);
            this.TravelPassengerExist(travelPassenger);

            travelPassenger.ApplyStatus = ApplyStatus.Canceled;
            await this.db.SaveChangesAsync();
        }

        public async Task<int> GetCarSeats(int userId)
        {
            var car = await this.db.Cars
                                        .Select(this.func.CarToCarReturnModel())
                                        .FirstOrDefaultAsync(c => c.CustomerId == userId);

            return car.Seats;
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public async Task ConfirmUser(string confirmationString)
        {
            var customer = await this.db.Customers
                                        .FirstOrDefaultAsync(c => c.ConfirmationString == confirmationString);
            this.UserExist(customer);
            customer.IsVerified = true;
            await this.db.SaveChangesAsync();
        }

        public async Task<int> CountAllUsers()
        {
            int customersCount = await this.db.Customers
                                        .CountAsync();

            return customersCount;
        }

        public async Task<SelectList> PrepareDriversInformationAsync()
        {
            var drivers = await this.db.Customers
                .Where(c => c.Role.Access == Access.Driver)
                .Select(c => new
                {
                    Id = c.Id,
                    Name = $"{c.FirstName} {c.LastName}"
                })
                .ToArrayAsync();

            var selectedList = new SelectList(drivers, "Id", "Name");

            return selectedList;
        }
    }
}