﻿using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using TangraTravel.Data.DataBase;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.Service.Exeception.User;
using TangraTravel.Service.Exeception.Travel;
using TangraTravel.Service.Exeception.TravelPassenger;

namespace TangraTravel.Service.Service.Abstract
{
    public abstract class AbstractService
    {
        private const string emailPattern = "@tangratravel.com";
        protected readonly TangraTravelContext db;

        protected AbstractService(TangraTravelContext db)
        {
            this.db = db;
        }

        protected async Task ValidationUniquePropertiesAsync(bool isAdmin, string email, string phone, string username = null)
        {
            if (isAdmin)
            {
                if (email != null && !email.EndsWith(emailPattern))
                {
                    throw new InputExistException($"Corporative email must be with domain: {emailPattern}");
                }
                if (email != null && await db.Admins.AnyAsync(x => x.Email == email))
                {
                    throw new InputExistException("Email exist in the system!");
                }
                if (phone != null && await db.Admins.AnyAsync(x => x.PhoneNumber == phone))
                {
                    throw new InputExistException("Phone exist in the system!");
                }
            }
            else
            {
                if (email != null && email.EndsWith(emailPattern))
                {
                    throw new InputExistException("Email is with corporative domain!");
                }
                if (email != null && await db.Customers.AnyAsync(x => x.Email == email))
                {
                    throw new InputExistException("Email exist in the system!");
                }
                if (phone != null && await db.Customers.AnyAsync(x => x.PhoneNumber == phone))
                {
                    throw new InputExistException("Phone exist in the system!");
                }
                if (username != null && await db.Customers.AnyAsync(x => x.Username == username))
                {
                    throw new InputExistException("Username exist in the system!");
                }
            }
        }

        protected string RemoveSpaces(string input)
        {
            return input.Replace(" ", string.Empty);
        }

        protected void ValidationForLogIn(User user, string password)
        {
            this.UserExist(user, true);
            if (password != null && user.Password != password)
            {
                throw new PasswordIncorrectException("Password is incorrect!");
            }
        }

        protected void UserExist<T>(T user, bool logIn = false)
        {
            if (user == null)
            {
                if (logIn)
                {
                    throw new EmailIncorrectException("There is no user with such email!");
                }
                else
                {
                    throw new UserNotExistException("There is no user in the system!");
                }
            }
        }

        protected void CarIsNull<T>(T car)
        {
            if (car == null)
            {
                throw new CarException("There is no such car in the system!");
            }
        }

        protected void TravelExists<T>(T travel)
        {
            if (travel == null)
            {
                throw new TravelDoesNotExistException("There is no such travel in the system!");
            }
        }

        protected void TravelPassengerExist<T>(T travelPassenger)
        {
            if (travelPassenger == null)
            {
                throw new TravelPassengerDoesNotExistException("There is no such travelPassenger record in the system!");
            }
        }
    }
}