﻿using System;

namespace TangraTravel.Service.Exeception.Travel
{
    public class TravelFailedCreationException : ApplicationException
    {
        public TravelFailedCreationException(string message)
        : base(message)
        {

        }
    }
}