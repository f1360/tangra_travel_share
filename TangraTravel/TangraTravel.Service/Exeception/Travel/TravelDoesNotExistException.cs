﻿using System;

namespace TangraTravel.Service.Exeception.Travel
{
    public class TravelDoesNotExistException : ApplicationException
    {       
        public TravelDoesNotExistException(string message)
        : base(message)
        {

        }
    }
}