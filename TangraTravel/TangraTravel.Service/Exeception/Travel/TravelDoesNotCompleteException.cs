﻿using System;

namespace TangraTravel.Service.Exeception.Travel
{
    public class TravelDoesNotCompleteException : ApplicationException
    {
        public TravelDoesNotCompleteException(string message)
            : base(message)
        {

        }
    }
}