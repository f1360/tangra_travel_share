﻿using System;

namespace TangraTravel.Service.Exeception.Travel
{
    public class TravelFailedDeletionException : ApplicationException
    {
        public TravelFailedDeletionException(string message)
        : base(message)
        {

        }
    }
}