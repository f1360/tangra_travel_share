﻿using System;

namespace TangraTravel.Service.Exeception.Travel
{
    public class InvalidDepartureTimeException : ApplicationException
    {
        public InvalidDepartureTimeException(string message)
        : base(message)
        {

        }
    }
}