﻿using System;

namespace TangraTravel.Service.Exeception.Car
{
    public class CarException : ApplicationException
    {
        public CarException(string message)
            : base(message)
        {

        }
    }
}