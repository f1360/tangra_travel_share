﻿using System;

namespace TangraTravel.Service.Exeception.TravelPassenger
{
    public class TravelPassengerDoesNotExistException : ApplicationException
    {
        public TravelPassengerDoesNotExistException(string message)
        : base(message)
        {

        }
    }
}