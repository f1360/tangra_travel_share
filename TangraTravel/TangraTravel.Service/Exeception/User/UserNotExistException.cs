﻿using System;

namespace TangraTravel.Service.Exeception.User
{
    public class UserNotExistException : ApplicationException
    {
        public UserNotExistException(string message)
        : base(message)
        {

        }
    }
}