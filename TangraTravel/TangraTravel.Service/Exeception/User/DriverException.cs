﻿using System;

namespace TangraTravel.Service.Exeception.User
{
    public class DriverException : ApplicationException
    {
        public DriverException(string message)
            : base(message)
        {

        }
    }
}