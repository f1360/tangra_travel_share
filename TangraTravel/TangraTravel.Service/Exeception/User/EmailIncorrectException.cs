﻿using System;

namespace TangraTravel.Service.Exeception.User
{
    public class EmailIncorrectException : ApplicationException
    {
        public EmailIncorrectException(string message)
            : base(message)
        {

        }
    }
}