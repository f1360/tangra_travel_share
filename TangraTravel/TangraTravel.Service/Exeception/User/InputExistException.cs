﻿using System;

namespace TangraTravel.Service.Exeception.User
{
    public class InputExistException : ApplicationException
    {
        public InputExistException(string message)
            : base(message)
        {

        }
    }
}