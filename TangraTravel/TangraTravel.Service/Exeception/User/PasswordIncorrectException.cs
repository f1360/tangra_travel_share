﻿using System;

namespace TangraTravel.Service.Exeception.User
{
    public class PasswordIncorrectException : ApplicationException
    {
        public PasswordIncorrectException(string message)
            : base(message)
        {

        }
    }
}