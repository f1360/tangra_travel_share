﻿using System;

namespace TangraTravel.Service.Exeception.User
{
    public class PassengerException : ApplicationException
    {
        public PassengerException(string message)
            : base(message)
        {

        }
    }
}