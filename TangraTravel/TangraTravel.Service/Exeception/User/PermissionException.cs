﻿using System;

namespace TangraTravel.Service.Exeception.User
{
    public class PermissionException : ApplicationException
    {
        public PermissionException(string message)
            : base(message)
        {

        }
    }
}