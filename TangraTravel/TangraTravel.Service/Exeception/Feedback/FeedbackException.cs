﻿using System;

namespace TangraTravel.Service.Exeception.Feedback
{
    public class FeedbackException : ApplicationException
    {
        public FeedbackException(string message)
            : base(message)
        {

        }
    }
}