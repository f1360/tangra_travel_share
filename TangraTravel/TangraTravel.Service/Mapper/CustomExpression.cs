﻿using System;
using System.Linq;
using System.Linq.Expressions;

using TangraTravel.Data.Enum;
using TangraTravel.Data.Models;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.Service.Models.AddressDtos;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Models.FeedbackDtos;
using TangraTravel.Service.Models.TravelCommentDtos;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.Service.Mapper
{
    public class CustomExpression : ICustomExpression
    {
        public Expression<Func<Car, CarReturnDto>> CarToCarReturnModel()
        {
            return c => new CarReturnDto
            {
                CustomerId = c.CustomerId.Value,
                CarId = c.Id,
                Brand = c.Model.Brand.Name,
                Model = c.Model.BrandModel,
                RegistrationNumber = c.RegistrationNumber,
                Seats = c.Seats,
                Owner = $"Full Name: {c.Customer.FirstName} {c.Customer.LastName}, Username: {c.Customer.Username}"
            };
        }

        public Expression<Func<Customer, CustomerReturnDto>> CustomerToCustomerReturnModel()
        {
            return c => new CustomerReturnDto
            {
                CustomerId = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                Email = c.Email,
                Gender = c.Gender,
                Username = c.Username,
                Rating = c.Rating,
                PhoneNumber = c.PhoneNumber,
                SystemStatus = c.Role.Access.ToString(),
                StatusProfile = c.IsBlocked == true ? "Blocked" : "Active",
                Address = new CustomerAddressDto
                {
                    AddressId = c.AddressId,
                    StreetAddress = c.Address.StreetName,
                    CityId = c.Address.CityId,
                    City = c.Address.City.Name,
                    CountryId = c.Address.City.CountryId,
                    Country = c.Address.City.Country.Name
                }
            };
        }

        public Expression<Func<Travel, TravelPresentDto>> TravelToTravelPresentDto()
        {
            return t => new TravelPresentDto
            {
                TravelId = t.Id,
                Driver = new DriverDto
                {
                    DriverName = $"{t.Driver.FirstName} {t.Driver.LastName}",
                    CarDetails = new CarTravelDto
                    {
                        Brand = t.Driver.Car.Model.Brand.Name,
                        Model = t.Driver.Car.Model.BrandModel,
                        RegistrationNumber = t.Driver.Car.RegistrationNumber,
                        Seats = t.Driver.Car.Seats
                    }
                },
                StartAddress = new CustomerAddressDto
                {
                    AddressId = t.StartAddressId,
                    StreetAddress = t.StartAddress.StreetName,
                    City = t.StartAddress.City.Name,
                    Country = t.StartAddress.City.Country.Name
                },
                EndAddress = new CustomerAddressDto
                {
                    AddressId = t.EndAddressId,
                    StreetAddress = t.EndAddress.StreetName,
                    City = t.EndAddress.City.Name,
                    Country = t.EndAddress.City.Country.Name
                },
                DepartureTime = t.DepartureTime.ToString("MM/dd/yyyy"),
                FreeSeats = t.FreeSeats,
                Status = t.TravelStatus.ToString(),
                Comment = new TravelCommentPresentDto
                {
                    Comment = t.TravelComment.Comment
                }
            };
        }

        public Expression<Func<Feedback, FeedbackReturnDto>> FeedbackToFeedbackReturnDto()
        {
            return f => new FeedbackReturnDto
            {
                FeedbackId = f.Id,
                Author = $"{f.Author.FirstName} {f.Author.LastName}",
                AuthorUsername = f.Author.Username,
                Target = $"{f.Target.FirstName} {f.Target.LastName}",
                TargetUsername = f.Target.Username,
                Rating = f.Rating,
                Comment = f.FeedbackComment != null ? f.FeedbackComment.Comment : "---No Comment---"
            };
        }

        public Expression<Func<TravelPassenger, TravelPassengerPresentDto>> TravelPassengerToTravelPassengerPresentDto()
        {
            return tp => new TravelPassengerPresentDto
            {
                TravelPassengerId = tp.Id,
                PassengerName = $"{tp.Passenger.FirstName} {tp.Passenger.LastName}",
                TravelId = tp.TravelId,
                ApplyStatus = tp.ApplyStatus
            };
        }

        public Expression<Func<Travel, TravelStoreDto>> TravelToTravelStoreDto()
        {
            return t => new TravelStoreDto
            {
                TravelId = t.Id,
                DepartureDate = t.DepartureTime.Date.ToString("d"),
                StartPoint = $"{t.StartAddress.City.Name}, {t.StartAddress.StreetName}",
                EndPoint = $"{t.EndAddress.City.Name}, {t.EndAddress.StreetName}",
                DriverId = t.DriverId,
                Driver = $"Username: {t.Driver.Username}, Phone: {t.Driver.PhoneNumber}",
                StatusTravel = t.TravelStatus.ToString(),
                TravelPassengers = t.TravelPassengers
                                     .Where(p => p.ApplyStatus == ApplyStatus.Accepted)
                                     .Select(p => new TravelPassengerStoreDto
                                     {
                                         PassengerId = p.PassengerId,
                                         Passenger = $"Username: {p.Passenger.Username}, Phone: {p.Passenger.PhoneNumber}"
                                     }).ToArray()
            };
        }

        public Expression<Func<Travel, TravelPassengerDetailsDto>> TravelToTravelPassengerDetailsDto(int customerId)
        {
            return t => new TravelPassengerDetailsDto
            {
                TravelId = t.Id,
                Driver = new DriverDto
                {
                    DriverName = $"{t.Driver.FirstName} {t.Driver.LastName}",
                    CarDetails = new CarTravelDto
                    {
                        Brand = t.Driver.Car.Model.Brand.Name,
                        Model = t.Driver.Car.Model.BrandModel,
                        RegistrationNumber = t.Driver.Car.RegistrationNumber,
                        Seats = t.Driver.Car.Seats
                    }
                },
                StartAddress = new CustomerAddressDto
                {
                    AddressId = t.StartAddressId,
                    StreetAddress = t.StartAddress.StreetName,
                    City = t.StartAddress.City.Name,
                    Country = t.StartAddress.City.Country.Name
                },
                EndAddress = new CustomerAddressDto
                {
                    AddressId = t.EndAddressId,
                    StreetAddress = t.EndAddress.StreetName,
                    City = t.EndAddress.City.Name,
                    Country = t.EndAddress.City.Country.Name
                },
                DepartureTime = t.DepartureTime.ToString("MM/dd/yyyy"),
                FreeSeats = t.FreeSeats,
                Status = t.TravelStatus.ToString(),
                Comment = new TravelCommentPresentDto
                {
                    Comment = t.TravelComment.Comment
                },
                PassengerStatus = t.TravelPassengers.Any(tp => tp.PassengerId == customerId) == true ? t.TravelPassengers.FirstOrDefault(tp => tp.PassengerId == customerId).ApplyStatus.ToString() : "No passengers"
            };
        }
    }
}