﻿using System;
using System.Linq.Expressions;

using TangraTravel.Data.Models;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Models.FeedbackDtos;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.Service.Mapper.Contracts
{
    public interface ICustomExpression
    {
        Expression<Func<Car, CarReturnDto>> CarToCarReturnModel();

        Expression<Func<Customer, CustomerReturnDto>> CustomerToCustomerReturnModel();

        Expression<Func<Travel, TravelPresentDto>> TravelToTravelPresentDto();

        Expression<Func<Feedback, FeedbackReturnDto>> FeedbackToFeedbackReturnDto();

        Expression<Func<TravelPassenger, TravelPassengerPresentDto>> TravelPassengerToTravelPassengerPresentDto();

        Expression<Func<Travel, TravelStoreDto>> TravelToTravelStoreDto();

        Expression<Func<Travel, TravelPassengerDetailsDto>> TravelToTravelPassengerDetailsDto(int customerId);
    }
}