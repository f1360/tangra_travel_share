﻿using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using TangraTravel.MVC.Mapper;
using TangraTravel.MVC.Models;
using TangraTravel.Service.Contracts;
using TangraTravel.MVC.Models.HomeDtos;

namespace TangraTravel.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICustomerService customerService;
        private readonly ITravelService travelService;

        public HomeController(ICustomerService customerService, ITravelService travelService)
        {
            this.customerService = customerService;
            this.travelService = travelService;
        }
                
        public async Task<IActionResult> Index()
        {
            var usersCount = await this.customerService.CountAllUsers();
            var travelsCount = await this.travelService.CountAllTravels();

            var view = new HomeViewModel
            {
                UsersCount = usersCount,
                TravelsCount = travelsCount
            };

            return View(view);
        }

        public async Task<IActionResult> TopDrivers()
        {
            var topDrivers = await this.customerService.GetTopTenDriversAsync();
            var topDriversView = topDrivers.Select(td => td.ToTopCustomerViewModel()).ToArray();

            return View(topDriversView);
        }

        public async Task<IActionResult> TopPassengers()
        {
            var topPassengers = await this.customerService.GetTopTenPassengersAsync();
            var topPassengersView = topPassengers.Select(td => td.ToTopCustomerViewModel()).ToArray();

            return View(topPassengersView);
        }

        [Authorize]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
                
        public IActionResult PageNotFound()
        {
            return this.View();
        }
    }
}
