﻿using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace TangraTravel.MVC.Controllers.Abstract
{
    public abstract class BaseController : Controller
    {
        protected string GetUserEmail()
        {
            var email = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email).Value;

            return email;
        }

        protected int GetUserId()
        {
            int id = int.Parse(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value);

            return id;
        }

        protected string[] GetAvatarInformation()
        {
            string url = User.Claims.FirstOrDefault(x => x.Type == "Avatar").Value;
            string alt = User.Claims.FirstOrDefault(x => x.Type == "Alt").Value;

            return new string[] { url, alt };
        }

        protected async Task UpdateTokenAsync(string imageUrl, string alt, string role = null)
        {
            int userId = this.GetUserId();
            string fullName = User.Identity.Name;
            if (role == null)
            {
                role = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, this.GetUserEmail()),
                new Claim(ClaimTypes.Name, fullName),
                new Claim(ClaimTypes.NameIdentifier, this.GetUserId().ToString()),
                new Claim(ClaimTypes.Role, role),
                new Claim("Avatar",imageUrl),
                new Claim("Alt",alt)
            };

            var claimsIdentity = new ClaimsIdentity(claims, "Login");
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
        }

        protected string GetUserFullName()
        {
            string name = User.Identity.Name;

            return name;
        }
    }
}