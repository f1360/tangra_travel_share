﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using TangraTravel.MVC.Mapper;
using TangraTravel.Service.Contracts;
using TangraTravel.MVC.Models.CustomerDto;
using TangraTravel.Service.Exeception.User;
using TangraTravel.MVC.Controllers.Abstract;

namespace TangraTravel.MVC.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly ICustomerService customerService;
        private readonly IAddressService addressService;

        public CustomerController(ICustomerService customerService, IAddressService addressService)
        {
            this.customerService = customerService;
            this.addressService = addressService;
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public IActionResult Index(bool showCar)
        {
            var view = this.PrepareViewModel();
            view.Car = showCar;
            return View(view);
        }        

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> Update()
        {
            int userId = this.GetUserId();
            var view = await this.CustomerForUpdateAsync(userId);

            return this.View(view);
        }

        [HttpPost]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> Update(CustomerUpdateViewModel parameters)
        {
            int userId = this.GetUserId();
            if (!this.ModelState.IsValid)
            {
                var view = await this.CustomerForUpdateAsync(userId);
                return this.View(view);
            }

            var input = parameters.ToCustomerUpdateDto();
            try
            {
                var customer = await this.customerService.UpdateAsync(userId, input);
                var view = customer.ToCustomerUpdateViewModel();
                view.AddressInformation = await this.addressService.PrepareAddressInformationAsync();
                await this.UpdateTokenAsync(view.AvatarUrl, view.Alt);

                return this.RedirectToAction("Update", "Customer");
            }
            catch (InputExistException iee)
            {
                string message = iee.Message;
                if (message.Contains("Email"))
                {
                    this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.Email), message);
                }
                else if (message.Contains("Phone"))
                {
                    this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.PhoneNumber), message);
                }
                else if (message.Contains("Username"))
                {
                    this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.Username), message);
                }

                var view = await this.CustomerForUpdateAsync(userId);
                return this.View(view);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> ChangePassword(CustomerUpdateViewModel parameters)
        {
            int userId = this.GetUserId();
            if (parameters.Password != parameters.ConfirmPassword)
            {
                this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.Password), "The passwords are not the same!");
                this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.ConfirmPassword), "The passwords are not the same!");
                var view = await this.CustomerForUpdateAsync(userId);
                return this.View("Update", view);
            }

            var input = parameters.ToCustomerUpdateDto();
            var customer = await this.customerService.UpdateAsync(userId, input);
            var viewCustomer = customer.ToCustomerUpdateViewModel();
            viewCustomer.AddressInformation = await this.addressService.PrepareAddressInformationAsync();
            return this.View("Update", viewCustomer);
        }

        private CustomerIndexViewModel PrepareViewModel()
        {
            var view = new CustomerIndexViewModel();
            view.UserId = this.GetUserId();
            view.Email = this.GetUserEmail();
            view.FullName = this.GetUserFullName();
            string[] avatar = this.GetAvatarInformation();
            view.AvatarUrl = avatar[0];
            view.AvatarAlt = avatar[1];

            return view;
        }

        private async Task<CustomerUpdateViewModel> CustomerForUpdateAsync(int userId)
        {
            var customer = await this.customerService.GetCustomerAsync(userId);
            var view = customer.ToCustomerUpdateViewModel();
            view.AddressInformation = await this.addressService.PrepareAddressInformationAsync();

            return view;
        }
    }
}