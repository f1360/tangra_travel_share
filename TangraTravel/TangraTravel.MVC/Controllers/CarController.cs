﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using TangraTravel.MVC.Mapper;
using TangraTravel.MVC.Models;
using TangraTravel.MVC.Models.CarDto;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Exeception.Car;
using TangraTravel.MVC.Controllers.Abstract;

namespace TangraTravel.MVC.Controllers
{
    public class CarController : BaseController
    {
        private readonly ICarService carService;
        private readonly ICustomerService customerService;

        public CarController(ICarService carService, ICustomerService customerService)
        {
            this.carService = carService;
            this.customerService = customerService;
        }

        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> Index()
        {
            int driverId = this.GetUserId();
            var car = await this.carService.GetByUerIdAsync(driverId);

            return View(car);
        }

        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> Update()
        {
            int userId = this.GetUserId();
            var car = await this.carService.GetByUerIdAsync(userId);
            var view = car.ToCarViewModel();
            return this.View(view);
        }

        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> Update(CarViewModel input)
        {
            if (!this.ModelState.IsValid)
            {
                this.ModelState.AddModelError(nameof(input.RegistrationNumber), "Registration number is incorrect!");
                return this.View(input);
            }

            int userId = this.GetUserId();
            try
            {
                await this.carService.UpdateAsync(userId, new CarUpdateDto { RegistrationNumber = input.RegistrationNumber });
                return this.View(input);
            }
            catch (CarException ce)
            {
                this.ModelState.AddModelError(nameof(input.RegistrationNumber), ce.Message);
                return this.View(input);
            }
        }

        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> Delete()
        {
            int userId = this.GetUserId();
            try
            {
                await this.carService.DeleteAsync(userId);

                await this.PrepareToken(userId);
                return this.RedirectToAction("Index", "Customer");
            }
            catch (CarException ce)
            {
                var error = new ErrorViewModel() { ErrorMessage = ce.Message };
                return this.View("ErrorRemoveCar", error);
            }
        }

        [Authorize(Roles = "Passenger")]
        public async Task<IActionResult> Register()
        {
            var view = new CarRegisterViewModel();
            view.BasicCarInfromation = await this.carService.TakeBrandsAndModelsAsync(); 

            return this.View(view);
        }

        [HttpPost]
        [Authorize(Roles = "Passenger")]
        public async Task<IActionResult> Register(CarRegisterViewModel input)
        {
            if (!this.ModelState.IsValid || input.ModelId == 0)
            {
                input.BasicCarInfromation = await this.carService.TakeBrandsAndModelsAsync();
                return this.View(input);
            }

            int customerId = this.GetUserId();
            try
            {
                var parameters = new CarCreateDto()
                {
                    ModelId = input.ModelId,
                    Seats = input.Seats,
                    RegistrationNumber = input.RegistrationNumber
                };
                var car = await this.carService.CreateAsync(customerId, parameters);

                await this.PrepareToken(customerId);
                return this.RedirectToAction("Index", "Customer", new { showCar = true });
            }
            catch (CarException ce)
            {
                this.ModelState.AddModelError(nameof(CarRegisterViewModel.RegistrationNumber), ce.Message);
                input.BasicCarInfromation = await this.carService.TakeBrandsAndModelsAsync();

                return this.View(input);
            }
        }

        private async Task PrepareToken(int userId)
        {
            var customer = await this.customerService.GetCustomerAsync(userId);
            await this.UpdateTokenAsync(customer.Avatar.ImageDataURL, customer.Avatar.Alt, customer.SystemStatus);
        }
    }
}