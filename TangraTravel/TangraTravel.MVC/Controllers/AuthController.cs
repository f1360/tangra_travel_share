﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using TangraTravel.Data.Enum;
using TangraTravel.MVC.Mapper;
using TangraTravel.Data.Models;
using TangraTravel.Service.Contracts;
using TangraTravel.MVC.Models.LoginDto;
using TangraTravel.MVC.Helpers.Contracts;
using TangraTravel.MVC.Models.CustomerDto;
using TangraTravel.Service.Exeception.User;

namespace TangraTravel.MVC.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAuthHelper helper;
        private readonly IAvatarService avatar;
        private readonly ICustomerService customerService;
        private readonly IAddressService addressService;        

        public AuthController(IAuthHelper helper, IAvatarService avatar, ICustomerService customerService, IAddressService addressService)
        {
            this.helper = helper;
            this.avatar = avatar;
            this.customerService = customerService;
            this.addressService = addressService;            
        }

        public IActionResult Login()
        {
            return View();
        }

        public async Task<IActionResult> LoginConfirm(string access)
        {
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["access"]))
            {
                access = HttpContext.Request.Query["access"];

            }
            await this.customerService.ConfirmUser(access);

            return View();
        }

        public  IActionResult LoginUnconfirmed()
        {
            return View();
        }

        public async Task<IActionResult> Register()
        {
            var view = await this.CustomerForRegisterAsync();

            return this.View(view);
        }

        private async Task<CustomerUpdateViewModel> CustomerForRegisterAsync()
        {
            var view = new CustomerUpdateViewModel
            {
                AddressInformation = await this.addressService.PrepareAddressInformationAsync()
            };

            return view;
        }

        [HttpPost]
        public async Task<IActionResult> Register(CustomerUpdateViewModel parameters)
        {            
            if (!this.ModelState.IsValid)
            {
                var view = await this.CustomerForRegisterAsync();
                return this.View(view);
            }

            var input = parameters.ToCustomerCreateDto();
            try
            {
                var customer = await this.customerService.CreateAsync(input);

                return this.RedirectToAction("AfterRegistration", "Auth");
            }
            catch (InputExistException iee)
            {
                string message = iee.Message;
                if (message.Contains("Email"))
                {
                    this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.Email), message);
                }
                else if (message.Contains("Phone"))
                {
                    this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.PhoneNumber), message);
                }
                else if (message.Contains("Username"))
                {
                    this.ModelState.AddModelError(nameof(CustomerUpdateViewModel.Username), message);
                }

                var view = await this.CustomerForRegisterAsync();

                return this.View(view);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel input)
        {            
            if (!ModelState.IsValid)
            {
                return this.View(input);
            }

            try
            {
                var user = await this.helper.TryGetUserAsync(input.Email, input.Password);

                bool isAdmin = user.Role.Access == Access.Admin;

                var customer = user as Customer;

                if (!isAdmin && !customer.IsVerified)
                {
                    return this.RedirectToAction("LoginUnconfirmed", "Auth");
                }
                var avatar = await this.avatar.GetAvatarAsync(user.Id, isAdmin);
                
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, $"{user.FirstName} {user.LastName}"),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role.Access.ToString()),
                    new Claim("Avatar",avatar.ImageDataURL),
                    new Claim("Alt",avatar.Alt)
                };

                var claimsIdentity = new ClaimsIdentity(claims, "Login");
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return this.RedirectToAction("Index", "Home");
            }
            catch (EmailIncorrectException eie)
            {
                this.ModelState.AddModelError(nameof(input.Email), eie.Message);
                return this.View(input);
            }
            catch (PasswordIncorrectException pie)
            {
                this.ModelState.AddModelError(nameof(input.Password), pie.Message);
                return this.View(input);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return this.RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Permission()
        {
            return this.View();
        }

        [HttpGet]
        public IActionResult AfterRegistration()
        {
            return this.View();
        }
    }
}