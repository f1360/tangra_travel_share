﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using TangraTravel.MVC.Mapper;
using TangraTravel.MVC.Models.Admin;
using TangraTravel.MVC.Models.TravelDtos;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Models.CustomerDtos;

namespace TangraTravel.MVC.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly ITravelService travelService;
        private readonly ICustomerService custromerService;
        private readonly ICarService carService;
        private readonly IAddressService addressService;

        public AdminController(ITravelService travelService, ICustomerService custromerService, ICarService carService, IAddressService addressService)
        {
            this.travelService = travelService;
            this.custromerService = custromerService;
            this.carService = carService;
            this.addressService = addressService;
        }

        [HttpGet]
        public async Task<IActionResult> Customers(int page, int byPage)
        {
            var customers = await this.custromerService.GetCustomersByPageAsync(page);
            var maxPage = await this.custromerService.MaxPage(byPage);

            var view = new CustomerAdminPaginationViewModel(byPage)
            {
                Page = page,
                Customers = customers.Select(c => c.ToCustomerAdminDto()).ToArray(),
                MaxPage = maxPage
            };
            await PrepareCarBrandLogo(view);
            return this.View(view);
        }

        [HttpGet]
        public async Task<IActionResult> Cars(int page, int byPage)
        {
            var maxPage = await this.carService.MaxPage(byPage);
            var cars = await this.carService.GetByPageAsync(page, byPage);

            var view = new CarAdminViewModel(byPage)
            {
                Page = page,
                MaxPage = maxPage,
                Cars = cars.Select(x => x.ToCarViewModel()).ToArray()
            };

            return this.View(view);
        }

        [HttpGet]
        public async Task<IActionResult> ChangeStatusProfile(int customerId, bool blocking)
        {
            if (blocking)
            {
                await this.custromerService.BlockAsync(customerId);
            }
            else
            {
                await this.custromerService.UnblockAsync(customerId);
            }

            return this.RedirectToAction("Customer", new { customerId = customerId });
        }

        [HttpGet]
        public async Task<IActionResult> Customer(int customerId)
        {
            var customer = await this.custromerService.GetCustomerAsync(customerId);
            var view = customer.ToCustomerAdminDto();

            return this.View(view);
        }

        [HttpGet]
        public async Task<IActionResult> Search(CustomerSearchByDto inputs)
        {
            var result = await this.custromerService.SearchAsync(inputs);
            if (result == null)
            {
                return this.RedirectToAction("Customers", new { page = 1, byPage = 5 });
            }

            var customerView = result.ToCustomerAdminDto();
            return this.View("Customer", customerView);
        }
        private async Task PrepareCarBrandLogo(CustomerAdminPaginationViewModel view)
        {
            foreach (var customer in view.Customers)
            {
                customer.CarModel = "Empty";
                if (customer.Role == "Driver")
                {
                    var car = await this.carService.GetByUerIdAsync(customer.CustomerId);
                    customer.CarAvatar.ImageUrl = car.Avatar.ImageUrl;
                    customer.CarAvatar.Alt = car.Avatar.Alt;
                    customer.CarModel = car.Model;
                }
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Travels(int page)
        {
            var travels = await this.travelService.GetAllAdminTravelsAsync(page, 5);

            var view = new TravelAdminPaginationViewModel(5)
            {
                Page = page,
                MaxPage = await this.travelService.MaxPageForAllTravels(),
                Travels = travels.Select(t => t.ToTravelDriverViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> FilterAllTravels()
        {
            var view = new FilterAvailableViewModel
            {
                StartAddresses = await this.addressService.PrepareAddressInformationAsync(),
                EndAddresses = await this.addressService.PrepareAddressInformationAsync(),
                Drivers = await this.custromerService.PrepareDriversInformationAsync()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> FilterAllByStartAddress(FilterAvailableViewModel filterModel)
        {
            var travels = await this.travelService.GetAllTravelsFilteredByStartPoint(1, 10, filterModel.StartAddressId);

            var view = new TravelAdminPaginationViewModel(5)
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToTravelDriverViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> FilterAllByEndAddress(FilterAvailableViewModel filterModel)
        {
            var travels = await this.travelService.GetAllTravelsFilteredByEndPoint(1, 10, filterModel.EndAddressId);

            var view = new TravelAdminPaginationViewModel(5)
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToTravelDriverViewModel()).ToArray()
            };

            return View("FilterAllByStartAddress", view);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> FilterAllByDriver(FilterAvailableViewModel filterModel)
        {
            var travels = await this.travelService.GetAllTravelsFilteredByDriver(1, 10, filterModel.DriverId);

            var view = new TravelAdminPaginationViewModel(5)
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToTravelDriverViewModel()).ToArray()
            };

            return View("FilterAllByStartAddress", view);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult SortAllTravelsAdmin()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> SortAllTravels(SortViewModel sortModel)
        {
            var travels = await this.travelService.GetAllTravelsSorted(1, 10, sortModel.Criterion);

            var view = new TravelAdminPaginationViewModel(5)
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToTravelDriverViewModel()).ToArray()
            };

            return View(view);
        }
    }
}