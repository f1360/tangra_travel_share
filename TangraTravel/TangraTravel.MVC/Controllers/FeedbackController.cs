﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;

using TangraTravel.MVC.Mapper;
using TangraTravel.Service.Contracts;
using TangraTravel.MVC.Models.FeedbackDto;
using TangraTravel.MVC.Controllers.Abstract;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.Service.Models.FeedbackDtos;

namespace TangraTravel.MVC.Controllers
{
    public class FeedbackController : BaseController
    {
        private readonly IFeedbackService feedbackService;
        private readonly ICustomerService customerService;
        private readonly ITravelService travelService;

        public FeedbackController(IFeedbackService feedbackService, ICustomerService customerService, ITravelService travelService)
        {
            this.feedbackService = feedbackService;
            this.customerService = customerService;
            this.travelService = travelService;
        }

        [Authorize]
        public async Task<IActionResult> Index(int page, int byPage)
        {
            var customers = await this.customerService.GetCustomersByPageAsync(page, byPage);
            var view = new FeedbackIndexByPageViewModel(byPage)
            {
                Page = page,
                MaxPage = await this.customerService.MaxPage(byPage),
                FeedbackCustomers = customers.Select(x => x.ToFeedbackCustomerViewModel()).ToArray()
            };
            return View(view);
        }

        [Authorize]
        public async Task<IActionResult> Selected(int userId, int page)
        {
            int byPage = 3;
            int maxPage = await this.feedbackService.MaxPageAsync(userId, byPage, IsGiven: false);
            var feedbacks = await this.feedbackService.ReceivedFeedback(userId, page, byPage);

            var view = new FeedbackSelectedByPageViewModel()
            {
                SelectedUserId = userId,
                Page = page,
                MaxPage = maxPage,
                FeedbackComments = feedbacks.Select(x => x.ToFeedbackCommentViewModel()).ToArray()
            };

            return this.View(view);
        }

        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FromMe(int page)
        {
            int byPage = 3;
            int customerId = this.GetUserId();
            int maxPage = await this.feedbackService.MaxPageAsync(customerId, byPage, IsGiven: true);
            var feedbacks = await this.feedbackService.GivenFeedback(customerId, page, byPage);

            var view = new FeedbackByPageViewModel()
            {
                Page = page,
                MaxPage = maxPage,
                FeedbackComments = feedbacks.Select(x => x.ToFeedbackCommentViewModel()).ToArray()
            };

            return this.View(view);
        }

        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> ThowardsMe(int page)
        {
            int byPage = 3;
            int customerId = this.GetUserId();
            int maxPage = await this.feedbackService.MaxPageAsync(customerId, byPage, IsGiven: false);
            var feedbacks = await this.feedbackService.ReceivedFeedback(customerId, page, byPage);

            var view = new FeedbackByPageViewModel()
            {
                Page = page,
                MaxPage = maxPage,
                FeedbackComments = feedbacks.Select(x => x.ToFeedbackCommentViewModel()).ToArray()
            };

            return this.View(view);
        }

        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> ApplyComment(int page)
        {
            int userId = this.GetUserId();
            var travelInformation = await this.travelService.RetriveCompletedTravelsAsync(userId, page);
            var view = new FeedbackApplyViewModel
            {
                Page = page,
                Travels = travelInformation
            };

            return this.View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> DriverFeedback(int travelId)
        {
            var passengers = await this.travelService.RetrivePassengerInFinishedTravelsAsync(travelId);
            if (passengers.Count == 0)
            {
                this.RedirectToAction("ApplyComment", 1);
            }

            var selectList = new SelectList(passengers, "TargetId", "TargetInformation");
            var view = new FeedbackViewModel
            {
                TravelId = travelId,
                Targets = selectList
            };

            return this.View(view);
        }

        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> DriverFeedback(int travelId, FeedbackViewModel inputs)
        {
            IList<TravelCustomer> passengers = null;
            SelectList selectList = null;
            if (!ModelState.IsValid)
            {
                passengers = await this.travelService.RetrivePassengerInFinishedTravelsAsync(travelId);
                selectList = new SelectList(passengers, "PassengerId", "PassengerInformation");
                inputs.TravelId = travelId;
                inputs.Targets = selectList;
                return this.View(inputs);
            }

            int driverId = this.GetUserId();
            var feedback = new FeedbackCreateDto
            {
                Rating = inputs.Rating,
                Comment = inputs.Comment,
                TravelId = travelId,
                TargetId = inputs.TargetId
            };
            try
            {
                var result = await this.feedbackService.DriverWriteFeedbackAsync(driverId, feedback);

                passengers = await this.travelService.RetrivePassengerInFinishedTravelsAsync(travelId);
                if (passengers.Count == 0)
                {
                    return this.RedirectToAction("ApplyComment", 1);
                }
                selectList = new SelectList(passengers, "PassengerId", "PassengerInformation");
                var view = new FeedbackViewModel
                {
                    TravelId = travelId,
                    Targets = selectList
                };
                return this.View(view);
            }
            catch (ApplicationException ae)
            {
                return this.RedirectToAction("Index", "Customer");
            }
        }

        [HttpGet]
        [Authorize(Roles = "Passenger")]
        public async Task<IActionResult> PassengerFeedback(int travelId)
        {
            int userId = this.GetUserId();
            var driver = await this.travelService.RetriveDriverInFinishedTravelAsync(travelId, userId);
            if (driver == null)
            {
                return this.RedirectToAction("ApplyComment", 1);
            }

            var selectList = new SelectList(new List<TravelCustomer> { driver }, "TargetId", "TargetInformation");
            var view = new FeedbackViewModel
            {
                TravelId = travelId,
                Targets = selectList
            };

            return this.View(view);
        }

        [HttpPost]
        [Authorize(Roles = "Passenger")]
        public async Task<IActionResult> PassengerFeedback(int travelId, FeedbackViewModel inputs)
        {
            int passengerId = this.GetUserId();
            TravelCustomer driver = null;
            SelectList selectList = null;
            if (!ModelState.IsValid)
            {
                driver = await this.travelService.RetriveDriverInFinishedTravelAsync(travelId, passengerId);
                selectList = new SelectList(new List<TravelCustomer> { driver }, "TargetId", "TargetInformation");
                inputs.TravelId = travelId;
                inputs.Targets = selectList;
                return this.View(inputs);
            }
           
            var feedback = new FeedbackCreateDto
            {
                Rating = inputs.Rating,
                Comment = inputs.Comment,
                TravelId = travelId,
                TargetId = inputs.TargetId
            };
            try
            {
                var result = await this.feedbackService.PassengerWriteFeedbackAsync(passengerId, feedback);

                return this.RedirectToAction("ApplyComment", 1);
            }
            catch (ApplicationException)
            {
                return this.RedirectToAction("Index", "Customer");
            }
        }

        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> GetFeedback(int feedbackId)
        {
            var view = await this.feedbackService.GetFeedbackAsync(feedbackId);

            return this.View(view);
        }
    }
}