﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using TangraTravel.MVC.Mapper;
using TangraTravel.Service.Contracts;
using TangraTravel.MVC.Models.TravelDtos;
using TangraTravel.MVC.Controllers.Abstract;

namespace TangraTravel.MVC.Controllers
{
    public class TravelController : BaseController
    {
        private readonly ITravelService travelService;
        private readonly ICustomerService customerService;
        private readonly IAddressService addressService;

        public TravelController(ITravelService travelService, ICustomerService customerService, IAddressService addressService)
        {
            this.travelService = travelService;
            this.customerService = customerService;
            this.addressService = addressService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index(int page)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllAvaliableTravels(userId, page, 5);

            var view = new TravelsByPageViewModel
            {
                Page = page,
                MaxPage = await this.travelService.MaxPageForAvailableTravels(userId),
                Travels = travels.Select(t => t.ToTravelMainViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> AllAsDriver(int page)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllDriverTravels(userId, page, 5);

            var view = new TravelsByPageViewModel
            {
                Page = page,
                MaxPage = await this.travelService.MaxPageForDriverTravels(userId),
                Travels = travels.Select(t => t.ToTravelDriverViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> AllAsPassenger(int page)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllPassengerTravels(userId, page, 5);

            var view = new TravelsByPageViewModel
            {
                Page = page,
                MaxPage = await this.travelService.MaxPageForPassengerTravels(userId),
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> PassengerDetails(int travelId)
        {
            var travel = await this.travelService.GetTravelAsync(travelId);

            var view = travel.ToTravelPassengerDetailsViewModel();

            return View(view);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> DriverDetails(int travelId)
        {
            var travel = await this.travelService.GetTravelAsync(travelId);

            var view = travel.ToTravelPassengerDetailsViewModel();

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> ApplyForTravel(int travelId)
        {
            int customerId = this.GetUserId();
            var customer = await this.customerService.GetCustomerAsync(customerId);
            if (customer.StatusProfile == "Blocked")
            {
                return this.RedirectToAction("Index", "Home");
            }

            var travel = await this.travelService.GetTravelAsync(travelId);

            var view = travel.ToTravelPassengerDetailsViewModel();

            return View(view);
        }

        [HttpPost]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> ApplyForTravel(TravelPassengerDetailsViewModel parameters)
        {
            int userId = this.GetUserId();
            var customer = await this.customerService.GetCustomerAsync(userId);
            if (customer.StatusProfile == "Blocked")
            {
                return this.RedirectToAction("Index", "Home");
            }

            var travelId = parameters.TravelId;

            await this.customerService.ApplyForTravelAsync(userId, travelId, Data.Enum.Access.Passenger);

            return this.RedirectToAction("Index", "Travel");
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CancelTravel(TravelPassengerDetailsViewModel parameters)
        {
            int userId = this.GetUserId();

            var travelId = parameters.TravelId;

            await this.customerService.CancelTravel(travelId, userId);

            var travels = await this.travelService.GetAllPassengerTravels(userId, 1, 5);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = await this.travelService.MaxPageForPassengerTravels(userId),
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View("AllAsPassenger", view);
        }

        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> CompleteTravel(TravelPassengerDetailsViewModel parameters)
        {
            await this.travelService.ChangeTravelStatusAsync(parameters.TravelId, "Completed");

            return this.RedirectToAction("DriverDetails", "Travel", new { travelId = parameters.TravelId });
        }

        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> MarkAsTravelling(TravelPassengerDetailsViewModel parameters)
        {
            await this.travelService.ChangeTravelStatusAsync(parameters.TravelId, "Travelling");

            return this.RedirectToAction("DriverDetails", "Travel", new { travelId = parameters.TravelId });
        }

        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> DeleteTravel(TravelPassengerDetailsViewModel parameters)
        {
            int userId = this.GetUserId();

            var travelId = parameters.TravelId;

            await this.travelService.DeleteTravelAsync(travelId);

            var travels = await this.travelService.GetAllDriverTravels(userId, 1, 5);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = await this.travelService.MaxPageForDriverTravels(userId),
                Travels = travels.Select(t => t.ToTravelDriverViewModel()).ToArray()
            };

            return View("AllAsDriver", view);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> TravelPassengerDetails(int travelPassId)
        {
            var travelPass = await this.customerService.GetApplicationAsync(travelPassId);

            var view = travelPass.ToApplicantStatusViewModel();

            return View(view);
        }

        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> UpdateApplyStatus(ApplicantStatusViewModel parameters)
        {
            var input = parameters.ToTravelPassengerApplyStatusDto();

            await this.customerService.ProcessApplicationAsync(parameters.TravelPassengerId, input);

            return this.RedirectToAction("DriverDetails", "Travel", new { travelId = parameters.TravelId });
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> CreateTravel()
        {
            int customerId = this.GetUserId();
            var customer = await this.customerService.GetCustomerAsync(customerId);
            if (customer.StatusProfile == "Blocked")
            {
                return this.RedirectToAction("Index", "Home");
            }

            var view = await this.TravelForCreateAsync();

            return this.View(view);
        }


        [HttpPost]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> CreateTravel(CreateTravelViewModel parameters)
        {
            int customerId = this.GetUserId();
            var customer = await this.customerService.GetCustomerAsync(customerId);
            if (customer.StatusProfile == "Blocked")
            {
                return this.RedirectToAction("Index", "Home");
            }

            if (!this.ModelState.IsValid)
            {
                return this.RedirectToAction(nameof(this.Index));
            }

            var input = parameters.ToTravelCreateDto();

            input.DriverId = this.GetUserId();
            var travel = await this.travelService.CreateTravelAsync(input);

            return this.RedirectToAction("AllAsDriver", "Travel", new { page = 1 });
        }

        private async Task<CreateTravelViewModel> TravelForCreateAsync()
        {
            var view = new CreateTravelViewModel
            {
                Addresses = await this.addressService.PrepareAddressInformationAsync()
            };

            return view;
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterAvailableTravels()
        {
            var view = new FilterAvailableViewModel
            {
                StartAddresses = await this.addressService.PrepareAddressInformationAsync(),
                EndAddresses = await this.addressService.PrepareAddressInformationAsync(),
                Drivers = await this.customerService.PrepareDriversInformationAsync()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterByStartAddress(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllAvaliableTravelsFilteredByStartPoint(userId, 1, 10, filterModel.StartAddressId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterByEndAddress(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllAvaliableTravelsFilteredByEndPoint(userId, 1, 10, filterModel.EndAddressId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View("FilterByStartAddress", view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterByDriver(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllAvaliableTravelsFilteredByDriver(userId, 1, 10, filterModel.DriverId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View("FilterByStartAddress", view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public IActionResult SortAvailableTravels()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> SortTravels(SortViewModel sortModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllAvaliableTravelsSorted(userId, 1, 10, sortModel.Criterion);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterPassengerTravels()
        {
            var view = new FilterAvailableViewModel
            {
                StartAddresses = await this.addressService.PrepareAddressInformationAsync(),
                EndAddresses = await this.addressService.PrepareAddressInformationAsync(),
                Drivers = await this.customerService.PrepareDriversInformationAsync()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterByStartAddressPass(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllPassengerTravelsFilteredByStartPoint(userId, 1, 10, filterModel.StartAddressId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterByEndAddressPass(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllPassengerTravelsFilteredByEndPoint(userId, 1, 10, filterModel.EndAddressId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View("FilterByStartAddressPass", view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> FilterByDriverPass(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllPassengerTravelsFilteredByDriver(userId, 1, 10, filterModel.DriverId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View("FilterByStartAddressPass", view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public IActionResult SortPassengerTravels()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Driver, Passenger")]
        public async Task<IActionResult> SortTravelsPassenger(SortViewModel sortModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllPassengerTravelsSorted(userId, 1, 10, sortModel.Criterion);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> FilterDriverTravels()
        {
            var view = new FilterAvailableViewModel
            {
                StartAddresses = await this.addressService.PrepareAddressInformationAsync(),
                EndAddresses = await this.addressService.PrepareAddressInformationAsync()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> FilterByStartAddressDriver(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllDriverTravelsFilteredByStartPoint(userId, 1, 10, filterModel.StartAddressId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View(view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> FilterByEndAddressDriver(FilterAvailableViewModel filterModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllDriverTravelsFilteredByEndPoint(userId, 1, 10, filterModel.EndAddressId);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View("FilterByStartAddressDriver", view);
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public IActionResult SortDriverTravels()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Driver")]
        public async Task<IActionResult> SortTravelsDriver(SortViewModel sortModel)
        {
            int userId = this.GetUserId();

            var travels = await this.travelService.GetAllDriverTravelsSorted(userId, 1, 10, sortModel.Criterion);

            var view = new TravelsByPageViewModel
            {
                Page = 1,
                MaxPage = 1,
                Travels = travels.Select(t => t.ToMyPassengerTravelViewModel()).ToArray()
            };

            return View(view);
        }
    }
}