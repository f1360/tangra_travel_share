using System;

using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;

using TangraTravel.Data.DataBase;
using TangraTravel.MVC.Extention;
using TangraTravel.Service.Models.Email;

namespace TangraTravel.MVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TangraTravelContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.LoginPath = "/Auth/Login";
                options.AccessDeniedPath = "/Auth/Permission";

                options.Cookie.Name = "Tangra_Travel_Token";
                options.SlidingExpiration = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(20);

                // Link: https://www.yogihosting.com/aspnet-core-cookie-authentication/
                // Link: https://docs.microsoft.com/en-us/archive/msdn-magazine/2017/september/cutting-edge-cookies-claims-and-authentication-in-asp-net-core
                // Link: https://www.kaspersky.com/resource-center/definitions/cookies
                // Link: https://stackoverflow.com/questions/38360045/why-do-i-get-user-identity-isauthenticated-false - possible failure for Authorization
            });

            services.Configure<EmailServiceOptions>(Configuration
                .GetSection(nameof(EmailServiceOptions)));

            services.ContainerIoC(this.Configuration);

            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                
                app.UseHsts();
            }
            app.UseStatusCodePagesWithReExecute("/Home/PageNotFound");

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}

// Link(Redirect to Action): https://www.codegrepper.com/code-examples/whatever/redirecttoaction+with+id
// Link(Icons): https://www.codegrepper.com/code-examples/whatever/insert+image+in+top+navigation+bar+html