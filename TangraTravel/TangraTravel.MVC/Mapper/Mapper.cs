﻿using TangraTravel.MVC.Models.Admin;
using TangraTravel.MVC.Models.CarDto;
using TangraTravel.MVC.Models.TravelDtos;
using TangraTravel.MVC.Models.CustomerDto;
using TangraTravel.MVC.Models.FeedbackDto;
using TangraTravel.Service.Models.CarDtos;
using TangraTravel.Service.Models.TravelDtos;
using TangraTravel.Service.Models.CustomerDtos;
using TangraTravel.Service.Models.FeedbackDtos;
using TangraTravel.Service.Models.TravelPassengersDtos;

namespace TangraTravel.MVC.Mapper
{
    public static class Mapper
    {
        public static CustomerUpdateViewModel ToCustomerUpdateViewModel(this CustomerReturnDto input)
        {
            var model = new CustomerUpdateViewModel
            {
                FirstName = input.FirstName,
                LastName = input.LastName,
                Gender = input.Gender,
                Email = input.Email,
                AddressId = input.Address.AddressId,
                PhoneNumber = input.PhoneNumber,
                Username = input.Username,
                AvatarId = input.Avatar.AvatarId,
                AvatarUrl = input.Avatar.ImageDataURL,
                Alt = input.Avatar.Alt

            };
            return model;
        }

        public static CustomerUpdateDto ToCustomerUpdateDto(this CustomerUpdateViewModel model)
        {
            var input = new CustomerUpdateDto
            {
                Username = model.Username,
                NewEmail = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                AddressId = model.AddressId,
                PhoneNumber = model.PhoneNumber,
                Avatar = model.File,
                Password = model.Password,
            };

            return input;
        }

        public static CustomerCreateDto ToCustomerCreateDto(this CustomerUpdateViewModel model)
        {
            var input = new CustomerCreateDto
            {
                Username = model.Username,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                AddressId = (int)model.AddressId,
                PhoneNumber = model.PhoneNumber,
                Password = model.Password
            };

            return input;
        }

        public static FeedbackCustomerViewModel ToFeedbackCustomerViewModel(this CustomerReturnDto customer)
        {
            var view = new FeedbackCustomerViewModel
            {
                AvatarUrl = customer.Avatar.ImageDataURL,
                FullName = customer.FullName,
                Rating = customer.Rating,
                UserName = customer.Username,
                CustomerId = customer.CustomerId,
                Role = customer.SystemStatus
            };

            return view;
        }

        public static FeedbackCommentViewModel ToFeedbackCommentViewModel(this FeedbackReturnDto feedback)
        {
            var view = new FeedbackCommentViewModel
            {
                Author = feedback.Author,
                AuthorUsername = feedback.AuthorUsername,
                Recepient = feedback.Target,
                RecepientUsername = feedback.TargetUsername,
                Rating = feedback.Rating,
                Comment = feedback.Comment
            };

            return view;
        }

        public static TopCustomerViewModel ToTopCustomerViewModel(this CustomerReturnDto customer)
        {
            var view = new TopCustomerViewModel
            {
                AvatarUrl = customer.Avatar.ImageDataURL,
                FullName = customer.FullName,
                Rating = customer.Rating,
                UserName = customer.Username
            };

            return view;
        }

        public static TravelMainViewModel ToTravelMainViewModel(this TravelPassengerDetailsDto travel)
        {
            var view = new TravelMainViewModel
            {
                TravelId = travel.TravelId,
                StartAddress = $"{travel.StartAddress.City},  {travel.StartAddress.Country}",
                EndAddress = $"{travel.EndAddress.City},  {travel.EndAddress.Country}",
                DepartureTime = travel.DepartureTime,
                FreeSeats = travel.FreeSeats,
                Comment = travel.Comment.Comment,
                DriverName = travel.Driver.DriverName,
                Status = travel.Status,
                PassengerStatus = travel.PassengerStatus
            };

            return view;
        }

        public static TravelMainViewModel ToTravelDriverViewModel(this TravelPresentDto travel)
        {
            var view = new TravelMainViewModel
            {
                TravelId = travel.TravelId,
                StartAddress = $"{travel.StartAddress.City},  {travel.StartAddress.Country}",
                EndAddress = $"{travel.EndAddress.City},  {travel.EndAddress.Country}",
                DepartureTime = travel.DepartureTime,
                FreeSeats = travel.FreeSeats,
                Comment = travel.Comment.Comment,
                DriverName = travel.Driver.DriverName,
                Status = travel.Status
            };

            return view;
        }

        public static TravelMainViewModel ToMyPassengerTravelViewModel(this TravelPassengerDetailsDto travel)
        {
            var view = new TravelMainViewModel
            {
                TravelId = travel.TravelId,
                StartAddress = $"{travel.StartAddress.City},  {travel.StartAddress.Country}",
                EndAddress = $"{travel.EndAddress.City},  {travel.EndAddress.Country}",
                DepartureTime = travel.DepartureTime,
                FreeSeats = travel.FreeSeats,
                Comment = travel.Comment.Comment,
                DriverName = travel.Driver.DriverName,
                Status = travel.Status,
                PassengerStatus = travel.PassengerStatus
            };

            return view;
        }

        public static CarViewModel ToCarViewModel(this CarReturnDto car)
        {
            var owner = car.Owner.Split(", ")[0].Replace("Full Name: ", "");
            var view = new CarViewModel
            {
                Owner = owner,
                Brand = car.Brand,
                Model = car.Model,
                RegistrationNumber = car.RegistrationNumber,
                Seats = car.Seats,
                Avatar = car.Avatar
            };

            return view;
        }

        public static CustomerAdminDto ToCustomerAdminDto(this CustomerReturnDto customer)
        {
            var view = new CustomerAdminDto()
            {
                CustomerId = customer.CustomerId,
                FullName = customer.FullName,
                Email = customer.Email,
                Username = customer.Username,
                PhoneNumber = customer.PhoneNumber,
                Role = customer.SystemStatus,
                StatusProfile = customer.StatusProfile == "Blocked" ? true : false,
                Address = $"{customer.Address.StreetAddress}, {customer.Address.City}",
                AvatarUrl = customer.Avatar.ImageDataURL
            };

            return view;
        }

        public static TravelPassengerDetailsViewModel ToTravelPassengerDetailsViewModel(this TravelPresentDto travel)
        {
            var view = new TravelPassengerDetailsViewModel
            {
                TravelId = travel.TravelId,
                StartAddress = $"{travel.StartAddress.City},  {travel.StartAddress.Country}",
                EndAddress = $"{travel.EndAddress.City},  {travel.EndAddress.Country}",
                DepartureTime = travel.DepartureTime,
                FreeSeats = travel.FreeSeats,
                Comment = travel.Comment.Comment,
                DriverName = travel.Driver.DriverName,
                Status = travel.Status,
                CarRegNumber = travel.Driver.CarDetails.RegistrationNumber,
                CarBrand = travel.Driver.CarDetails.Brand,
                CarModel = travel.Driver.CarDetails.Model
            };

            return view;
        }

        public static ApplicantStatusViewModel ToApplicantStatusViewModel(this TravelPassengerPresentDto travelPassenger)
        {
            var view = new ApplicantStatusViewModel
            {
                TravelId = travelPassenger.TravelId,
                TravelPassengerId = travelPassenger.TravelPassengerId,
                FullName = travelPassenger.PassengerName,
                ApplyStatus = travelPassenger.ApplyStatus
            };

            return view;
        }

        public static TravelPassengerApplyStatusDto ToTravelPassengerApplyStatusDto(this ApplicantStatusViewModel applicant)
        {
            var view = new TravelPassengerApplyStatusDto
            {
                Criteria = applicant.ApplyStatus
            };

            return view;
        }

        public static TravelCreateDto ToTravelCreateDto(this CreateTravelViewModel model)
        {
            var input = new TravelCreateDto
            {
                StartAddressId = model.StartAddressId,
                EndAddressId = model.EndAddressId,
                DepartureTime = model.DepartureTime,
                Comment = model.Comment
            };

            return input;
        }
    }
}