﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using CloudinaryDotNet;

using TangraTravel.MVC.Helpers;
using TangraTravel.Service.Mapper;
using TangraTravel.Service.Service;
using TangraTravel.Service.Contracts;
using TangraTravel.Service.Models.Cloud;
using TangraTravel.MVC.Helpers.Contracts;
using TangraTravel.Service.Mapper.Contracts;
using TangraTravel.Service.Models.Provider;

namespace TangraTravel.MVC.Extention
{
    public static class ServiceExtention
    {
        public static void ContainerIoC(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(Cloud(configuration));
            services.AddSingleton<ICloud, Cloud>();
            services.AddScoped<IProvider, CustomProvider>();
            services.AddScoped<IUserAuthService, UserAuthService>();
            services.AddScoped<IAuthHelper, AuthHelper>();
            services.AddScoped<IAvatarService, AvatarService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICustomExpression, CustomExpression>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IFeedbackService, FeedbackService>();
            services.AddScoped<ITravelService, TravelService>();
            services.AddScoped<ICarService, CarService>();
            services.AddScoped<IEmailService, EmailService>();
        }

        private static Cloudinary Cloud(IConfiguration configuration)
            => new Cloudinary(
                new Account(
                    configuration.GetSection("Cloudinary").GetValue<string>("cloudName"),
                    configuration.GetSection("Cloudinary").GetValue<string>("apyKey"),
                    configuration.GetSection("Cloudinary").GetValue<string>("apiSecret")
                    ));

    }
}