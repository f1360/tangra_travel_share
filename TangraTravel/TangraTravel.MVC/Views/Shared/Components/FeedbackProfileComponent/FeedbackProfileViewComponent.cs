﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TangraTravel.MVC.Models.Component;
using TangraTravel.Service.Contracts;

namespace TangraTravel.MVC.Views.Shared.Components.FeedbackProfileComponent
{
    public class FeedbackProfileViewComponent : ViewComponent
    {
        private readonly IFeedbackService feedbackService;

        public FeedbackProfileViewComponent(IFeedbackService feedbackService)
        {
            this.feedbackService = feedbackService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int userId)
        {
            var result = new FeedbackProfileViewModel()
            {
                Bests = await this.feedbackService.GivenSortedFeedbackAsync(userId, 5, best: true),
                Bads = await this.feedbackService.GivenSortedFeedbackAsync(userId, 5, best: false)
            };

            return View("FeedbackProfile", result);
        }
    }
}