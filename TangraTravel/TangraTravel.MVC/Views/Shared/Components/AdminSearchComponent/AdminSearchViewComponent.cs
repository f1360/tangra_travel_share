﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TangraTravel.Service.Models.CustomerDtos;

namespace TangraTravel.MVC.Views.Shared.Components.AdminSearchComponent
{
    public class AdminSearchViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = new CustomerSearchByDto();

            return View("AdminSearch", result);
        }
    }
}