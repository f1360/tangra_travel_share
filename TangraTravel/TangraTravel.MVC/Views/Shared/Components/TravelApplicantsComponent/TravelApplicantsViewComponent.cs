﻿using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TangraTravel.Service.Contracts;
using TangraTravel.MVC.Mapper;

namespace TangraTravel.MVC.Views.Shared.Components.TravelApplicantsViewComponent
{
    public class TravelApplicantsViewComponent : ViewComponent
    {
        private readonly ICustomerService customerService;

        public TravelApplicantsViewComponent(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int travelId)
        {
            var applicants = await this.customerService.ListApplicationsAsync(travelId);
            var result = applicants.Select(tp => tp.ToApplicantStatusViewModel()).ToArray();

            return View("TravelApplicants", result);
        }
    }
}