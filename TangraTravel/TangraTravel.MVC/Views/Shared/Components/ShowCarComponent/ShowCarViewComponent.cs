﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TangraTravel.MVC.Mapper;
using TangraTravel.Service.Contracts;

namespace TangraTravel.MVC.Views.Shared.Components.ShowCarComponent
{
    public class ShowCarViewComponent : ViewComponent
    {
        private readonly ICarService carService;

        public ShowCarViewComponent(ICarService carService)
        {
            this.carService = carService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int userId)
        {
            var car = await carService.GetByUerIdAsync(userId);
            var view = car.ToCarViewModel();

            return View("ShowCar", view);
        }
    }
}