﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TangraTravel.Service.Contracts;

namespace TangraTravel.MVC.Views.Shared.Components.NearestTravelsComponent
{
    public class NearestTravelsViewComponent : ViewComponent
    {
        private readonly ITravelService travelService;

        public NearestTravelsViewComponent(ITravelService travelService)
        {
            this.travelService = travelService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int userId)
        {
            var result = await this.travelService.GetSoonTravelsAsync(userId, 3);

            return View("TravelComponent", result);
        }
    }
}