﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.Abstract;
using TangraTravel.Service.Models.CustomerDtos;

namespace TangraTravel.MVC.Models.Admin
{
    public class CustomerAdminPaginationViewModel : Pagination
    {
        public CustomerAdminPaginationViewModel(int byPage)
            :base(byPage)
        {
        }

        public ICollection<CustomerAdminDto> Customers { get; set; }
    }
}