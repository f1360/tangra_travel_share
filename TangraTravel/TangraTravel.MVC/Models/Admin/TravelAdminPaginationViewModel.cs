﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.Abstract;
using TangraTravel.MVC.Models.TravelDtos;

namespace TangraTravel.MVC.Models.Admin
{
    public class TravelAdminPaginationViewModel : Pagination
    {
        public TravelAdminPaginationViewModel(int byPage)
            :base(byPage)
        {
        }

        public ICollection<TravelMainViewModel> Travels { get; set; }
    }
}