﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.CarDto;
using TangraTravel.MVC.Models.Abstract;

namespace TangraTravel.MVC.Models.Admin
{
    public class CarAdminViewModel : Pagination
    {
        public CarAdminViewModel(int byPage)
            : base(byPage)
        {
        }
        public ICollection<CarViewModel> Cars { get; set; }
    }
}