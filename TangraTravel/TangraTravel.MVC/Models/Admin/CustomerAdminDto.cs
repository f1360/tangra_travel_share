﻿using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.MVC.Models.Admin
{
    public class CustomerAdminDto
    {
        public CustomerAdminDto()
        {
            this.CarAvatar = new CarAvatarDto();
        }

        public int CustomerId { get; set; }
        
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string PhoneNumber { get; set; }

        public string Role { get; set; }

        public bool StatusProfile { get; set; }

        public string Address { get; set; }

        public string AvatarUrl { get; set; }

        public string CarModel { get; set; }
        public CarAvatarDto CarAvatar { get; set; }
    }
}