﻿using System.ComponentModel.DataAnnotations;

namespace TangraTravel.MVC.Models.LoginDto
{
    public class LoginViewModel
    {
        [Required, MaxLength(100, ErrorMessage = "Email length must be {1}!"), EmailAddress(ErrorMessage = "Invalid value for {0}")]
        public string Email { get; set; }

        [Required, MinLength(8, ErrorMessage = "Password length must be at least {1} characters!")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Password is invalid!")]
        public string Password { get; set; }
    }
}