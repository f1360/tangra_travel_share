﻿namespace TangraTravel.MVC.Models.Abstract
{
    public abstract class Pagination : IPagination
    {
        protected Pagination(int byPage)
        {
            this.ByPage = byPage;
        }

        public int Page { get; set; }

        public int ByPage { get; set; }

        public int MaxPage { get; set; }
    }
}