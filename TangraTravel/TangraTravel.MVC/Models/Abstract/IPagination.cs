﻿namespace TangraTravel.MVC.Models.Abstract
{
    public interface IPagination
    {
        public int Page { get; set; }

        public int ByPage { get; set; }

        public int MaxPage { get; set; }
    }
}