﻿using Microsoft.AspNetCore.Http;

namespace TangraTravel.MVC.Models.Avatar
{
    public class AvatarViewModel
    {
        public int? AvatarId { get; set; }

        public string AvatarUrl { get; set; }

        public string Alt { get; set; }

        public IFormFile File { get; set; }
    }
}