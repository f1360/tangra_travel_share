﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.Abstract;

namespace TangraTravel.MVC.Models.FeedbackDto
{
    public class FeedbackSelectedByPageViewModel : Pagination
    {
        public FeedbackSelectedByPageViewModel()
            : base(byPage: 3)
        {
        }

        public int SelectedUserId { get; set; }

        public ICollection<FeedbackCommentViewModel> FeedbackComments { get; set; }
    }
}