﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.Abstract;

namespace TangraTravel.MVC.Models.FeedbackDto
{
    public class FeedbackByPageViewModel : Pagination
    {
        public FeedbackByPageViewModel()
            : base(byPage: 3)
        {
        }

        public ICollection<FeedbackCommentViewModel> FeedbackComments { get; set; }
    }
}