﻿namespace TangraTravel.MVC.Models.FeedbackDto
{
    public class FeedbackCustomerViewModel
    {
        public int CustomerId { get; set; }

        public string AvatarUrl { get; set; }

        public string FullName { get; set; }

        public double Rating { get; set; }

        public string UserName { get; set; }

        public string Role { get; set; }

        public string Feedback { get; set; }
    }
}