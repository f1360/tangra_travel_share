﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.Abstract;

namespace TangraTravel.MVC.Models.FeedbackDto
{
    public class FeedbackIndexByPageViewModel : Pagination
    {
        public FeedbackIndexByPageViewModel(int byPage)
            : base(byPage)
        {
        }

        public ICollection<FeedbackCustomerViewModel> FeedbackCustomers { get; set; }
    }
}