﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.Abstract;
using TangraTravel.Service.Models.TravelDtos;

namespace TangraTravel.MVC.Models.FeedbackDto
{
    public class FeedbackApplyViewModel : Pagination
    {
        public FeedbackApplyViewModel()
            : base(byPage: 10)
        {

        }
        public IList<TravelFeedbackDto> Travels { get; set; }
    }
}