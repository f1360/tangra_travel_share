﻿using System.ComponentModel.DataAnnotations;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace TangraTravel.MVC.Models.FeedbackDto
{
    public class FeedbackViewModel
    {
        public int TravelId { get; set; }

        public int TargetId { get; set; }
        public SelectList Targets { get; set; }

        [Range(0, 5, ErrorMessage = "{0} value can be between {2} to {1}")]
        public int Rating { get; set; }

        [MaxLength(255, ErrorMessage = "{0} value must be between and {1} symbols!")]
        public string Comment { get; set; }
    }
}