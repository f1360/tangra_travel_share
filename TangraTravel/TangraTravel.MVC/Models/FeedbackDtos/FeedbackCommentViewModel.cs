﻿namespace TangraTravel.MVC.Models.FeedbackDto
{
    public class FeedbackCommentViewModel
    {
        public string Author { get; set; }

        public string AuthorUsername { get; set; }

        public string Recepient { get; set; }

        public string RecepientUsername { get; set; }

        public double Rating { get; set; }

        public string Comment { get; set; }
    }
}