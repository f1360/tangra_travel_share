﻿using System.Collections.Generic;

using TangraTravel.Service.Models.FeedbackDtos;

namespace TangraTravel.MVC.Models.Component
{
    public class FeedbackProfileViewModel
    {
        public ICollection<FeedbackReturnDto> Bests { get; set; }

        public ICollection<FeedbackReturnDto> Bads { get; set; }
    }
}