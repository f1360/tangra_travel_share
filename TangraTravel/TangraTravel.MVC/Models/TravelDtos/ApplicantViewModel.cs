﻿using TangraTravel.Data.Enum;

namespace TangraTravel.MVC.Models.TravelDtos
{
    public class ApplicantViewModel
    {
        public int TravelId { get; set; }

        public int TravelPassengerId { get; set; }

        public string FullName { get; set; }

        public ApplyStatus ApplyStatus { get; set; }
    }
}