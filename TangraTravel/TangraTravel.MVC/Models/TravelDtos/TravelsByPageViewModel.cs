﻿using System.Collections.Generic;

using TangraTravel.MVC.Models.Abstract;

namespace TangraTravel.MVC.Models.TravelDtos
{
    public class TravelsByPageViewModel : Pagination
    {
        public TravelsByPageViewModel()
            : base(byPage: 5)
        {
        }

        public ICollection<TravelMainViewModel> Travels { get; set; }
    }
}