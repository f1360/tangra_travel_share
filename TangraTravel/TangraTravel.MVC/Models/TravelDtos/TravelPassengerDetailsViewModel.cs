﻿namespace TangraTravel.MVC.Models.TravelDtos

{
    public class TravelPassengerDetailsViewModel : TravelMainViewModel
    {
        public string CarRegNumber { get; set; }

        public string CarBrand { get; set; }

        public string CarModel { get; set; }
    }
}