﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace TangraTravel.MVC.Models.TravelDtos
{
    public class CreateTravelViewModel
    {
        public int StartAddressId { get; set; }

        public int EndAddressId { get; set; }

        public string DepartureTime { get; set; }

        public string Comment { get; set; }

        public SelectList Addresses { get; set; }
    }
}