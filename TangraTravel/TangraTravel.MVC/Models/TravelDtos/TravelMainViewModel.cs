﻿namespace TangraTravel.MVC.Models.TravelDtos

{
    public class TravelMainViewModel
    {
        public int TravelId { get; set; }

        public string StartAddress { get; set; }

        public string EndAddress { get; set; }

        public string DepartureTime { get; set; }

        public int FreeSeats { get; set; }

        public string Comment { get; set; }

        public string DriverName { get; set; }

        public string Status { get; set; }

        public string PassengerStatus { get; set; }
    }
}