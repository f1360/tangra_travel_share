﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace TangraTravel.MVC.Models.TravelDtos
{
    public class SortViewModel
    {
        public string Criterion { get; set; }
    }
}
