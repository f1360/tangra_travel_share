﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace TangraTravel.MVC.Models.TravelDtos
{
    public class FilterAvailableViewModel
    {
        public int StartAddressId { get; set; }

        public SelectList StartAddresses { get; set; }

        public int EndAddressId { get; set; }

        public SelectList EndAddresses { get; set; }

        public int DriverId { get; set; }

        public SelectList Drivers { get; set; }
    }
}