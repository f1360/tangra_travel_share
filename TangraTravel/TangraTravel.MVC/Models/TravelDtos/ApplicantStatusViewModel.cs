﻿using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc.Rendering;

using TangraTravel.Data.Enum;

namespace TangraTravel.MVC.Models.TravelDtos
{
    public class ApplicantStatusViewModel : ApplicantViewModel
    {
        public List<SelectListItem> ApplicantStatuses { get; set; }

        public ApplicantStatusViewModel()
        {
            ApplicantStatuses = new List<SelectListItem>();

            foreach (ApplyStatus applyStatus in (ApplyStatus[])ApplyStatus.GetValues(typeof(ApplyStatus)))
            {
                if (applyStatus == ApplyStatus.Canceled)
                {
                    continue;
                }

                ApplicantStatuses.Add(new SelectListItem
                {
                    Value = ((int)applyStatus).ToString(),
                    Text = applyStatus.ToString()
                });
            }
        }
    }
}