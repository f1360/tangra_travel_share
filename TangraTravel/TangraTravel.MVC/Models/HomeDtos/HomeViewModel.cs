﻿namespace TangraTravel.MVC.Models.HomeDtos
{
    public class HomeViewModel
    {
        public int UsersCount { get; set; }

        public int TravelsCount { get; set; }
    }
}