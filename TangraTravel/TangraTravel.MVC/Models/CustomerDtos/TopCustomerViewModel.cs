﻿namespace TangraTravel.MVC.Models.CustomerDto
{
    public class TopCustomerViewModel
    {
        public string AvatarUrl { get; set; }

        public string FullName { get; set; }

        public double Rating { get; set; }

        public string UserName { get; set; }
    }
}