﻿namespace TangraTravel.MVC.Models.CustomerDto
{
    public class CustomerIndexViewModel
    {
        public int UserId { get; set; }

        public string FullName { get; set; }

        public string AvatarUrl { get; set; }

        public string AvatarAlt { get; set; }

        public string Email { get; set; }

        public bool Car { get; set; }
    }
}