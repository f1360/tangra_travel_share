﻿using TangraTravel.Service.Models.AvatarDtos;

namespace TangraTravel.MVC.Models.CarDto
{
    public class CarViewModel
    {
        public string Owner { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public string RegistrationNumber { get; set; }

        public int Seats { get; set; }

        public CarAvatarDto Avatar { get; set; }

        public string ErrorMessage { get; set; }
    }
}