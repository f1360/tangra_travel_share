﻿using System.ComponentModel.DataAnnotations;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace TangraTravel.MVC.Models.CarDto
{
    public class CarRegisterViewModel
    {
        [Required, StringLength(10, MinimumLength = 4, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string RegistrationNumber { get; set; }

        [Range(1,8,ErrorMessage = "Seats can be between 1 to 10 places!")]
        public int Seats { get; set; }

        public int ModelId { get; set; }
        public SelectList BasicCarInfromation { get; set; }
    }
}