﻿using System.Threading.Tasks;

using TangraTravel.Data.Models;
using TangraTravel.Data.Models.Abstract;

namespace TangraTravel.MVC.Helpers.Contracts
{
    public interface IAuthHelper
    {
        Task<User> TryGetUserAsync(string email, string password);
    }
}