﻿using System.Threading.Tasks;

using TangraTravel.Service.Contracts;
using TangraTravel.Data.Models.Abstract;
using TangraTravel.MVC.Helpers.Contracts;

namespace TangraTravel.MVC.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IUserAuthService authService;

        public AuthHelper(IUserAuthService authService)
        {
            this.authService = authService;
        }
        public async Task<User> TryGetUserAsync(string email, string password)
        {
            var user = await this.authService.LogInAsync(email, password);

            return user;
        }
    }
}