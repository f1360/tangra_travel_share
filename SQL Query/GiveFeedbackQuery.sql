SELECT 
	t.Id AS TravelId,
	t.TravelStatus AS [Travel Status],
	d.id AS DriverId,
	d.Email AS [Driver Email],
	d.Password AS [Driver Password],
	d.Username AS [Driver Username],
	dr.Access AS [DriverRole],
	p.id AS PassengerId,
	tp.ApplyStatus AS [Apply Status Passenger],
	p.Email AS [Passenger Email],
	p.Password AS [Passenger Password],
	p.Username AS [Passenger Username],
	pr.Access AS [Passenger Role]
FROM Travels AS t
JOIN Customers AS d ON t.DriverId =  d.Id
LEFT JOIN Roles AS dr ON dr.id = d.RoleId
LEFT JOIN TravelPassengers AS tp ON tp.TravelId = t.id
LEFT JOIN Customers AS p ON p.Id = tp.PassengerId
LEFT JOIN Roles AS pr ON pr.Id = p.RoleId
WHERE t.id =3

SELECT c.FirstName, c.LastName, c.Rating, f.Rating AS [Feedback Rating], fb.Comment AS [Comment]
FROM Customers AS c
JOIN Feedbacks AS f ON f.TargetId = c.Id
LEFT JOIN FeedbackComments AS fb ON fb.FeedbackId = f.Id
WHERE c.id = 5