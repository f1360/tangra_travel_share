SELECT 
	c.Id AS CarId, 
	b.Name AS Brand,
	m.BrandModel AS Model,
	c.RegistrationNumber AS [Registration Number],
	c.Seats,
	c.IsActive AS Status,
	owner.FirstName + ' '+owner.LastName + ' '+owner.Username AS Owner,
	owner.Id AS OwnerId,
	owner.Email AS Email,
	t.Id AS TravelId,
	t.TravelStatus AS [Travel Status],
	t.FreeSeats AS FreeSeats
FROM Cars AS c
JOIN Models AS m ON m.Id = c.ModelId
JOIN Brands AS b ON b.Id = m.BrandId
JOIN Customers AS owner ON owner.Id = c.CustomerId
JOIN Travels AS t ON owner.Id = t.DriverId
WHERE owner.Email = 'stoyan.stavrev@abv.bg' --And c.IsActive = 1