SELECT 
	t.Id AS TravelId,
	t.FreeSeats AS [Free Seats],
	t.TravelStatus AS [Travel Status],
	d.Id AS DriverId,
	d.Email AS [Driver Email],
	car.Seats AS [Car Seats],
	p.Id AS PassengerId,
	p.Email AS [Passenger Email],
	tp.ApplyStatus
FROM Travels AS t
JOIN Customers AS d ON t.DriverId = d.id
JOIN Cars AS car ON car.CustomerId = d.Id
JOIN TravelPassengers AS tp ON t.Id = tp.TravelId
JOIN Customers AS p ON tp.PassengerId = p.Id
WHERE t.Id = 3 AND tp.ApplyStatus = 'Accepted'